package com.dataart.dartcard.presentation.di.module

import android.content.Context
import com.dataart.dartcard.BuildConfig
import com.dataart.dartcard.data.AuthorizationApiService
import com.dataart.dartcard.data.UserSessionApiService
import com.dataart.dartcard.data.cache.CacheContext
import com.dataart.dartcard.data.retrofit.ApiServiceBuilder
import com.dataart.dartcard.data.retrofit.OkHttpBuilder
import com.dataart.dartcard.data.retrofit.RxErrorHandlingCallAdapterFactory
import com.dataart.dartcard.data.retrofit.TokenAuthenticator
import com.jakewharton.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton


@Module
class DataModule : BaseDataModule() {

    @Provides
    @Singleton
    @Named("no_session")
    fun provideOkHttpClient(): OkHttpClient {
        return OkHttpBuilder.newBuilder(BuildConfig.API_ENDPOINT)
                .withLoggingInterceptor(HttpLoggingInterceptor.Level.BODY)
                .withReadTimeOut(READ_WRITE_TIMEOUT)
                .withWriteTimeOut(READ_WRITE_TIMEOUT)
                .withClientTypeInterceptor()
                .withConnectTimeOut(CONNECT_TIMEOUT)
                .withSelfSignedSSLSocketFactory()
                .build()
    }

    @Provides
    @Singleton
    fun provideAuthorizationApiService(@Named("no_session") client: OkHttpClient): AuthorizationApiService {
        return ApiServiceBuilder.newBuilder(BuildConfig.API_ENDPOINT)
                .withOkHttpClient(client)
                .withConverterFactory(GsonConverterFactory.create())
                .withCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                .build(AuthorizationApiService::class.java)
    }


    @Provides
    @Singleton
    @Named("session")
    fun provideUserSessionOkHttpClient(authorizationApiService: AuthorizationApiService, cache: CacheContext): OkHttpClient {
        val tokenAuthenticator = TokenAuthenticator(authorizationApiService, cache)
        return OkHttpBuilder.newBuilder(BuildConfig.API_ENDPOINT)
                .withLoggingInterceptor(HttpLoggingInterceptor.Level.BODY)
                .withClientTypeInterceptor()
                .withReadTimeOut(READ_WRITE_TIMEOUT)
                .withWriteTimeOut(READ_WRITE_TIMEOUT)
                .withConnectTimeOut(CONNECT_TIMEOUT)
                .withAuthorizationHeaderInterceptor(cache)
                .withUnauthorizedErrorInterceptor()
                .withSelfSignedSSLSocketFactory()
                .withAuthenticator(tokenAuthenticator)
                .build()
    }


    @Provides
    @Singleton
    fun provideUserSessionApiService(@Named("session") client: OkHttpClient): UserSessionApiService {
        return ApiServiceBuilder.newBuilder(BuildConfig.API_ENDPOINT)
                .withOkHttpClient(client)
                .withConverterFactory(GsonConverterFactory.create())
                .withCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                .build(UserSessionApiService::class.java)
    }


    @Provides
    fun providePicasso(context: Context, @Named("session") client: okhttp3.OkHttpClient): Picasso {
        return Picasso.Builder(context)
                .downloader(OkHttp3Downloader(client))
                .loggingEnabled(true)
                .build()
    }

    companion object {

        private val READ_WRITE_TIMEOUT = 60
        private val CONNECT_TIMEOUT = 60
    }

}