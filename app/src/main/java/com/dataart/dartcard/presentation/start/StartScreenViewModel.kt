package com.dataart.dartcard.presentation.start

import com.dataart.dartcard.domain.SchedulersFacade
import com.dataart.dartcard.domain.auth.AuthRepository
import com.dataart.dartcard.presentation.base.viewmodel.BaseViewModel
import com.dataart.dartcard.presentation.base.viewmodel.SingleLiveEvent


class StartScreenViewModel(private val authRepository: AuthRepository) : BaseViewModel() {

    val isUserAuthorized = SingleLiveEvent<Boolean>()

    fun chooseNavigation() {
        addDisposable(
                authRepository.isAuthorized
                        .compose(SchedulersFacade.applySchedulersObservable())
                        .subscribe { isUserAuthorized.setValue(it) }
        )
    }
}
