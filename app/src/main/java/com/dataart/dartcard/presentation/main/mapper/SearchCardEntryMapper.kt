package com.dataart.dartcard.presentation.main.mapper

import com.dataart.dartcard.data.room.entity.SearchCardEntry
import com.dataart.dartcard.domain.mapper.Mapper
import com.dataart.dartcard.presentation.main.models.CardModel

class SearchCardEntryMapper : Mapper<SearchCardEntry, CardModel>() {
    override fun map(source: SearchCardEntry): CardModel {
        return CardModel(source.id).apply {
            brandName = source.brandName
            photo = source.photo
            favorite = source.favorite
            ownerName = source.ownerName
            synced = source.synced
            isPrivate = source.isPrivate
            mine = source.mine
        }
    }
}
