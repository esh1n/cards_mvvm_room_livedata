package com.dataart.dartcard.presentation.main.fragment.cards

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.dataart.dartcard.R
import com.dataart.dartcard.presentation.main.adapter.fragment.CardsPagerAdapter

class CardsTabHostFragment : Fragment() {

    var viewPager: ViewPager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_cards_tab_host, container, false)
        viewPager = rootView.findViewById(R.id.viewpager_card_tabs)
        viewPager?.adapter = CardsPagerAdapter(childFragmentManager, requireActivity())
        val tabLayout = rootView.findViewById<TabLayout>(R.id.sliding_tabs)
        tabLayout.setupWithViewPager(viewPager)
        return rootView
    }

    fun getCurrentFragment(): Fragment? {
        val currentFragmentIndex = viewPager?.currentItem ?: 0
        return childFragmentManager.findFragmentByTag("android:switcher:" + R.id.viewpager_card_tabs + ":" + currentFragmentIndex)
    }

}
