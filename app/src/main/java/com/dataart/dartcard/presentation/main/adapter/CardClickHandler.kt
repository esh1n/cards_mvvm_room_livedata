package com.dataart.dartcard.presentation.main.adapter

import com.dataart.dartcard.presentation.main.models.CardModel

interface CardClickHandler {
    fun manageFavorite(cardModel: CardModel)

    fun openDetails(cardModel: CardModel)
}
