package com.dataart.dartcard.presentation.addedit.activity

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.Button

import com.dataart.dartcard.R
import com.dataart.dartcard.presentation.addedit.fragment.AddEditCardFragment
import com.journeyapps.barcodescanner.CaptureManager
import com.journeyapps.barcodescanner.DecoratedBarcodeView

/**
 * Custom Scannner Activity extending from Activity to display a custom layout form scanner view.
 */
class CustomScannerActivity : Activity(), DecoratedBarcodeView.TorchListener {

    private lateinit var capture: CaptureManager
    private var barcodeScannerView: DecoratedBarcodeView? = null
    private var switchFlashlightButton: Button? = null
    private var btnCardWithoutBarCode: View? = null
    private var btnCardBarCodeManually: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_custom_scanner)
        initButtons()

        barcodeScannerView = findViewById<View>(R.id.zxing_barcode_scanner) as DecoratedBarcodeView
        barcodeScannerView?.setTorchListener(this)

        switchFlashlightButton = findViewById<View>(R.id.switch_flashlight) as Button

        // if the device does not have flashlight in its camera,
        // then remove the switch flashlight button...
        if (!hasFlash()) {
            switchFlashlightButton?.visibility = View.GONE
        }

        capture = CaptureManager(this, barcodeScannerView)
        capture.initializeFromIntent(intent, savedInstanceState)
        capture.decode()
    }

    private fun initButtons() {
        btnCardWithoutBarCode = findViewById(R.id.btn_card_no_barcode)
        btnCardBarCodeManually = findViewById(R.id.btn_card_manually)
        btnCardBarCodeManually?.setOnClickListener { _ -> cancelScanning(CancelScanState.INPUT_MANUALLY) }
        btnCardWithoutBarCode?.setOnClickListener { _ -> cancelScanning(CancelScanState.NO_BARCODE) }
    }

    private fun cancelScanning(cancelScanState: CancelScanState) {
        val resultIntent = Intent()
        resultIntent.putExtra(AddEditCardFragment.BAR_CODE_ADD_OTHER_WAY, cancelScanState.name)
        setResult(Activity.RESULT_CANCELED, resultIntent)
        onBackPressed()
    }

    override fun onResume() {
        super.onResume()
        capture.onResume()
    }

    override fun onPause() {
        super.onPause()
        capture.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        capture.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        capture.onSaveInstanceState(outState)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return barcodeScannerView?.onKeyDown(keyCode, event) ?: true || super.onKeyDown(keyCode, event)
    }

    /**
     * Check if the device's camera has a Flashlight.
     *
     * @return true if there is Flashlight, otherwise false.
     */
    private fun hasFlash(): Boolean {
        return applicationContext.packageManager
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)
    }

    fun switchFlashlight() {
        if (getString(R.string.turn_on_flashlight) == switchFlashlightButton?.text) {
            barcodeScannerView?.setTorchOn()
        } else {
            barcodeScannerView?.setTorchOff()
        }
    }

    override fun onTorchOn() {
        switchFlashlightButton?.setText(R.string.turn_off_flashlight)
    }

    override fun onTorchOff() {
        switchFlashlightButton?.setText(R.string.turn_on_flashlight)
    }

    enum class CancelScanState {
        INPUT_MANUALLY, NO_BARCODE
    }
}
