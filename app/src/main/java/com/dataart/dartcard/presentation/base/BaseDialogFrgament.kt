package com.dataart.dartcard.presentation.base

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.DialogFragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dataart.dartcard.data.retrofit.RefreshTokenException
import com.dataart.dartcard.presentation.auth.AuthActivity
import com.dataart.dartcard.presentation.base.viewmodel.BaseViewModel
import dagger.android.support.AndroidSupportInjection

abstract class BaseVMDialogFragment<VM : BaseViewModel> : DialogFragment() {

    @get:LayoutRes
    abstract val layoutResource: Int

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutResource, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView(view)
    }

    protected open fun setupView(rootView: View) {

    }

    protected lateinit var viewModel: VM

    abstract val viewModelClass: Class<VM>

    abstract val factory: ViewModelProvider.Factory

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, factory).get(viewModelClass)
        observeError()
    }

    private fun observeError() {
        viewModel.error.observe(this, Observer<Throwable> { this.onError(it!!) })
    }

    protected open fun onError(t: Throwable) {
        Log.e(viewModelClass.simpleName, t.message)
        if (t is RefreshTokenException) {
            activity!!.finish()
            AuthActivity.start(activity)
        }
    }
}