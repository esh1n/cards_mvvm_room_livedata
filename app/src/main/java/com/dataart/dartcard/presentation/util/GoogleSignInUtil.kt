package com.dataart.dartcard.presentation.util

import android.content.Context
import android.content.Intent
import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import android.util.Log

import com.dataart.dartcard.R
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task

object GoogleSignInUtil {

    private const val RC_GET_AUTH_CODE = 42

    fun getGoogleClient(context: Context): GoogleSignInClient {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestServerAuthCode(context.getString(R.string.server_client_id), true)
                .requestEmail()
                .build()
        return GoogleSignIn.getClient(context, gso)
    }

    fun signInExplicitly(fragment: Fragment, client: GoogleSignInClient) {
        fragment.startActivityForResult(client.signInIntent, RC_GET_AUTH_CODE)
    }

    fun processSignInResult(requestCode: Int, data: Intent?, authProcessor: AuthProcessor) {
        if (requestCode == RC_GET_AUTH_CODE) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task, authProcessor)
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>, authProcessor: AuthProcessor) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            authProcessor.onGetAuthCode(account.serverAuthCode, account.email)
        } catch (e: ApiException) {
            Log.w(GoogleSignInUtil::class.java.simpleName, "signInResult:failed code=" + e.statusCode)
            authProcessor.onError(SignInApiError.valueOf(e.statusCode)?.description())
        }

    }

    interface AuthProcessor {

        fun onGetAuthCode(code: String?, email: String?)

        fun onError(message: Int?)
    }

    enum class SignInApiError(val value: Int) {
        API_NOT_CONNECTED(17) {
            override fun description() = R.string.error_signin_api_not_connected
        },
        CANCELED(16) {
            override fun description() = R.string.error_signin_api_cancelled
        },
        DEVELOPER_ERROR(10) {
            override fun description() = R.string.error_signin_api_developer
        },
        ERROR(13) {
            override fun description() = R.string.error_signin_api_error
        },
        INTERNAL_ERROR(8) {
            override fun description() = R.string.error_signin_api_internal_error
        },
        INTERRUPTED(14) {
            override fun description() = R.string.error_signin_api_interrupted
        },
        NETWORK_ERROR(7) {
            override fun description() = R.string.error_signin_api_network_error
        },
        RESOLUTION_REQUIRED(6) {
            override fun description() = R.string.error_signin_api_resolution
        },
        SIGN_IN_REQUIRED(4) {
            override fun description() = R.string.error_signin_api_sign_in_required
        },
        TIMEOUT(15) {
            override fun description() = R.string.error_signin_api_timeout
        },
        SUCCESS_CACHE(-1) {
            override fun description() = R.string.error_signin_api_success_cache
        };

        @StringRes
        abstract fun description(): Int

        companion object {
            fun valueOf(value: Int): SignInApiError? = SignInApiError.values().find { it.value == value }
        }
    }


}
