package com.dataart.dartcard.presentation.main.viewmodel

import android.arch.lifecycle.MutableLiveData
import com.dataart.dartcard.domain.SchedulersFacade
import com.dataart.dartcard.domain.cards.CardsRepository
import com.dataart.dartcard.presentation.base.viewmodel.BaseLoadableViewModel
import com.dataart.dartcard.presentation.main.models.CardWithBarcodeModel
import com.dataart.dartcard.presentation.util.BarcodeDecoder

class BarCodeViewModel internal constructor(private val cardsRepository: CardsRepository) : BaseLoadableViewModel() {

    val card = MutableLiveData<CardWithBarcodeModel>()
    fun loadBarCode(cardId: String, width: Int, height: Int) {
        addDisposable(cardsRepository.loadBarCode(cardId)
                .map { card ->
                    val bitmap = BarcodeDecoder.decodeWithRotate(card.value, card.format, 90, width, height)
                    return@map CardWithBarcodeModel(card.brandName, bitmap)
                }
                .compose(SchedulersFacade.applySchedulersSingle())
                .doOnSubscribe { _ -> loadingStatus.value = true }
                .doAfterTerminate { -> loadingStatus.value = false }
                .subscribe { bitmap -> this.card.value = bitmap })
    }
}