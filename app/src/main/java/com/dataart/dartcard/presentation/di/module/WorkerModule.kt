package com.dataart.dartcard.presentation.di.module

import com.birbit.android.jobqueue.JobManager
import com.dataart.dartcard.data.UserSessionApiService
import com.dataart.dartcard.data.room.AppDatabase
import com.dataart.dartcard.domain.sync.SyncRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class WorkerModule {

    @Provides
    @Singleton
    fun provideSyncRepository(appDatabase: AppDatabase, userSessionApiService: UserSessionApiService, jobManager: JobManager): SyncRepository {
        return SyncRepository(userSessionApiService, appDatabase, jobManager)
    }
}