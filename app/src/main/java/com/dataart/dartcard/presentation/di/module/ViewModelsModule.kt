package com.dataart.dartcard.presentation.di.module

import com.birbit.android.jobqueue.JobManager
import com.dataart.dartcard.data.AuthorizationApiService
import com.dataart.dartcard.data.UserSessionApiService
import com.dataart.dartcard.data.cache.CacheContext
import com.dataart.dartcard.data.job.RxJobResultBus
import com.dataart.dartcard.data.room.AppDatabase
import com.dataart.dartcard.domain.auth.AuthRepository
import com.dataart.dartcard.domain.brands.BrandsRepository
import com.dataart.dartcard.domain.cards.CardsRepository
import com.dataart.dartcard.domain.favorite.UserCardRepository
import com.dataart.dartcard.presentation.addedit.viewmodel.AddEditCardViewModelFactory
import com.dataart.dartcard.presentation.auth.viewmodel.AuthViewModelFactory
import com.dataart.dartcard.presentation.main.viewmodel.MainViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class ViewModelsModule {

    @Provides
    internal fun provideCardsViewModelFactory(database: AppDatabase, userSessionApiService: UserSessionApiService,
                                              workManager: JobManager, cacheContext: CacheContext, rxJobResultBus: RxJobResultBus): MainViewModelFactory {
        val cardsRepository = CardsRepository(userSessionApiService, database, workManager)
        val brandsRepository = BrandsRepository(userSessionApiService, database)
        val userCardRepository = UserCardRepository(userSessionApiService, database, workManager)
        return MainViewModelFactory(cardsRepository, brandsRepository, userCardRepository, workManager, cacheContext, rxJobResultBus)
    }

    @Provides
    internal fun provideAuthViewModelFactory(database: AppDatabase, cacheContext: CacheContext,
                                             authorizationApiService: AuthorizationApiService,
                                             workManager: JobManager): AuthViewModelFactory {
        val authRepository = AuthRepository(database, cacheContext, authorizationApiService, workManager)
        return AuthViewModelFactory(authRepository, workManager)
    }

    @Provides
    internal fun provideAddEditCardViewModelFactory(database: AppDatabase, userSessionApiService: UserSessionApiService, workManager: JobManager): AddEditCardViewModelFactory {
        val cardsRepository = CardsRepository(userSessionApiService, database, workManager)
        val brandsRepository = BrandsRepository(userSessionApiService, database)
        return AddEditCardViewModelFactory(cardsRepository, brandsRepository)
    }
}
