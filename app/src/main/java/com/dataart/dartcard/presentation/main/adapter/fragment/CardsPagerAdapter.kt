package com.dataart.dartcard.presentation.main.adapter.fragment


import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class CardsPagerAdapter(fm: FragmentManager, private val context: Context) : FragmentPagerAdapter(fm) {

    override fun getCount(): Int {
        return CardsTab.values().size
    }

    override fun getItem(position: Int): Fragment {
        val cardsTab = CardsTab.getByPosition(position)
        return cardsTab.fragment
    }

    override fun getPageTitle(position: Int): CharSequence? {
        val cardsTab = CardsTab.getByPosition(position)
        return context.getString(cardsTab.titleResourceId)
    }


}