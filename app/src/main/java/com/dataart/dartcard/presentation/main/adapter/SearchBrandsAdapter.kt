package com.dataart.dartcard.presentation.main.adapter


import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.dataart.dartcard.R
import com.dataart.dartcard.databinding.ItemSearchBrandBinding
import com.dataart.dartcard.presentation.main.models.BrandModel
import com.dataart.dartcard.presentation.util.inflate


class SearchBrandsAdapter(private val mClickHandler: (BrandModel) -> Unit) : RecyclerView.Adapter<SearchBrandsAdapter.ViewHolder>() {

    private var brands: List<BrandModel>? = null


    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = viewGroup.context.inflate(R.layout.item_search_brand, viewGroup)
        view.isFocusable = true
        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val brandModel = brands?.get(position)
        if (brandModel != null) {
            viewHolder.bindTo(brandModel)
        } else {
            viewHolder.clear()
        }
    }

    override fun getItemCount(): Int {
        return brands?.size ?: 0
    }

    fun swapBrands(newBrands: List<BrandModel>) {
        if (brands == null) {
            this.brands = newBrands
            notifyDataSetChanged()
        } else {
            val result = DiffUtil.calculateDiff(DiffCallback(brands!!, newBrands))
            brands = newBrands
            result.dispatchUpdatesTo(this)
        }
    }


    private inner class DiffCallback(private val oldBrands: List<BrandModel>, private val newBrands: List<BrandModel>) : DiffUtil.Callback() {

        override fun getOldListSize(): Int {
            return oldBrands.size
        }

        override fun getNewListSize(): Int {
            return newBrands.size
        }

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val (_, id) = oldBrands[oldItemPosition]
            val (_, id1) = newBrands[newItemPosition]
            return id == id1
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldBrand = oldBrands[oldItemPosition]
            val newBrand = newBrands[newItemPosition]
            return oldBrand == newBrand
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {

        var binding: ItemSearchBrandBinding? = null

        init {
            binding = DataBindingUtil.bind(view)
            view.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            val adapterPosition = adapterPosition
            val brandModel = brands?.get(adapterPosition)
            brandModel?.let {
                mClickHandler.invoke(it)
            }

        }

        fun bindTo(brandModel: BrandModel) {
            binding?.brand = brandModel
        }

        fun clear() {
            binding?.brand = null
        }
    }
}