package com.dataart.dartcard.presentation.addedit.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import com.dataart.dartcard.R
import com.dataart.dartcard.databinding.FragmentAddBarcodeManuallyBinding
import com.dataart.dartcard.presentation.addedit.adapter.BarcodeFormatSpinnerAdapter
import com.dataart.dartcard.presentation.addedit.viewmodel.AddEditCardFlowViewModel
import com.dataart.dartcard.presentation.addedit.viewmodel.AddEditCardViewModelFactory
import com.dataart.dartcard.presentation.addedit.viewmodel.BarCodeInputViewModel
import com.dataart.dartcard.presentation.base.BaseVMFragment
import com.dataart.dartcard.presentation.util.BarcodeDecoder
import com.dataart.dartcard.presentation.validation.ValidationField
import com.dataart.dartcard.presentation.validation.ValidationInput
import com.google.zxing.BarcodeFormat
import io.reactivex.Observable
import javax.inject.Inject

class BarcodeInputFragment : BaseVMFragment<BarCodeInputViewModel>() {

    @Inject
    override lateinit var factory: AddEditCardViewModelFactory

    private var binding: FragmentAddBarcodeManuallyBinding? = null
    private lateinit var sharedViewModel: AddEditCardFlowViewModel

    private lateinit var submitItem: MenuItem

    private lateinit var barcodeAdapter: BarcodeFormatSpinnerAdapter

    override val layoutResource = R.layout.fragment_add_barcode_manually

    private val barCodeSource: Observable<ValidationField>
        get() {
            val validatedView = binding!!.vetBarcode
            return validatedView
                    .textChangesSource
                    .map { _ ->
                        ValidationField.newBuilder()
                                .withType(ValidationField.Type.BARCODE)
                                .withInput(ValidationInput(validatedView.text, true))
                                .withSuccessValidationAction {
                                    Log.d("valBrand", "success validation")
                                    submitItem.isVisible = true
                                }
                                .withEmptyInputAction { submitItem.isVisible = false }
                                .withValidationErrorAction { submitItem.isVisible = false }.build()
                    }
        }

    public override fun setupView(rootView: View) {
        barcodeAdapter = BarcodeFormatSpinnerAdapter(requireActivity())
        binding = DataBindingUtil.bind(rootView)
        binding?.barcodeSpinner?.adapter = barcodeAdapter
        binding?.barcodeContainer?.setOnClickListener { _ -> binding?.barcodeSpinner?.performClick() }
    }

    override val viewModelClass = BarCodeInputViewModel::class.java

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sharedViewModel = ViewModelProviders.of(requireActivity()).get(AddEditCardFlowViewModel::class.java)
        viewModel.barCodeLiveData.observe(this, Observer { barCodeModel ->
            barCodeModel?.let {
                sharedViewModel.selectBarcode(it)
                requireActivity().supportFragmentManager.popBackStack()
            }

        })
        sharedViewModel.card.observe(this, Observer { card ->
            card?.barCode?.let {
                binding?.vetBarcode?.text = it.value ?: ""

            }
            val formatValue = card?.barCode?.format
            val currentFormat = BarcodeDecoder.parseBarcodeFormat(formatValue, BarcodeFormat.EAN_13)
            val position = barcodeAdapter.getPosition(currentFormat)
            binding?.barcodeSpinner?.setSelection(position)
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu?.clear()
        inflater?.inflate(R.menu.menu_submit, menu)
        submitItem = menu?.findItem(R.id.action_submit)!!
        submitItem.isVisible = false
        submitItem.actionView?.setOnClickListener { _ -> tryToSaveBarCode() }
    }

    private fun tryToSaveBarCode() {
        binding?.vetBarcode?.text?.let {
            val format = barcodeAdapter.getItem(binding?.barcodeSpinner?.selectedItemPosition ?: 0)
            viewModel.tryToStoreBarCode(it, format)
        }

    }

    override fun onStart() {
        super.onStart()
        binding?.vetBarcode?.error = null
        viewModel.validateFields(barCodeSource)
    }

    override fun onStop() {
        super.onStop()
        binding?.vetBarcode?.error = null
    }
}
