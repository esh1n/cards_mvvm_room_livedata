package com.dataart.dartcard.presentation.main.adapter


import android.content.Context
import android.databinding.DataBindingUtil
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.dataart.dartcard.R
import com.dataart.dartcard.databinding.ItemMoreMenuBinding
import com.dataart.dartcard.presentation.main.models.MoreMenuItem
import java.util.*

class MoreMenuAdapter(context: Context) : ArrayAdapter<MoreMenuItem>(context, 0, ArrayList(Arrays.asList(*MoreMenuItem.values()))) {

    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var cv = convertView
        val holder: ViewHolder
        if (cv == null) {
            cv = inflater.inflate(R.layout.item_more_menu, null)
            holder = ViewHolder(cv)
            cv!!.tag = holder
        } else {
            holder = cv.tag as ViewHolder
        }
        holder.populate(getItem(position))
        return cv
    }

    private inner class ViewHolder(rootView: View) {
        private val binding: ItemMoreMenuBinding? = DataBindingUtil.bind(rootView)

        fun populate(item: MoreMenuItem?) {
            binding?.item = item
            binding?.executePendingBindings()
        }
    }
}