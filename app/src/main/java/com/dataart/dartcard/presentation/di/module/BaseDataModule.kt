package com.dataart.dartcard.presentation.di.module

import android.content.SharedPreferences
import com.dataart.dartcard.data.cache.CacheContext
import com.dataart.dartcard.data.cache.SharePreferenceSingleValueCache
import com.dataart.dartcard.data.room.AppDatabase
import com.dataart.dartcard.data.room.dao.CardDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by sergeyeshin on 11/5/17.
 */
@Module
open class BaseDataModule {

    @Provides
    @Singleton
    fun provideUserSessionApiService(appDatabase: AppDatabase): CardDao {
        return appDatabase.cardDao()
    }

    @Provides
    @Singleton
    fun provideCacheContext(sharedPreferences: SharedPreferences): CacheContext {
        val fileSystemCache = SharePreferenceSingleValueCache(sharedPreferences)
        return CacheContext(fileSystemCache)
    }
}
