package com.dataart.dartcard.presentation.base

import android.os.Bundle
import android.support.v7.app.ActionBar
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.view.View
import com.dataart.dartcard.R
import java.util.*

abstract class BaseToolbarActivity : BaseDIActivity() {
    protected var toolbar: Toolbar? = null

    protected open val isToolbarVisible = true

    protected open val isDisplayHomeAsUpEnabled = false

    override val contentViewResourceId = R.layout.activity_base_toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupToolbar()
        supportActionBar?.let {
            if (isToolbarVisible) {
                it.show()
            } else {
                it.hide()
            }
        }
    }

    private fun setupToolbar() {
        toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        if (toolbar != null) {
            setSupportActionBar(toolbar)
            supportActionBar?.let {
                onSetupActionBar(it)
            }
            onSetupToolbar(toolbar!!)
        }
    }

    protected fun onSetupToolbar(toolbar: Toolbar) {
        toolbar.setNavigationOnClickListener { _ -> onBackPressed() }
        getToolbarNavigationIcon(toolbar)?.let {
            it.isFocusable = false
            it.isFocusableInTouchMode = false
        }
    }

    protected open fun onSetupActionBar(actionBar: ActionBar) {
        actionBar.setDisplayShowTitleEnabled(isDisplayHomeAsUpEnabled)
        actionBar.setDisplayHomeAsUpEnabled(isDisplayHomeAsUpEnabled)
        actionBar.setHomeAsUpIndicator(R.drawable.ic_close_white_active)
    }

    private fun getToolbarNavigationIcon(toolbar: Toolbar): View? {
        //check if contentDescription previously was set
        val hadContentDescription = TextUtils.isEmpty(toolbar.navigationContentDescription)
        val contentDescription = if (!hadContentDescription)
            toolbar.navigationContentDescription!!.toString()
        else
            "navigationIcon"
        toolbar.navigationContentDescription = contentDescription
        val potentialViews = ArrayList<View>()

        //find the view based on it's content description, set programatically or with android:contentDescription
        toolbar.findViewsWithText(
                potentialViews,
                contentDescription,
                View.FIND_VIEWS_WITH_CONTENT_DESCRIPTION)

        //Nav icon is always instantiated at this point because calling setNavigationContentDescription ensures its existence
        var navIcon: View? = null
        if (potentialViews.size > 0) {
            navIcon = potentialViews[0]
        }

        //Clear content description if not previously present
        if (hadContentDescription) {
            toolbar.navigationContentDescription = null
        }

        return navIcon
    }

    fun enableHomeAsUpButton(action: () -> Unit) {
        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.setHomeAsUpIndicator(R.drawable.ic_close_white_active)
        }
        toolbar?.setNavigationOnClickListener { _ -> action.invoke() }
    }

    fun disableHomeAsUpButton() {
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }
}
