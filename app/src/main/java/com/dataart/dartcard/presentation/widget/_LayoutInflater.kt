package com.dataart.dartcard.presentation.widget

import android.content.Context
import android.view.LayoutInflater

val Context.layoutInflater get() = LayoutInflater.from(this);