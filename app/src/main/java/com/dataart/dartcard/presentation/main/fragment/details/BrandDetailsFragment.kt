package com.dataart.dartcard.presentation.main.fragment.details

import android.arch.lifecycle.Observer
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.dataart.dartcard.R
import com.dataart.dartcard.databinding.FragmentBrandDetailsBinding
import com.dataart.dartcard.presentation.base.BaseVMFragment
import com.dataart.dartcard.presentation.base.addFragmentToStack
import com.dataart.dartcard.presentation.base.setTitle
import com.dataart.dartcard.presentation.main.adapter.BrandCardsAdapter
import com.dataart.dartcard.presentation.main.adapter.CardClickHandler
import com.dataart.dartcard.presentation.main.models.CardModel
import com.dataart.dartcard.presentation.main.viewmodel.BrandDetailsViewModel
import com.dataart.dartcard.presentation.main.viewmodel.MainViewModelFactory
import com.dataart.dartcard.presentation.util.SnackbarBuilder
import javax.inject.Inject

class BrandDetailsFragment : BaseVMFragment<BrandDetailsViewModel>() {

    @Inject
    override lateinit var factory: MainViewModelFactory

    private lateinit var adapter: BrandCardsAdapter

    private var binding: FragmentBrandDetailsBinding? = null

    private val cardClickHandler = object : CardClickHandler {
        override fun manageFavorite(cardModel: CardModel) {
            viewModel.manageFavoriteState(cardModel)
        }

        override fun openDetails(cardModel: CardModel) {
            val fragment = CardDetailFragment.newInstance(cardModel.id)
            activity.addFragmentToStack(fragment)
        }
    }

    override val viewModelClass = BrandDetailsViewModel::class.java


    override val layoutResource = R.layout.fragment_brand_details

    override fun setupView(rootView: View) {
        binding = DataBindingUtil.bind(rootView)
        binding?.let {
            val linearLayoutManager = LinearLayoutManager(activity)
            val dividerItemDecoration = DividerItemDecoration(requireActivity(), linearLayoutManager.orientation)
            adapter = BrandCardsAdapter(cardClickHandler)
            with(it.recyclerviewBrandCards) {
                addItemDecoration(dividerItemDecoration)
                layoutManager = linearLayoutManager
                setHasFixedSize(true)
                this.adapter = this@BrandDetailsFragment.adapter
            }
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeLoadingStatus()
        val brandId = arguments?.getString(BRAND_ID_KEY)
        brandId?.let {
            observeCards(it)
            observeBrand(it)
        }

    }

    private fun observeCards(brandId: String) {
        viewModel.getCardsByBrand(brandId).observe(this, Observer {
            val isEmpty = it?.isEmpty() ?: true
            binding?.tvEmptyCards?.visibility = if (isEmpty) View.VISIBLE else View.GONE
            adapter.submitList(it)
            if (isEmpty) {
                SnackbarBuilder.buildSnack(view!!, "No cards in brand").show()
                requireActivity().supportFragmentManager.popBackStack()
            }
        })
    }

    private fun observeBrand(brandName: String) {
        viewModel.getBrand(brandName).observe(this, Observer { brandEntry ->
            brandEntry?.let { setTitle(it.name) }
        })
    }

    private fun observeLoadingStatus() {
        viewModel.loadingStatus.observe(this, Observer<Boolean> { it -> it?.let { processLoadingStatus(it) } })
    }

    private fun processLoadingStatus(isLoading: Boolean) {
        binding?.loadingIndicator?.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    companion object {

        private const val BRAND_ID_KEY = "BRAND_ID_KEY"

        fun newInstance(brandId: String) = BrandDetailsFragment().apply {
            arguments = Bundle().apply { putString(BRAND_ID_KEY, brandId) }
        }
    }
}
