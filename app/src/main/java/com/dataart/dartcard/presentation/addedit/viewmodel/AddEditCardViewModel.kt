package com.dataart.dartcard.presentation.addedit.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import com.dataart.dartcard.domain.SchedulersFacade
import com.dataart.dartcard.domain.cards.CardsRepository
import com.dataart.dartcard.presentation.addedit.mapper.EditCardMapper
import com.dataart.dartcard.presentation.addedit.model.AddEditCardModel
import com.dataart.dartcard.presentation.base.viewmodel.BaseLoadableViewModel
import com.dataart.dartcard.presentation.base.viewmodel.SingleLiveEvent
import com.dataart.dartcard.presentation.main.models.CardModel
import com.dataart.dartcard.presentation.validation.ValidationField
import com.dataart.dartcard.presentation.validation.ValidationManager
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers

class AddEditCardViewModel internal constructor(private val repository: CardsRepository) : BaseLoadableViewModel() {

    val formattedDiscount = MutableLiveData<String>()
    private var mapper = EditCardMapper()
    private val validationManager: ValidationManager = ValidationManager()
    val navigateBackEvent = SingleLiveEvent<Void>()


    fun getCard(id: String): LiveData<CardModel> {
        return Transformations.map(repository.getCardById(id)) { mapper.map(it) }
    }

    fun saveCard(cardModel: AddEditCardModel) {
        if (!validationManager.isValid) {
            validationManager.showResultActions()
            return
        }

        addDisposable(
                Observable.just(cardModel)
                        .flatMapCompletable { repository.addCard(it) }
                        .compose(SchedulersFacade.applySchedulersCompletable())
                        .doOnSubscribe { _ -> loadingStatus.setValue(true) }
                        .doAfterTerminate { loadingStatus.setValue(false) }
                        .subscribe({
                            navigateBackEvent.call()
                        }, {
                            error.setValue(it)
                        }))
    }

    fun validateFields(validationSource: Observable<ValidationField>) {
        addDisposable(
                validationManager
                        .validateSource(validationSource, true)
                        .compose(SchedulersFacade.applySchedulersObservable())
                        .subscribe({ it.call() }, { errorValue -> error.setValue(errorValue) }))
    }

    fun editCard(cardModel: AddEditCardModel) {
        if (!validationManager.isValid) {
            validationManager.showResultActions()
            return
        }

        addDisposable(
                Observable.just(cardModel)
                        .flatMapCompletable { repository.editCard(it) }
                        .compose(SchedulersFacade.applySchedulersCompletable())
                        .doOnSubscribe { _ -> loadingStatus.setValue(true) }
                        .doAfterTerminate { loadingStatus.setValue(false) }
                        .subscribe({ navigateBackEvent.call() }, { error.setValue(it) }))
    }

    fun fixDiscountField(discountText: Observable<String>) {

        addDisposable(
                discountText
                        .map { text ->
                            if (text.startsWith("0")) {
                                return@map text.substring(1 until text.length)
                            } else {
                                return@map text
                            }
                        }
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .subscribe { text -> formattedDiscount.value = text }
        )

    }
}
