package com.dataart.dartcard.presentation.di.fragments


import com.dataart.dartcard.presentation.auth.AuthFragment
import com.dataart.dartcard.presentation.di.module.ViewModelsModule

import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
interface AuthActivityModule {

    @ContributesAndroidInjector(modules = [ViewModelsModule::class])
    fun contributeAuthFragment(): AuthFragment
}

