package com.dataart.dartcard.presentation.validation.validator;


import com.dataart.dartcard.presentation.validation.ValidationInput;

public class EmptyStringValidator implements Validator {

    @Override
    public ValidationResult validate(ValidationInput validationInput) {
        Object input = validationInput.getInput();

        if (input == null) {
            return ValidationResult.EMPTY;
        }

        if (!(input instanceof String)) {
            throw new IllegalArgumentException("Illegal input type. Need: String");
        }

        String stringToValidate = (String) input;

        return stringToValidate.equals("") ? ValidationResult.EMPTY : ValidationResult.PASSED;
    }
}