package com.dataart.dartcard.presentation.main.mapper

import com.dataart.dartcard.data.room.entity.BrandEntry
import com.dataart.dartcard.data.room.util.StringSearchUtil
import com.dataart.dartcard.domain.mapper.Mapper


class NewBrandMapper : Mapper<String, BrandEntry>() {

    override fun map(source: String): BrandEntry {
        val searchableName = StringSearchUtil.applyDefaultSearchRules(source)
        return BrandEntry(name = source, searchableName = searchableName, serverId = null, synced = false);
    }
}