package com.dataart.dartcard.presentation.addedit.viewmodel

import android.arch.lifecycle.MutableLiveData
import com.dataart.dartcard.domain.SchedulersFacade
import com.dataart.dartcard.domain.brands.BrandsRepository
import com.dataart.dartcard.presentation.base.viewmodel.BaseViewModel
import com.dataart.dartcard.presentation.main.models.BrandModel
import com.dataart.dartcard.presentation.main.viewmodel.SearchBrandViewModel
import com.dataart.dartcard.presentation.validation.ValidationField
import com.dataart.dartcard.presentation.validation.ValidationManager
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ChooseBrandViewModel(private val brandsRepository: BrandsRepository) : BaseViewModel() {


    private val validationManager = ValidationManager()
    val selectedBrand = MutableLiveData<BrandModel>()

    val filteredBrands = MutableLiveData<List<BrandModel>>()

    fun validateFields(validationSource: Observable<ValidationField>) {
        addDisposable(
                validationManager.validateSource(validationSource, true)
                        .compose<ValidationField.ValidationResultAction>(
                                SchedulersFacade.applySchedulersObservable<ValidationField.ValidationResultAction>())
                        .subscribe({ it.call() }, { errorValue -> error.setValue(errorValue) }))
    }

    fun filter(source: Observable<String>) {
        addDisposable(source
                .observeOn(Schedulers.io())
                .map { SearchBrandViewModel.updateQuery(it) }
                .flatMapMaybe<List<BrandModel>> { brandsRepository.findBrandsByName(it) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { filteredBrands -> this.filteredBrands.setValue(filteredBrands) })
    }

    fun tryToSaveBrand(modelFromEditText: String) {
        if (!validationManager.isValid) {
            validationManager.showResultActions()
            return
        }

        addDisposable(brandsRepository
                .tryToFindBrandByName(modelFromEditText)
                .compose(SchedulersFacade.applySchedulersMaybe())
                .subscribe({ brandModel -> selectedBrand.setValue(brandModel) },
                        { errorThr -> error.setValue(errorThr) }))

    }
}


