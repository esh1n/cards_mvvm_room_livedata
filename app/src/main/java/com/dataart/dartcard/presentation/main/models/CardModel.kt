package com.dataart.dartcard.presentation.main.models

data class CardModel(val id: String,
                     var serverId: String? = null,
                     var photo: String? = null,
                     var brandName: String? = null,
                     var barCode: String? = null,
                     var cardNumber: String? = null,
                     var note: String? = null,
                     var ownerName: String? = null,
                     var favorite: Boolean = false,
                     var discount: Int = 0,
                     var backPhoto: String? = null,
                     var barCodeFormat: String? = null,
                     var mine: Boolean = false,
                     var isPrivate: Boolean = false,
                     var synced: Boolean = false)