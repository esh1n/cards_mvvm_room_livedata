package com.dataart.dartcard.presentation.addedit.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.dataart.dartcard.R
import com.dataart.dartcard.presentation.widget.layoutInflater
import com.google.zxing.BarcodeFormat


class BarcodeFormatSpinnerAdapter(context: Context) : ArrayAdapter<BarcodeFormat>(context, R.layout.item_barcode_format_spinner, R.id.tv_barcode_format, BarcodeFormat.values()) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getItemVIew(position, convertView, parent)
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getItemVIew(position, convertView, parent)
    }

    private fun getItemVIew(position: Int, cv: View?, parent: ViewGroup): View {
        val convertView = cv
                ?: context.layoutInflater.inflate(R.layout.item_barcode_format_spinner, parent, false)
        val barcodeFormat = getItem(position)
        if (barcodeFormat != null) {
            val textView = convertView.findViewById<View>(R.id.tv_barcode_format) as TextView
            textView.text = barcodeFormat.name
        }
        return convertView
    }
}