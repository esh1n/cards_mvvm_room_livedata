package com.dataart.dartcard.presentation.addedit.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider

import com.dataart.dartcard.domain.brands.BrandsRepository
import com.dataart.dartcard.domain.cards.CardsRepository


class AddEditCardViewModelFactory(private val cardsRepository: CardsRepository, private val brandsRepository: BrandsRepository) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AddEditCardViewModel::class.java)) {
            return AddEditCardViewModel(cardsRepository) as T
        }

        if (modelClass.isAssignableFrom(ChooseBrandViewModel::class.java)) {
            return ChooseBrandViewModel(brandsRepository) as T
        }

        if (modelClass.isAssignableFrom(BarCodeInputViewModel::class.java)) {
            return BarCodeInputViewModel() as T
        }

        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

