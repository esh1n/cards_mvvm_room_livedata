package com.dataart.dartcard.presentation.util

import android.graphics.Typeface
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.View


object SpannableUtil {

    fun addClickablePart(ss: SpannableString,
                         text: String, word: String?, color: Int,
                         action: () -> Unit) {

        if (word != null) {
            val idx1 = text.indexOf(word)
            if (idx1 == -1) {
                return
            }
            val idx2 = idx1 + word.length
            ss.setSpan(object : ClickableSpan() {

                override fun onClick(widget: View) {
                    action.invoke()
                }

                override fun updateDrawState(ds: TextPaint) {
                    ds.color = color
                }
            }, idx1, idx2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
    }

    fun addColoredPart(ss: SpannableString,
                       text: String, word: String?, color: Int) {

        if (word != null) {
            val idx1 = text.indexOf(word)
            if (idx1 == -1) {
                return
            }
            val idx2 = idx1 + word.length
            addColoredPart(ss, idx1, idx2, color)
        }
    }

    fun addColoredPart(ss: SpannableString?,
                       startIndex: Int, endIndex: Int, color: Int) {

        ss?.setSpan(ForegroundColorSpan(color), startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
    }

    fun addBoldPart(ss: SpannableString,
                    text: String, word: String?) {

        if (word != null) {
            val idx1 = text.indexOf(word)
            if (idx1 == -1) {
                return
            }
            val idx2 = idx1 + word.length
            ss.setSpan(StyleSpan(Typeface.BOLD), idx1, idx2, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
    }

}