package com.dataart.dartcard.presentation.main.viewmodel

import android.arch.lifecycle.LiveData
import com.dataart.dartcard.data.room.util.StringSearchUtil
import com.dataart.dartcard.presentation.base.viewmodel.BaseLoadableViewModel

abstract class SearchBrandViewModel<T>() : BaseLoadableViewModel() {

    abstract fun searchExactQuery(query: String): LiveData<T>

    fun searchContainsQuery(query: String): LiveData<T> {
        val updatedQuery = updateQuery(query)
        return searchExactQuery(updatedQuery)
    }

    companion object {
        fun updateQuery(oldQuery: String?): String {
            return if (oldQuery.isNullOrBlank()) {
                "%"
            } else {
                val updatedForSearch = StringSearchUtil.applyDefaultSearchRules(oldQuery)
                if (updatedForSearch.isBlank()) {
                    ""
                } else "%$updatedForSearch%"
            }
        }
    }

}
