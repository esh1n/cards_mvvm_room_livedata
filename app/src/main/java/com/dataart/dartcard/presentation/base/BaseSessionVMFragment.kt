package com.dataart.dartcard.presentation.base

import android.arch.lifecycle.Observer
import android.os.Bundle
import com.dataart.dartcard.presentation.auth.AuthActivity
import com.dataart.dartcard.presentation.base.viewmodel.BaseSessionViewModel

abstract class BaseSessionVMFragment<VM : BaseSessionViewModel> : BaseVMFragment<VM>() {

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.logoutEvent.observe(this, Observer {
            it?.let {
                with(requireActivity()) {
                    AuthActivity.start(activity)
                    finish()
                }
            }
        })
    }
}