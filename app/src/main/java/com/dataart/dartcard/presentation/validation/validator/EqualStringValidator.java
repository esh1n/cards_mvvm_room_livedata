package com.dataart.dartcard.presentation.validation.validator;

import android.util.Pair;

import com.dataart.dartcard.presentation.validation.ValidationInput;

public class EqualStringValidator implements Validator {

    @Override
    public ValidationResult validate(ValidationInput validationInput) {
        Object input = validationInput.getInput();

        if (input == null) {
            return ValidationResult.EMPTY;
        }

        if (!(input instanceof Pair)) {
            throw new IllegalArgumentException("Illegal type. Need: Pair of Strings");
        }
        Pair strings = (Pair) input;

        if (!(strings.first instanceof String) || !(strings.second instanceof String)) {
            throw new IllegalArgumentException("Illegal type. Need: Pair of Strings");
        }

        Pair<String, String> stringsToValidate = (Pair<String, String>) input;
        if (validationInput.isMandatory() &&
                (stringsToValidate.first == null || strings.first.equals("")) ||
                (stringsToValidate.second == null || strings.second.equals(""))) {
            return ValidationResult.EMPTY;
        }

        boolean matches = stringsToValidate.first.equals(stringsToValidate.second);
        return matches ? ValidationResult.PASSED : ValidationResult.FAILED;
    }


}