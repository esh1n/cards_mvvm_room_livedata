package com.dataart.dartcard.presentation.main.mapper

import com.dataart.dartcard.domain.mapper.Mapper
import java.text.SimpleDateFormat
import java.util.*

class EntityDateMapper(private val mDateFormat: DateFormat) : Mapper<Long, String>() {

    override fun map(source: Long): String {
        val dateFormat = SimpleDateFormat(mDateFormat.format, Locale.UK)
        return dateFormat.format(Date(source))
    }

    enum class DateFormat constructor(val format: String) {

        SYNC_TIME_STAMP("MMM dd, yyyy 'at' hh:mm a"),
    }
}
