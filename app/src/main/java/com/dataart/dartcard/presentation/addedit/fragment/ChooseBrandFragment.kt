package com.dataart.dartcard.presentation.addedit.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import com.dataart.dartcard.R
import com.dataart.dartcard.databinding.FragmentChooseBrandBinding
import com.dataart.dartcard.presentation.addedit.viewmodel.AddEditCardFlowViewModel
import com.dataart.dartcard.presentation.addedit.viewmodel.AddEditCardViewModelFactory
import com.dataart.dartcard.presentation.addedit.viewmodel.ChooseBrandViewModel
import com.dataart.dartcard.presentation.base.BaseVMFragment
import com.dataart.dartcard.presentation.main.adapter.SearchBrandsAdapter
import com.dataart.dartcard.presentation.main.models.BrandModel
import com.dataart.dartcard.presentation.validation.ValidationField
import com.dataart.dartcard.presentation.validation.ValidationInput
import io.reactivex.Observable
import javax.inject.Inject

class ChooseBrandFragment : BaseVMFragment<ChooseBrandViewModel>() {

    @Inject
    override lateinit var factory: AddEditCardViewModelFactory

    private lateinit var sharedViewModel: AddEditCardFlowViewModel

    private var binding: FragmentChooseBrandBinding? = null

    private lateinit var adapter: SearchBrandsAdapter

    override val layoutResource = R.layout.fragment_choose_brand

    private var submitItem: MenuItem? = null

    private val brandNameSource: Observable<ValidationField>
        get() {
            val validatedView = binding!!.vetBrandName
            return validatedView
                    .textChangesSource
                    .map { _ ->
                        ValidationField.newBuilder()
                                .withType(ValidationField.Type.BRAND_NAME)
                                .withInput(ValidationInput(validatedView.text, true))
                                .withSuccessValidationAction {
                                    Log.d("valBrand", "success validation")
                                    submitItem?.isVisible = true
                                }
                                .withEmptyInputAction { submitItem?.isVisible = false }
                                .withValidationErrorAction { submitItem?.isVisible = false }
                                .build()
                    }
        }


    override fun setupView(rootView: View) {
        super.setupView(rootView)
        initViews(rootView)
    }

    private fun initViews(rootView: View) {
        binding = DataBindingUtil.bind(rootView)
        adapter = SearchBrandsAdapter { this.selectBrand(it) }
        binding?.let {
            it.recyclerviewBrands.layoutManager = LinearLayoutManager(requireActivity())
            it.recyclerviewBrands.setHasFixedSize(true)
            it.recyclerviewBrands.adapter = adapter
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu?.clear()
        inflater?.inflate(R.menu.menu_submit, menu)

        submitItem = menu?.findItem(R.id.action_submit)
        submitItem?.let {
            Log.d("valBrand", "init option set visible false")
            it.isVisible = false
            it.actionView?.setOnClickListener { _ -> tryChooseOrAddBrand() }
        }
    }

    override fun onStart() {
        super.onStart()
        viewModel.validateFields(brandNameSource)
        binding?.vetBrandName?.textChangesSource?.let {
            viewModel.filter(it)
        }
    }

    private fun tryChooseOrAddBrand() {
        binding?.vetBrandName?.text?.let {
            viewModel.tryToSaveBrand(it)
        }
    }

    override val viewModelClass = ChooseBrandViewModel::class.java

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sharedViewModel = ViewModelProviders.of(requireActivity()).get(AddEditCardFlowViewModel::class.java)
        viewModel.filteredBrands.observe(this, Observer<List<BrandModel>> { brandModels ->
            adapter.swapBrands(brandModels.orEmpty())
            val isEmpty = brandModels?.isEmpty() ?: true
            binding?.tvEmptyBrands?.visibility = if (isEmpty) View.VISIBLE else View.GONE
        })
        viewModel.selectedBrand.observe(this, Observer { brandModel ->
            brandModel?.let {
                sharedViewModel.selectBrand(it)
                requireActivity().supportFragmentManager.popBackStack()
            }

        })

        sharedViewModel.card.observe(this, Observer { card ->
            card?.let { cardModel ->
                val brandModel = cardModel.brandModel
                brandModel?.let {
                    Log.d("valBrand", "set value from shared ${it.name}")
                    binding!!.vetBrandName.text = it.name
                }
            }
        })
    }

    private fun selectBrand(brandModel: BrandModel) {
        binding?.let {
            it.vetBrandName.text = brandModel.name
            it.vetBrandName.setSelectionToEnd()
        }
    }
}

