package com.dataart.dartcard.presentation.auth.viewmodel

import com.birbit.android.jobqueue.JobManager
import com.dataart.dartcard.domain.SchedulersFacade
import com.dataart.dartcard.domain.auth.AuthRepository
import com.dataart.dartcard.domain.jobmanager.startSync
import com.dataart.dartcard.presentation.base.viewmodel.BaseLoadableViewModel
import com.dataart.dartcard.presentation.base.viewmodel.SingleLiveEvent


class AuthViewModel(private val repository: AuthRepository, private val workManager: JobManager) : BaseLoadableViewModel() {

    val onLoggedInEvent = SingleLiveEvent<Void>()

    val onLoggedOutEvent = SingleLiveEvent<Void>()

    fun login(authCode: String, email: String) {
        addDisposable(repository.login(authCode, email)
                .compose(SchedulersFacade.applySchedulersCompletable())
                .doOnSubscribe { _ -> loadingStatus.setValue(true) }
                .doAfterTerminate { loadingStatus.setValue(false) }
                .subscribe({
                    onLoggedInEvent.call()
                    workManager.startSync()
                }, { throwable -> error.setValue(throwable) }))

    }

    fun logout() {
        addDisposable(
                repository.logout()
                        .compose(SchedulersFacade.applySchedulersCompletable())
                        .subscribe({ onLoggedOutEvent.call() }) { _ -> error.setValue(RuntimeException("Logout failed")) }
        )

    }
}
