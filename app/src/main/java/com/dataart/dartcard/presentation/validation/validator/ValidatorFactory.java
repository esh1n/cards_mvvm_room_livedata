package com.dataart.dartcard.presentation.validation.validator;

import com.dataart.dartcard.presentation.validation.ValidationField;

import java.util.HashMap;

public class ValidatorFactory {
    private static HashMap<ValidationField.Type, Validator> validators = new HashMap<>();

    public static Validator getValidator(ValidationField.Type type) {
        Validator validator = validators.get(type);

        if (validator == null) {
            validator = createValidator(type);
            validators.put(type, validator);
        }

        return validator;
    }

    public static void setValidator(ValidationField.Type type, Validator validator) {
        validators.put(type, validator);
    }

    private static Validator createValidator(ValidationField.Type type) {
        switch (type) {
            case NAME:
                return new EmptyStringValidator();
            case BRAND_NAME:
                return new RegExpValidator("^.{1,30}$");
            case BARCODE:
                return new RegExpValidator("^.{1,30}$");
            case CARD_NUMBER:
                return new RegExpValidator("^[\\p{L}0-9]{1,25}$");
            default:
                throw new IllegalArgumentException("Validator doesn't exist for such type:" + type);
        }
    }
}
