package com.dataart.dartcard.presentation.validation.validator


import com.dataart.dartcard.presentation.validation.ValidationInput

interface Validator {

    fun validate(input: ValidationInput): ValidationResult

    enum class ValidationResult {
        EMPTY, FAILED, PASSED
    }
}