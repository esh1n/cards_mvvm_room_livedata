package com.dataart.dartcard.presentation.main.mapper


import com.dataart.dartcard.data.room.entity.BrandedCard
import com.dataart.dartcard.domain.mapper.Mapper
import com.dataart.dartcard.presentation.main.models.CardModel


class CardEntityMapper : Mapper<BrandedCard, CardModel>() {
    override fun map(source: BrandedCard): CardModel {
        return CardModel(source.cardId, synced = source.synced).apply {
            brandName = source.brandName
            photo = source.photo
            favorite = true
            ownerName = source.ownerName
            isPrivate = source.isPrivate
            mine = source.mine
        }
    }
}
