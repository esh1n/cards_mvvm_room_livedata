package com.dataart.dartcard.presentation.main.fragment.details

import android.arch.lifecycle.Observer
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.PopupMenu
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import com.dataart.dartcard.R
import com.dataart.dartcard.databinding.FragmentCardDetailsBinding
import com.dataart.dartcard.databinding.ViewCardActionsBinding
import com.dataart.dartcard.presentation.addedit.activity.AddEditCardActivity
import com.dataart.dartcard.presentation.base.BaseVMFragment
import com.dataart.dartcard.presentation.base.setTitle
import com.dataart.dartcard.presentation.main.adapter.ImagePagerAdapter
import com.dataart.dartcard.presentation.main.viewmodel.CardDetailsViewModel
import com.dataart.dartcard.presentation.main.viewmodel.MainViewModelFactory
import com.dataart.dartcard.presentation.util.DialogUtil
import javax.inject.Inject

class CardDetailFragment : BaseVMFragment<CardDetailsViewModel>() {

    @Inject
    override lateinit var factory: MainViewModelFactory
    private var binding: FragmentCardDetailsBinding? = null
    private var imagePagerAdapter: ImagePagerAdapter? = null
    private var menuBinding: ViewCardActionsBinding? = null
    private val deleteAction = { viewModel.deleteCard(binding?.card!!) }


    override val layoutResource = R.layout.fragment_card_details;

    override val viewModelClass = CardDetailsViewModel::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun setupView(rootView: View) {
        binding = DataBindingUtil.bind(rootView)
        imagePagerAdapter = ImagePagerAdapter(requireActivity())
        binding?.vpImages?.adapter = imagePagerAdapter
        binding?.indicator?.setViewPager(binding?.vpImages)
        imagePagerAdapter?.registerDataSetObserver(binding?.indicator!!.dataSetObserver)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeLoadingStatus()
        observeCard()
        observeEvents()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu?.clear()
        inflater?.inflate(R.menu.menu_card_actions, menu)
        val item = menu?.findItem(R.id.action)
        menuBinding = DataBindingUtil.bind(item!!.actionView)
        setMenuClickListeners()
    }

    private fun setMenuClickListeners() {
        menuBinding?.let { it ->
            it.imvFavorite.setOnClickListener { _ ->
                it.card?.let {
                    viewModel.manageFavoriteState(it)
                }
            }
            it.imvMore.setOnClickListener { showPopupMenu(it) }
        }
    }

    private fun observeEvents() {
        viewModel.cardDeleted.observe(this, Observer { _ -> requireActivity().supportFragmentManager.popBackStack() })
    }

    private fun showPopupMenu(v: View) {
        val popupMenu = PopupMenu(requireActivity(), v)
        popupMenu.inflate(R.menu.popup_card_more)
        popupMenu.setOnMenuItemClickListener { item ->

            when (item.itemId) {
                R.id.item_edit -> {
                    AddEditCardActivity.startEdit(activity, binding!!.card?.id)
                    true
                }
                R.id.item_delete -> {
                    val title = getString(R.string.text_card_delete)
                    val content = getString(R.string.text_content_card_delete, menuBinding!!.card?.brandName)
                    val ok = getString(R.string.text_ok)
                    val cancel = getString(R.string.text_cancel)
                    DialogUtil.showYesNoDialog(requireActivity(), title, content, ok, cancel, deleteAction)
                    true
                }
                else -> false
            }
        }
        popupMenu.show()
    }

    private fun observeCard() {
        val id = arguments!!.getString(ARG_ID)
        viewModel.getCard(id).observe(this, Observer { card ->
            Log.d("sync", "REFRESH BRANDS")
            binding?.let {
                it.card = card
                if (card != null) {
                    val barcodeExist = !card.barCode.isNullOrEmpty()
                    it.tvBarCodeTitle.visibility = if (barcodeExist) View.VISIBLE else View.GONE
                    it.tvBarCodeValue.visibility = if (barcodeExist) View.VISIBLE else View.GONE

                    val barcodeImageExist = !card.barCode.isNullOrEmpty() && !card.barCodeFormat.isNullOrEmpty()
                    it.containerBarCodeImv.visibility = if (barcodeImageExist) View.VISIBLE else View.GONE
                    if (barcodeImageExist) {
                        it.containerBarCodeImv.findViewById<View>(R.id.imv_barcode).setOnClickListener { _ ->
                            run {
                                val fm = requireActivity().supportFragmentManager
                                BarcodeFragment.newInstance(card.id).show(fm, "")
                            }
                        }
                    }

                    val ownerNameExist = !card.ownerName.isNullOrEmpty()
                    it.tvCardOwnerNameTitle.visibility = if (ownerNameExist) View.VISIBLE else View.GONE
                    it.tvCardOwnerNameTitle.visibility = if (ownerNameExist) View.VISIBLE else View.GONE

                    val noteExist = !card.note.isNullOrEmpty()
                    it.tvCardCommentTitle.visibility = if (noteExist) View.VISIBLE else View.GONE
                    it.tvCardCommentValue.visibility = if (noteExist) View.VISIBLE else View.GONE

                    val cardNumberExist = !card.cardNumber.isNullOrBlank()
                    it.containerCardNumber.visibility = if (cardNumberExist) View.VISIBLE else View.GONE

                    imagePagerAdapter?.refreshData(listOf(card.photo, card.backPhoto))

                    setTitle(card.brandName)

                    menuBinding?.card = card
                }
            }

        })
    }

    private fun observeLoadingStatus() {
        viewModel.loadingStatus.observe(this, Observer<Boolean> { this.processLoadingStatus(it) })
    }

    private fun processLoadingStatus(isLoading: Boolean?) {
        menuBinding?.loadingIndicator?.visibility = if (isLoading != false) View.VISIBLE else View.GONE
    }

    companion object {

        private const val ARG_ID = "id"

        fun newInstance(id: String) = CardDetailFragment().apply {
            arguments = Bundle().apply { putString(ARG_ID, id) }
        }
    }
}

