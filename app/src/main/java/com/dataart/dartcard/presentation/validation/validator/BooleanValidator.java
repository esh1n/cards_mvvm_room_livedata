package com.dataart.dartcard.presentation.validation.validator;


import android.support.annotation.NonNull;

import com.dataart.dartcard.presentation.validation.ValidationInput;



public class BooleanValidator implements Validator {

    @Override
    public ValidationResult validate(@NonNull ValidationInput validationInput) {
        if (!(validationInput.getInput() instanceof Boolean)) {
            throw new IllegalArgumentException(getExceptionMessage(validationInput.getInput()));
        }

        boolean input = (boolean) validationInput.getInput();
        return input ? ValidationResult.PASSED : ValidationResult.FAILED;
    }

    private String getExceptionMessage(Object input) {
        return String.format("%s is not supported type. Boolean should be used", input.getClass().getSimpleName());
    }
}