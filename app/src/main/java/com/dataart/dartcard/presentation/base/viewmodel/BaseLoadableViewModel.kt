package com.dataart.dartcard.presentation.base.viewmodel

import android.arch.lifecycle.MutableLiveData

open class BaseLoadableViewModel() : BaseViewModel() {

    val loadingStatus = MutableLiveData<Boolean>()
}
