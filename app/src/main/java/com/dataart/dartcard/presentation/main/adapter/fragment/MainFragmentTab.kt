package com.dataart.dartcard.presentation.main.adapter.fragment

import android.support.v4.app.Fragment
import com.dataart.dartcard.presentation.main.fragment.GlobalSearchFragment
import com.dataart.dartcard.presentation.main.fragment.MoreFragment
import com.dataart.dartcard.presentation.main.fragment.cards.CardsTabHostFragment


public enum class MainFragmentTab(val value: Int) {
    CARDS(0), SEARCH(1), MORE(2);

    val fragment: Fragment
        get() {
            return when (this) {
                CARDS -> CardsTabHostFragment()
                SEARCH -> GlobalSearchFragment()
                MORE -> MoreFragment()
            }
        }

    companion object {

        fun getByPosition(position: Int): MainFragmentTab {
            for (tab in values()) {
                if (tab.value == position) {
                    return tab
                }
            }
            throw IllegalArgumentException("No such tab")
        }
    }
}