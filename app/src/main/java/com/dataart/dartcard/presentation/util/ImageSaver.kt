package com.dataart.dartcard.presentation.util

import android.content.Context
import android.content.ContextWrapper
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import java.io.*

class ImageSaver(private val filename: String, val context: Context) {

    fun saveToInternalStorage(bitmapImage: Bitmap): String {
        // Create imageDir
        val imageFile = createFile()

        val fos = FileOutputStream(imageFile)
        try {
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.JPEG, QUALITY, fos)
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                fos.flush()
                fos.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
        return imageFile.absolutePath
    }

    private fun createFile(): File {
        val cw = ContextWrapper(context)
        val directory = cw.getDir(DIRECTORY_NAME, Context.MODE_PRIVATE)

        if (!directory.exists() && !directory.mkdirs()) {
            Log.e("ImageSaver", "Error creating directory $directory")
        }

        return File(directory, filename)
    }

    fun getImageFromStorage(path: String): Bitmap? {
        return try {
            val f = File(path)
            BitmapFactory.decodeStream(FileInputStream(f))

        } catch (e: FileNotFoundException) {
            e.printStackTrace()
            null
        }

    }

    companion object {

        private const val DIRECTORY_NAME = "shared_cards_zion"

        private const val QUALITY = 100
    }
}
