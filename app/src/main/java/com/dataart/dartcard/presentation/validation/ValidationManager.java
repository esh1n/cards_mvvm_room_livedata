package com.dataart.dartcard.presentation.validation;

import com.dataart.dartcard.presentation.validation.ValidationField.Type;
import com.dataart.dartcard.presentation.validation.ValidationField.ValidationResultAction;
import com.dataart.dartcard.presentation.validation.validator.Validator;
import com.dataart.dartcard.presentation.validation.validator.Validator.ValidationResult;
import com.dataart.dartcard.presentation.validation.validator.ValidatorFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;


public class ValidationManager {
    private HashMap<Type, ValidationResultAction> errorActions;

    public ValidationManager() {
        errorActions = new HashMap<>();
    }

    public boolean isValid() {
        return getErrorActions().isEmpty();
    }

    @NonNull
    public List<ValidationField.ValidationResultAction> getErrorActions() {
        return new ArrayList<>(errorActions.values());
    }

    public void showResultActions() {
        for (ValidationField.ValidationResultAction resultAction : getErrorActions()) {
            resultAction.call();
        }
    }

    public Observable<ValidationResultAction> validateSource(
            Observable<ValidationField> validationSource,
            boolean skipInitialValue) {
        return validationSource
                .map(this::validateField)
                .skip(skipInitialValue ? 1 : 0);
    }

    @NonNull
    public ValidationResultAction validateField(ValidationField field) {
        ValidationResultAction resultAction;

        Validator.ValidationResult validationResult = validate(field);


        if (validationResult == ValidationResult.FAILED) {
            resultAction = field.getValidationErrorAction();
        } else if (validationResult == ValidationResult.EMPTY) {
            resultAction = field.getEmptyInputAction();
        } else {
            resultAction = field.getSuccessValidationAction();
        }

        synchronized (this) {
            if (validationResult != ValidationResult.PASSED) {
                errorActions.put(field.getType(), resultAction);
            } else {
                errorActions.remove(field.getType());
            }
        }

        return resultAction;
    }

    public ValidationResult validate(ValidationField field) {
        return ValidatorFactory.getValidator(field.getType())
                .validate(field.getInput());
    }
}
