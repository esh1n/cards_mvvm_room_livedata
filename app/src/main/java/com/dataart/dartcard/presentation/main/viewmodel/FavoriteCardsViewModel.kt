package com.dataart.dartcard.presentation.main.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import com.birbit.android.jobqueue.JobManager
import com.dataart.dartcard.domain.SchedulersFacade
import com.dataart.dartcard.domain.favorite.UserCardRepository
import com.dataart.dartcard.domain.jobmanager.startSync
import com.dataart.dartcard.presentation.main.mapper.CardEntityMapper
import com.dataart.dartcard.presentation.main.models.CardModel
import io.reactivex.Completable

class FavoriteCardsViewModel internal constructor(private val favsRepository: UserCardRepository,
                                                  private val jobManager: JobManager)
    : SearchBrandViewModel<List<CardModel>>() {

    private val cardEntityMapper: CardEntityMapper = CardEntityMapper()


    fun unFavorite(cardId: String) {
        addDisposable(
                favsRepository.unFavoriteCard(cardId)
                        .compose(SchedulersFacade.applySchedulersCompletable())
                        .doOnSubscribe { _ -> loadingStatus.setValue(true) }
                        .doAfterTerminate { loadingStatus.setValue(false) }
                        .subscribe({ }, { throwable -> error.setValue(throwable) }))
    }

    override fun searchExactQuery(query: String): LiveData<List<CardModel>> {
        return Transformations.map(favsRepository.getFavoritesCardsFromCache(query)) { cardEntityMapper.map(it) }
    }

    fun refreshCards() {
        addDisposable(
                Completable.fromCallable { jobManager.startSync() }
                .compose(SchedulersFacade.applySchedulersCompletable())
                .doOnSubscribe { _ -> loadingStatus.setValue(true) }
                .doAfterTerminate { loadingStatus.setValue(false) }
                .subscribe({ }, { throwable -> error.setValue(throwable) }))
    }
}
