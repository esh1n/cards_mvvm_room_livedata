package com.dataart.dartcard.presentation.main.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.paging.PagedList
import com.birbit.android.jobqueue.JobManager
import com.dataart.dartcard.data.room.entity.BrandWithPhoto
import com.dataart.dartcard.domain.SchedulersFacade
import com.dataart.dartcard.domain.brands.BrandsRepository
import com.dataart.dartcard.domain.jobmanager.startSync
import io.reactivex.Completable

class BrandsViewModel internal constructor(private val brandsRepository: BrandsRepository,
                                           private val jobManager: JobManager)
    : SearchBrandViewModel<PagedList<BrandWithPhoto>>() {

    override fun searchExactQuery(query: String): LiveData<PagedList<BrandWithPhoto>> {
        return brandsRepository.getBrands(query)
    }

    fun refreshBrands() {
        addDisposable(
                Completable.fromCallable { jobManager.startSync() }
                .compose(SchedulersFacade.applySchedulersCompletable())
                .doOnSubscribe { _ -> loadingStatus.setValue(true) }
                .doAfterTerminate { loadingStatus.setValue(false) }
                .subscribe({ },
                        { throwable -> error.setValue(throwable) }))
    }
}
