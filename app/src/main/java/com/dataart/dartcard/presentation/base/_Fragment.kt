package com.dataart.dartcard.presentation.base

import android.support.v4.app.Fragment
import com.dataart.dartcard.presentation.util.hideKeyboard

fun Fragment.hideKeyboard() {
    view?.let { hideKeyboard(activity, it.windowToken) }
}

fun Fragment.setTitle(title: String?) {
    (activity as BaseToolbarActivity).setABTitle(title)
}



