package com.dataart.dartcard.presentation.main.fragment.cards

import android.arch.lifecycle.Observer
import com.dataart.dartcard.R
import com.dataart.dartcard.data.isNotNullOrEmpty
import com.dataart.dartcard.presentation.base.addFragmentToStack
import com.dataart.dartcard.presentation.main.adapter.CardClickHandler
import com.dataart.dartcard.presentation.main.adapter.CardsAdapter
import com.dataart.dartcard.presentation.main.fragment.details.CardDetailFragment
import com.dataart.dartcard.presentation.main.models.CardModel
import com.dataart.dartcard.presentation.main.viewmodel.MyCardsViewModel

class MyCardsFragment : HomeSearchFragment<MyCardsViewModel, CardsAdapter>() {

    private val clickHandler = object : CardClickHandler {
        override fun manageFavorite(cardModel: CardModel) {
            viewModel.manageFavoriteState(cardModel)
        }

        override fun openDetails(cardModel: CardModel) {
            val fragment = CardDetailFragment.newInstance(cardModel.id)
            activity.addFragmentToStack(fragment)
        }
    }

    override val viewModelClass = MyCardsViewModel::class.java


    override val layoutResource = R.layout.fragment_home

    override val adapterImplementation = CardsAdapter(clickHandler)


    override fun observeItemsByName(searchQuery: String) {
        viewModel.searchContainsQuery(searchQuery).observe(this, Observer<List<CardModel>> { this.swapCardsInUI(it) })
    }

    private fun swapCardsInUI(cardModels: List<CardModel>?) {
        adapter.swapCards(cardModels.orEmpty())
        if (cardModels.isNotNullOrEmpty()) {
            hideEmptyView()
        } else {
            showEmptyView()
        }
    }

    override fun refresh() {
        viewModel.refreshMyCards()
    }

    override val emptyTextViewId = R.string.empty_text_no_my_cards
}