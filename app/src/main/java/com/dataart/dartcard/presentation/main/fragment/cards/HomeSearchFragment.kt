package com.dataart.dartcard.presentation.main.fragment.cards

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.support.design.widget.FloatingActionButton
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.animation.LinearInterpolator
import android.view.animation.OvershootInterpolator
import com.dataart.dartcard.R
import com.dataart.dartcard.presentation.addedit.activity.AddEditCardActivity
import com.dataart.dartcard.presentation.base.SearchFragment
import com.dataart.dartcard.presentation.base.viewmodel.BaseLoadableViewModel
import com.dataart.dartcard.presentation.main.viewmodel.MainViewModelFactory
import javax.inject.Inject

abstract class HomeSearchFragment<VM : BaseLoadableViewModel, A : RecyclerView.Adapter<*>> : SearchFragment<VM, A>() {


    @Inject
    override lateinit var factory: MainViewModelFactory

    var fab: FloatingActionButton? = null
    var swipe_refresh_layout: SwipeRefreshLayout? = null;

    override fun onInitView(rootView: View) {
        super.onInitView(rootView)
        fab = rootView.findViewById(R.id.fab_add_task);
        fab?.setOnClickListener { _ -> AddEditCardActivity.startAdd(requireActivity()) }
        swipe_refresh_layout = rootView.findViewById(R.id.swipe_refresh_layout);
        swipe_refresh_layout!!.setOnRefreshListener { this.refresh() }
        recyclerView?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {

                when (newState) {
                    RecyclerView.SCROLL_STATE_DRAGGING -> {
                        hideFabWithObjectAnimator()
                    }
                    RecyclerView.SCROLL_STATE_IDLE -> {
                        showFabWithObjectAnimator()
                    }
                }
            }
        })
    }

    fun hideFabWithObjectAnimator() {
        val xScaleAnimator = ObjectAnimator.ofFloat<View>(fab, View.SCALE_X, 0f)
        val yScaleAnimator = ObjectAnimator.ofFloat<View>(fab, View.SCALE_Y, 0f)
        with(AnimatorSet()) {
            duration = 200
            interpolator = LinearInterpolator()
            playTogether(xScaleAnimator, yScaleAnimator)
            start()
        }

    }

    fun showFabWithObjectAnimator() {
        val xScaleAnimator = ObjectAnimator.ofFloat<View>(fab, View.SCALE_X, 1f)
        val yScaleAnimator = ObjectAnimator.ofFloat<View>(fab, View.SCALE_Y, 1f)
        with(AnimatorSet()) {
            duration = 400
            interpolator = OvershootInterpolator()
            playTogether(xScaleAnimator, yScaleAnimator)
            start()
        }

    }

    override fun showLoading() {
        if (!swipe_refresh_layout!!.isRefreshing) {
            progressView?.visibility = View.VISIBLE
        }
    }

    override fun hideLoading() {
        if (swipe_refresh_layout!!.isRefreshing) {
            swipe_refresh_layout?.isRefreshing = false
        } else {
            progressView?.visibility = View.GONE
        }
    }

    abstract fun refresh()
}