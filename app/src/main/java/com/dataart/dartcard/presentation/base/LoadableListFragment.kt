package com.dataart.dartcard.presentation.base

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.TextView
import com.dataart.dartcard.R
import com.dataart.dartcard.presentation.base.viewmodel.BaseLoadableViewModel
import com.dataart.dartcard.presentation.util.SnackbarBuilder

abstract class LoadableListFragment<VM : BaseLoadableViewModel, A : RecyclerView.Adapter<*>> : BaseVMFragment<VM>() {

    protected var recyclerView: RecyclerView? = null

    protected var progressView: View? = null

    protected var emptyView: TextView? = null

    protected lateinit var adapter: A

    @get:LayoutRes
    abstract override val layoutResource: Int

    abstract val adapterImplementation: A

    protected open val emptyTextViewId = R.string.empty_text_no_cards

    abstract fun load()

    override fun setupView(rootView: View) {
        super.setupView(rootView)
        recyclerView = rootView.findViewById(R.id.recyclerview)
        progressView = rootView.findViewById(R.id.loading_indicator)
        emptyView = rootView.findViewById(R.id.view_empty)
        emptyView?.setText(emptyTextViewId)
        onInitView(rootView)
        onSetupRecyclerView(recyclerView)
    }

    protected open fun onInitView(rootView: View) {}

    private fun observeLoadingStatus() {
        viewModel.loadingStatus.observe(this, Observer { loading ->
            if (loading!!) {
                showLoading()
            } else {
                hideLoading()
            }
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeLoadingStatus()
    }

    override fun onStart() {
        super.onStart()
        load()
    }

    protected fun onSetupRecyclerView(recyclerView: RecyclerView?) {
        recyclerView?.let {
            val linearLayoutManager = LinearLayoutManager(activity)
            it.layoutManager = linearLayoutManager
            it.setHasFixedSize(true)
            val dividerItemDecoration = DividerItemDecoration(activity!!, linearLayoutManager.orientation)
            it.addItemDecoration(dividerItemDecoration)
            adapter = adapterImplementation
            it.adapter = adapter
        }

    }

    override fun onError(t: Throwable) {
        super.onError(t)
        hideLoading()

        Log.e(javaClass.name, t.message)

        SnackbarBuilder.buildErrorSnack(
                view!!,
                getString(R.string.error_unknown),
                getString(R.string.text_retry), View.OnClickListener { _ -> load() }).show();
    }

    protected open fun showLoading() {
        progressView?.visibility = View.VISIBLE
    }

    protected open fun hideLoading() {
        progressView?.visibility = View.INVISIBLE
    }

    protected open fun showEmptyView() {
        emptyView?.visibility = View.VISIBLE
    }

    protected fun hideEmptyView() {
        emptyView?.visibility = View.GONE
    }
}
