package com.dataart.dartcard.presentation.main.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.birbit.android.jobqueue.JobManager
import com.dataart.dartcard.data.cache.CacheContext
import com.dataart.dartcard.data.job.RxJobResultBus
import com.dataart.dartcard.domain.brands.BrandsRepository
import com.dataart.dartcard.domain.cards.CardsRepository
import com.dataart.dartcard.domain.favorite.UserCardRepository


class MainViewModelFactory(private val cardsRepository: CardsRepository, private val brandsRepository: BrandsRepository,
                           private val userCardRepository: UserCardRepository,
                           private val jobManager: JobManager,
                           private val cacheContext: CacheContext,
                           private val rxJobResultBus: RxJobResultBus) : ViewModelProvider.Factory {


    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(FavoriteCardsViewModel::class.java)) {
            return FavoriteCardsViewModel(userCardRepository, jobManager) as T
        }

        if (modelClass.isAssignableFrom(BrandsViewModel::class.java)) {
            return BrandsViewModel(brandsRepository, jobManager) as T
        }

        if (modelClass.isAssignableFrom(CardDetailsViewModel::class.java)) {
            return CardDetailsViewModel(cardsRepository, userCardRepository) as T
        }

        if (modelClass.isAssignableFrom(BrandDetailsViewModel::class.java)) {
            return BrandDetailsViewModel(brandsRepository, cardsRepository, userCardRepository) as T
        }

        if (modelClass.isAssignableFrom(MyCardsViewModel::class.java)) {
            return MyCardsViewModel(cardsRepository, userCardRepository, jobManager) as T
        }

        if (modelClass.isAssignableFrom(GlobalSearchViewModel::class.java)) {
            return GlobalSearchViewModel(cardsRepository, userCardRepository) as T
        }

        if (modelClass.isAssignableFrom(BarCodeViewModel::class.java)) {
            return BarCodeViewModel(cardsRepository) as T
        }

        if (modelClass.isAssignableFrom(MoreViewModel::class.java)) {
            return MoreViewModel(cacheContext) as T
        }

        if (modelClass.isAssignableFrom(RootContainerViewModel::class.java)) {
            return RootContainerViewModel(rxJobResultBus) as T
        }

        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
