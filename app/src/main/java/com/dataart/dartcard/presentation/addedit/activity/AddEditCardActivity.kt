package com.dataart.dartcard.presentation.addedit.activity

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import com.dataart.dartcard.R
import com.dataart.dartcard.presentation.addedit.fragment.AddEditCardFragment
import com.dataart.dartcard.presentation.addedit.fragment.EditCardFragment
import com.dataart.dartcard.presentation.base.SingleFragmentActivity
import com.dataart.dartcard.presentation.base.setABTitle
import com.dataart.dartcard.presentation.base.startActivity

class AddEditCardActivity : SingleFragmentActivity() {

    override fun getStartScreen(savedInstanceState: Bundle?): Fragment {
        return if (cardId.isNullOrBlank()) {
            AddEditCardFragment.newInstance()
        } else {
            EditCardFragment.newInstance(cardId!!)
        }
    }

    private var cardId: String? = null


    override fun onReadArgs(args: Bundle?) {
        cardId = args?.getString(CARD_ID_KEY)
        setABTitle(getString(if (cardId.isNullOrBlank()) R.string.title_add_card else R.string.title_edit_card))
    }

    override val isDisplayHomeAsUpEnabled = true

    companion object {

        const val CARD_ID_KEY = "CARD_ID_KEY"

        fun startEdit(context: Context?, cardId: String? = null) {
            context.startActivity<AddEditCardActivity>(Bundle()
                    .apply { putString(CARD_ID_KEY, cardId) })
        }

        fun startAdd(activity: Activity) {
            startEdit(activity)
        }
    }
}
