package com.dataart.dartcard.presentation.widget

import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Parcel
import android.os.Parcelable
import android.support.annotation.StringRes
import android.text.InputFilter
import android.text.InputType
import android.text.TextUtils
import android.util.AttributeSet
import android.view.View
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.TextView
import com.dataart.dartcard.R
import com.dataart.dartcard.databinding.ViewEditTextValidatedBinding
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.Observable

class ValidatedEditText : FrameLayout {
    private lateinit var binding: ViewEditTextValidatedBinding

    val focusChangesSource: Observable<Boolean>
        get() = RxView.focusChanges(binding.editText)

    val textChangesSource: Observable<String>
        get() = Observable.defer {
            RxTextView.textChangeEvents(binding.editText)
                    .map { value -> value.view().text.toString() }
        }

    /**
     * @param error set error hint message; if null - hide error message
     */
    var error: String?
        get() {
            val error = binding.inputLayout.error
            return if (TextUtils.isEmpty(error)) "" else error!!.toString()
        }
        set(error) {
            if (error == null) {
                hideError()
                return
            }

            binding.inputLayout.error = error
        }

    val isErrorShown: Boolean
        get() = !TextUtils.isEmpty(binding.inputLayout.error)

    var text: String
        get() = binding.editText.text.toString()
        set(text) {
            binding.editText.setText(text)
            if (!TextUtils.isEmpty(text)) {
                setSelectionToEnd()
                binding.editText.requestFocusFromTouch()
            }
        }

    constructor(context: Context) : super(context) {
        initView(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initView(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initView(context, attrs)
    }

    private fun initView(context: Context, attrs: AttributeSet?) {
        if (isInEditMode) {
            return
        }

        binding = DataBindingUtil.inflate(context.layoutInflater, R.layout.view_edit_text_validated, this, false)

        attrs?.let {
            handleEditTextAttributes(it)
            handleCounterAttributes(it)
        }


        addView(binding.root)
    }


    private fun handleCounterAttributes(attrs: AttributeSet) {
        val isCounterEnabled = attrs.getAttributeBooleanValue(APP_NAMESPACE, "counterEnabled", false)
        binding.inputLayout.isCounterEnabled = isCounterEnabled
        if (isCounterEnabled) {
            val counterMaxLength = getIntAttribute(APP_NAMESPACE, "counterMaxLength", attrs)
            if (counterMaxLength != NOT_SET) {
                binding.inputLayout.counterMaxLength = counterMaxLength
                setMaxLength(counterMaxLength)
            }
        }
    }

    private fun handleEditTextAttributes(attrs: AttributeSet) {

        getStringAttribute(NAMESPACE, "text", attrs)?.let { this.text = it }

        getStringAttribute(NAMESPACE, "hint", attrs)?.let { setHint(it) }

        val maxLength = getIntAttribute(NAMESPACE, "maxLength", attrs)
        if (maxLength != NOT_SET) {
            setMaxLength(maxLength)
        }

        val lines = getIntAttribute(NAMESPACE, "lines", attrs)
        if (lines != NOT_SET) {
            binding.editText.setLines(lines)
        }

        val inputType = attrs.getAttributeIntValue(NAMESPACE, "inputType", NOT_SET)
        if (inputType != NOT_SET) {
            binding.editText.inputType = inputType

            if (binding.editText.inputType == InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD) {
                binding.inputLayout.isPasswordVisibilityToggleEnabled = true
            }
        }
    }

    fun setInputFilter(inputFilter: InputFilter) {
        binding.editText.filters = arrayOf(inputFilter)
    }

    override fun setOnClickListener(clickListener: View.OnClickListener?) {
        super.setOnClickListener(clickListener)

        binding.editText.setOnClickListener(clickListener)
    }

    override fun setFocusable(focusable: Boolean) {
        super.setFocusable(focusable)
        binding.editText.isFocusable = focusable
        binding.editText.isFocusableInTouchMode = focusable
    }

    override fun clearFocus() {
        super.clearFocus()
        binding.editText.clearFocus()
    }

    override fun onSaveInstanceState(): Parcelable? {
        val superState = super.onSaveInstanceState()
        val ss = SavedState(superState)
        ss.text = text
        ss.errorText = this.error ?: ""
        ss.isErrorShown = isErrorShown

        return ss
    }

    override fun onRestoreInstanceState(state: Parcelable) {
        val ss = state as SavedState
        super.onRestoreInstanceState(ss.superState)
        text = ss.text
        error = if (ss.isErrorShown) ss.errorText else null
    }

    fun setError(@StringRes error: Int) {
        this.error = context.getString(error)
    }

    private fun hideError() {
        binding.inputLayout.error = null
    }

    fun setText(@StringRes text: Int) {
        binding.editText.setText(text)
    }

    fun setHint(hint: String) {
        binding.inputLayout.hint = hint
    }

    private fun getIntAttribute(namespace: String, attrName: String, attrs: AttributeSet): Int {
        val resourceValue = attrs.getAttributeResourceValue(namespace, attrName, NOT_SET)
        return if (resourceValue != NOT_SET) {
            resources.getInteger(resourceValue)
        } else attrs.getAttributeIntValue(namespace, attrName, NOT_SET)

    }

    private fun getStringAttribute(namespace: String, attrName: String, attrs: AttributeSet): String? {
        val resourceValue = attrs.getAttributeResourceValue(namespace, attrName, NOT_SET)
        return if (resourceValue != NOT_SET) {
            resources.getString(resourceValue)
        } else {
            attrs.getAttributeValue(namespace, attrName)
        }

    }

    private fun setMaxLength(maxLength: Int) {
        val filterArray = arrayOfNulls<InputFilter>(1)
        filterArray[0] = InputFilter.LengthFilter(maxLength)
        binding.editText.filters = filterArray
    }

    /**
     * trim text and set trimmed version
     *
     * @return trimmed text
     */
    fun trimText(): String {
        val text = text
        val trimmed = text.trim { it <= ' ' }
        if (trimmed != text) {
            this.text = trimmed
        }

        return trimmed
    }

    fun setOnEditorActionListener(listener: TextView.OnEditorActionListener) {
        binding.editText.setOnEditorActionListener(listener)
    }

    fun setSelectionToEnd() {
        val selection = binding.editText.text?.length ?: 0
        binding.editText.setSelection(selection)
    }

    internal class SavedState : View.BaseSavedState {
        var text: String = ""
        var errorText: String = ""
        var isErrorShown: Boolean = false

        constructor(superState: Parcelable) : super(superState) {}

        private constructor(`in`: Parcel) : super(`in`) {
            text = `in`.readString()
            errorText = `in`.readString()
            isErrorShown = `in`.readByte().toInt() == 1
        }

        override fun writeToParcel(out: Parcel, flags: Int) {
            super.writeToParcel(out, flags)
            out.writeString(text)
            out.writeString(errorText)
            out.writeByte((if (isErrorShown) 1 else 0).toByte())
        }

        companion object {
            val CREATOR: Parcelable.Creator<SavedState> = object : Parcelable.Creator<SavedState> {
                override fun createFromParcel(`in`: Parcel): SavedState {
                    return SavedState(`in`)
                }

                override fun newArray(size: Int): Array<SavedState?> {
                    return arrayOfNulls(size)
                }
            }
        }
    }

    companion object {
        private val NAMESPACE = "http://schemas.android.com/apk/res/android"
        private val APP_NAMESPACE = "http://schemas.android.com/apk/res-auto"
        private val NOT_SET = -1

        fun getTextChangesSource(editText: EditText): Observable<String> {
            return Observable.defer {
                RxTextView.textChangeEvents(editText)
                        .map { value -> value.view().text.toString() }
            }
        }
    }

}
