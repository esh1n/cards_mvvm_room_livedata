package com.dataart.dartcard.presentation.main.adapter.fragment

import android.support.annotation.StringRes
import android.support.v4.app.Fragment

import com.dataart.dartcard.R
import com.dataart.dartcard.presentation.main.fragment.cards.BrandsFragment
import com.dataart.dartcard.presentation.main.fragment.cards.FavoriteCardsFragment
import com.dataart.dartcard.presentation.main.fragment.cards.MyCardsFragment

internal enum class CardsTab(private val value: Int) {
    FAVORITES(0), MY(1), ALL(2);

    val titleResourceId: Int
        @StringRes
        get() {
            return when (this) {
                FAVORITES -> R.string.title_favorites
                MY -> R.string.title_my_cards
                ALL -> R.string.title_types
            }
        }

    val fragment: Fragment
        get() {
            return when (this) {
                FAVORITES -> FavoriteCardsFragment()
                MY -> MyCardsFragment()
                ALL -> BrandsFragment()
            }
        }

    companion object {

        fun getByPosition(position: Int): CardsTab {
            for (tab in values()) {
                if (tab.value == position) {
                    return tab
                }
            }
            throw IllegalArgumentException("No such tab")
        }
    }
}