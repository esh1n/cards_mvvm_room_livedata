package com.dataart.dartcard.presentation.start


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle

import com.dataart.dartcard.R
import com.dataart.dartcard.presentation.auth.AuthActivity
import com.dataart.dartcard.presentation.auth.viewmodel.AuthViewModelFactory
import com.dataart.dartcard.presentation.base.BaseDIActivity
import com.dataart.dartcard.presentation.main.CardsActivity

import javax.inject.Inject

class StartActivity : BaseDIActivity() {

    @Inject
    internal lateinit var factory: AuthViewModelFactory

    private lateinit var viewModel: StartScreenViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_container)
        viewModel = ViewModelProviders.of(this, factory).get(StartScreenViewModel::class.java)
        observeNavigateEvents()
        viewModel.chooseNavigation()
    }

    private fun observeNavigateEvents() {
        viewModel.isUserAuthorized.observe(this, Observer { isAuthorized ->
            if (isAuthorized!!) {
                CardsActivity.start(this)
            } else {
                AuthActivity.start(this)
            }
        }
        )
    }
}
