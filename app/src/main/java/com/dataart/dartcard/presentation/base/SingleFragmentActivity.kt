package com.dataart.dartcard.presentation.base

import android.os.Bundle
import android.support.v4.app.Fragment

abstract class SingleFragmentActivity : BaseFragmentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onInitContent(savedInstanceState)
    }


    protected fun onInitContent(savedInstanceState: Bundle?) {
        val startScreen = getStartScreen(savedInstanceState)
        if (startScreen != null) {
            this.addFragment(startScreen)
        }
    }

    protected abstract fun getStartScreen(savedInstanceState: Bundle?): Fragment?
}
