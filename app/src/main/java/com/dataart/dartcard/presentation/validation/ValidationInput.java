package com.dataart.dartcard.presentation.validation;

/**
 * Created by atselkovsky on 04.10.17.
 */

public class ValidationInput {
    private Object input;
    private boolean isMandatory;

    public ValidationInput(Object input, boolean isMandatory) {
        this.input = input;
        this.isMandatory = isMandatory;
    }

    public Object getInput() {
        return input;
    }

    public boolean isMandatory() {
        return isMandatory;
    }
}