package com.dataart.dartcard.presentation.main.adapter

import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.ViewGroup
import com.dataart.dartcard.R
import com.dataart.dartcard.databinding.ItemCardFavoriteBinding
import com.dataart.dartcard.presentation.main.models.CardModel
import com.dataart.dartcard.presentation.util.ImageLoader.loadImage
import com.dataart.dartcard.presentation.widget.layoutInflater
import com.squareup.picasso.Picasso


class CardsAdapter(private val clickHandler: CardClickHandler) : RecyclerView.Adapter<CardsAdapter.ViewHolder>() {
    private var cardModels: List<CardModel>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = parent.context.layoutInflater.inflate(R.layout.item_card_favorite, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return cardModels?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val cardModel = cardModels?.get(position)
        if (cardModel != null) {
            holder.bindTo(cardModel)
        } else {
            holder.clear()
        }
    }


    fun swapCards(cardModels: List<CardModel>) {
        if (this.cardModels == null) {
            this.cardModels = cardModels
            notifyDataSetChanged()
        } else {
            val result = DiffUtil.calculateDiff(DiffCallback(this.cardModels!!, cardModels))
            this.cardModels = cardModels
            result.dispatchUpdatesTo(this)
        }
    }

    private inner class DiffCallback(private val oldCards: List<CardModel>, private val newCards: List<CardModel>) : DiffUtil.Callback() {

        override fun getOldListSize(): Int {
            return oldCards.size
        }

        override fun getNewListSize(): Int {
            return newCards.size
        }

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val (id) = oldCards[oldItemPosition]
            val (id1) = newCards[newItemPosition]
            return id == id1
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldBrand = oldCards[oldItemPosition]
            val newBrand = newCards[newItemPosition]
            return oldBrand == newBrand
        }
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private val binding: ItemCardFavoriteBinding?

        init {
            binding = DataBindingUtil.bind(itemView)

            binding!!.imvFavoriteCard.setOnClickListener(this)
            itemView.setOnClickListener(this)
        }

        fun bindTo(cardModel: CardModel) {
            binding!!.card = cardModel
            Picasso.get().loadImage(binding.appCompatImageView, cardModel.photo)
            Log.d("image", "bind  to " + cardModel.id)
        }

        fun clear() {
            binding?.card = null
        }

        override fun onClick(v: View) {
            val cardModel = cardModels?.get(adapterPosition)
            cardModel?.let {
                if (v.id == R.id.imv_favorite_card) {
                    clickHandler.manageFavorite(cardModel)
                } else {
                    clickHandler.openDetails(cardModel)
                }
            }

        }
    }

}