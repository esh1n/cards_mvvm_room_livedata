package com.dataart.dartcard.presentation.addedit.model

import com.dataart.dartcard.presentation.main.models.BrandModel

class AddEditCardModel(val id: String? = null, val serverId: String? = null) {

    var brandModel: BrandModel? = null

    var barCode: BarCodeModel? = null

    var facePhoto: String? = null

    var backPhoto: String? = null

    var note: String? = null

    var cardNumber: String? = null

    var discount: Int = 0

    var facePhotoUrl: String? = null

    var backPhotoUrl: String? = null

    var isPrivate: Boolean = false
}
