package com.dataart.dartcard.presentation.base.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.LiveDataReactiveStreams
import com.dataart.dartcard.data.job.JobResult
import com.dataart.dartcard.data.job.ReduceThrottle
import com.dataart.dartcard.data.job.RxJobResultBus
import com.dataart.dartcard.data.retrofit.RefreshTokenException
import io.reactivex.BackpressureStrategy
import java.util.concurrent.TimeUnit

abstract class BaseSessionViewModel(rxJobResultBus: RxJobResultBus) : BaseLoadableViewModel() {


    val logoutEvent: LiveData<JobResult>


    init {
        val logoutFlowable = rxJobResultBus.events
                .filter { it.error is RefreshTokenException }
                .toFlowable(BackpressureStrategy.DROP)
                .compose(ReduceThrottle(1, TimeUnit.SECONDS))
                .take(1)
        logoutEvent = LiveDataReactiveStreams.fromPublisher(logoutFlowable)
    }
}
