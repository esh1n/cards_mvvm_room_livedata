package com.dataart.dartcard.presentation.auth

import android.arch.lifecycle.Observer
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Handler
import android.view.View
import com.dataart.dartcard.R
import com.dataart.dartcard.databinding.FragmentAuthBinding
import com.dataart.dartcard.presentation.auth.viewmodel.AuthViewModel
import com.dataart.dartcard.presentation.auth.viewmodel.AuthViewModelFactory
import com.dataart.dartcard.presentation.base.BaseVMFragment
import com.dataart.dartcard.presentation.main.CardsActivity
import com.dataart.dartcard.presentation.util.DiscoColors.colorizeDisco
import com.dataart.dartcard.presentation.util.GoogleSignInUtil
import com.dataart.dartcard.presentation.util.SnackbarBuilder
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import javax.inject.Inject

class AuthFragment : BaseVMFragment<AuthViewModel>() {


    @Inject
    override lateinit var factory: AuthViewModelFactory

    private var binding: FragmentAuthBinding? = null

    private var googleSignInClient: GoogleSignInClient? = null

    override val viewModelClass = AuthViewModel::class.java

    override val layoutResource = R.layout.fragment_auth

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeNavigateEvent()
        observeLoadingStatus()
        signOut()
    }

    private fun signOut() {
        val activity = requireActivity();
        GoogleSignInUtil.getGoogleClient(activity).signOut()
                .addOnCompleteListener(activity) { _ -> viewModel.logout() }
    }

    private fun observeLoadingStatus() {
        viewModel.loadingStatus.observe(this, Observer<Boolean> { processLoadingStatus(it) })
    }

    private fun processLoadingStatus(isLoading: Boolean?) {
        binding?.let {
            it.loadingIndicator.visibility = if (isLoading == true) View.VISIBLE else View.GONE
        }
    }

    public override fun onError(t: Throwable) {
        super.onError(t)
        SnackbarBuilder.buildErrorSnack(view!!, t.message ?: "error").show()
    }

    private fun observeNavigateEvent() {
        viewModel.onLoggedInEvent.observe(this, Observer { _ ->
            SnackbarBuilder.buildSnack(view!!, "Logged in.Starting sync").show()
            val activity = requireActivity()
            CardsActivity.start(activity)
            activity.finish()

        })
        viewModel.onLoggedOutEvent.observe(this, Observer { _ ->
            Handler().post { binding?.btnSignIn?.isEnabled = true }
            googleSignInClient = GoogleSignInUtil.getGoogleClient(requireActivity())
        })
    }

    override fun setupView(rootView: View) {
        binding = DataBindingUtil.bind(rootView)
        binding?.let {
            it.tvBrandTitle.text = requireActivity().colorizeDisco(true)
            it.btnSignIn.setOnClickListener { _ -> GoogleSignInUtil.signInExplicitly(this, googleSignInClient!!) }
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        GoogleSignInUtil.processSignInResult(requestCode, data, GoogleSignInAuthProcessor())
    }

    private inner class GoogleSignInAuthProcessor : GoogleSignInUtil.AuthProcessor {

        override fun onGetAuthCode(code: String?, email: String?) {
            if (code.isNullOrBlank()) {
                SnackbarBuilder.buildErrorSnack(view!!, getString(R.string.error_signin_no_server_code)).show();
            } else {
                viewModel.login(code!!, email!!)
            }

        }

        override fun onError(message: Int?) {
            val error = message ?: R.string.error_unknown_fail;
            SnackbarBuilder.buildErrorSnack(view!!, getString(error)).show()
        }
    }
}

