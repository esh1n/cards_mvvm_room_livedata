package com.dataart.dartcard.presentation.addedit.viewmodel

import android.arch.lifecycle.MutableLiveData
import com.dataart.dartcard.domain.SchedulersFacade
import com.dataart.dartcard.presentation.addedit.model.BarCodeModel
import com.dataart.dartcard.presentation.base.viewmodel.BaseViewModel
import com.dataart.dartcard.presentation.validation.ValidationField
import com.dataart.dartcard.presentation.validation.ValidationManager
import com.google.zxing.BarcodeFormat
import io.reactivex.Observable

class BarCodeInputViewModel : BaseViewModel() {


    private val validationManager: ValidationManager = ValidationManager()

    val barCodeLiveData = MutableLiveData<BarCodeModel>()

    fun validateFields(validationSource: Observable<ValidationField>) {
        addDisposable(
                validationManager.validateSource(validationSource, true)
                        .compose<ValidationField.ValidationResultAction>(SchedulersFacade.applySchedulersObservable<ValidationField.ValidationResultAction>())
                        .subscribe({ it.call() }, { errorValue -> error.setValue(errorValue) })
        )
    }

    fun tryToStoreBarCode(barCode: String, format: BarcodeFormat) {
        if (!validationManager.isValid) {
            validationManager.showResultActions()
            return
        }
        val barCodeModel = BarCodeModel(barCode, format.name)
        barCodeLiveData.value = barCodeModel
    }
}
