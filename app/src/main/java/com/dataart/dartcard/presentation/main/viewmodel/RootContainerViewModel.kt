package com.dataart.dartcard.presentation.main.viewmodel

import com.dataart.dartcard.data.job.RxJobResultBus
import com.dataart.dartcard.presentation.base.viewmodel.BaseSessionViewModel

class RootContainerViewModel(rxJobResultBus: RxJobResultBus) : BaseSessionViewModel(rxJobResultBus) {
}