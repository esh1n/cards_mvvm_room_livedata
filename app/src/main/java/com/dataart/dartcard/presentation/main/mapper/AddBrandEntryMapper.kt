package com.dataart.dartcard.presentation.main.mapper

import com.dataart.dartcard.data.room.entity.BrandEntry
import com.dataart.dartcard.domain.mapper.Mapper
import com.dataart.dartcard.presentation.main.models.BrandModel

class AddBrandEntryMapper : Mapper<BrandEntry, BrandModel>() {
    override fun map(source: BrandEntry): BrandModel {
        return BrandModel(source.name, synced = source.synced)
    }
}
