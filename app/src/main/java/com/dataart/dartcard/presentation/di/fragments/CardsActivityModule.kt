package com.dataart.dartcard.presentation.di.fragments


import com.dataart.dartcard.presentation.di.module.ViewModelsModule
import com.dataart.dartcard.presentation.main.fragment.GlobalSearchFragment
import com.dataart.dartcard.presentation.main.fragment.MainFragment
import com.dataart.dartcard.presentation.main.fragment.MoreFragment
import com.dataart.dartcard.presentation.main.fragment.cards.BrandsFragment
import com.dataart.dartcard.presentation.main.fragment.cards.FavoriteCardsFragment
import com.dataart.dartcard.presentation.main.fragment.cards.MyCardsFragment
import com.dataart.dartcard.presentation.main.fragment.details.BarcodeFragment
import com.dataart.dartcard.presentation.main.fragment.details.BrandDetailsFragment
import com.dataart.dartcard.presentation.main.fragment.details.CardDetailFragment

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface CardsActivityModule {

    @ContributesAndroidInjector(modules = [ViewModelsModule::class])
    fun contributeBrandsFragment(): BrandsFragment

    @ContributesAndroidInjector(modules = [ViewModelsModule::class])
    fun contributeCardsFragment(): FavoriteCardsFragment

    @ContributesAndroidInjector(modules = [ViewModelsModule::class])
    fun contributeCardDetailFragment(): CardDetailFragment

    @ContributesAndroidInjector(modules = [ViewModelsModule::class])
    fun contributeBrandDetailsFragment(): BrandDetailsFragment

    @ContributesAndroidInjector(modules = [ViewModelsModule::class])
    fun contributeMyCardsFragment(): MyCardsFragment

    @ContributesAndroidInjector(modules = [ViewModelsModule::class])
    fun contributeGlobalSearchFragment(): GlobalSearchFragment

    @ContributesAndroidInjector(modules = [ViewModelsModule::class])
    fun contributeMoreFragment(): MoreFragment

    @ContributesAndroidInjector(modules = [ViewModelsModule::class])
    fun contributeBarcodeFragment(): BarcodeFragment

    @ContributesAndroidInjector(modules = [ViewModelsModule::class])
    fun mainFragment(): MainFragment

}

