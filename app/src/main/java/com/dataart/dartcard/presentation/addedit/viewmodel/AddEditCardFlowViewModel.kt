package com.dataart.dartcard.presentation.addedit.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.dataart.dartcard.presentation.addedit.model.AddEditCardModel
import com.dataart.dartcard.presentation.addedit.model.BarCodeModel
import com.dataart.dartcard.presentation.main.models.BrandModel
import com.dataart.dartcard.presentation.main.models.CardModel

class AddEditCardFlowViewModel : ViewModel() {
    val card = MutableLiveData<AddEditCardModel>()

    fun init() {
        val cardModel = AddEditCardModel()
        card.value = cardModel
    }

    fun selectBrand(newBrandModel: BrandModel) {
        card.value = card.value.apply { this?.brandModel = newBrandModel }
    }

    fun selectBarcode(newBarCodeModel: BarCodeModel) {
        card.value = card.value.apply { this?.barCode = newBarCodeModel }
    }

    fun selectFacePhoto(photoPath: String) {
        card.value = card.value.apply { this?.facePhoto = photoPath }
    }

    fun selectBackPhoto(photoPath: String) {
        card.value = card.value.apply { this?.backPhoto = photoPath }
    }

    fun copyCard(cardFromDB: CardModel, id: String) {

        val copiedCard = AddEditCardModel(id, cardFromDB.serverId).apply {
            discount = cardFromDB.discount
            barCode = BarCodeModel(cardFromDB.barCode, cardFromDB.barCodeFormat)
            cardNumber = cardFromDB.cardNumber
            note = cardFromDB.note
            facePhotoUrl = cardFromDB.photo
            backPhotoUrl = cardFromDB.backPhoto
            this.brandModel = BrandModel(name = cardFromDB.brandName ?: "", synced = false)
        }
        card.value = copiedCard
    }

    fun selectCardNumber(cardNumber: String) {

        card.value = card.value.apply { this?.cardNumber = cardNumber }
    }
}
