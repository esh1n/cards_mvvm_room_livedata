package com.dataart.dartcard.presentation.main.adapter.fragment

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter


class MainFragmentsPagerAdapter(fm: FragmentManager, private val context: Context) : FragmentPagerAdapter(fm) {

    override fun getCount(): Int {
        return MainFragmentTab.values().size
    }

    override fun getItem(position: Int): Fragment {
        val cardsTab = MainFragmentTab.getByPosition(position)
        return cardsTab.fragment
    }

}