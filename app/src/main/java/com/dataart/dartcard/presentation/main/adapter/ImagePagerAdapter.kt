package com.dataart.dartcard.presentation.main.adapter


import android.content.Context
import android.support.annotation.DrawableRes
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.dataart.dartcard.R
import com.dataart.dartcard.presentation.util.ImageLoader.loadImage
import com.dataart.dartcard.presentation.widget.layoutInflater
import com.squareup.picasso.Picasso
import java.util.*

class ImagePagerAdapter(context: Context) : PagerAdapter() {

    private var urls: List<String?> = ArrayList(CAPACITY)
    private val inflater: LayoutInflater = context.layoutInflater
    private val cornerRadius: Int = context.resources.getDimension(R.dimen.card_photo_radius_small).toInt()

    override fun instantiateItem(collection: ViewGroup, position: Int): Any {
        val placeHolder = if (position == 0) R.drawable.ic_card_placeholder else R.drawable.ic_card_placeholder_back
        val rootView = inflater.inflate(R.layout.item_pager_image, collection, false)
        populateView(rootView, getUrl(position), placeHolder)
        collection.addView(rootView)
        return rootView
    }

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }

    override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
        collection.removeView(view as View)
    }

    private fun populateView(rootView: View, url: String?, @DrawableRes placeHolder: Int) {
        val imageView = rootView.findViewById<ImageView>(R.id.imv_card)
        Picasso.get().loadImage(imageView, url, placeHolder, cornerRadius)
    }

    fun refreshData(urls: List<String?>) {
        this.urls = urls
        notifyDataSetChanged()
    }

    override fun getCount(): Int {
        return CAPACITY
    }

    private fun getUrl(position: Int): String? {
        return if (position < urls.size)
            urls[position]
        else
            null

    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    companion object {
        private const val CAPACITY = 2
    }
}
