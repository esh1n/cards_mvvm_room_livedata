package com.dataart.dartcard.presentation.util

import android.graphics.Bitmap
import android.graphics.Matrix
import com.google.zxing.BarcodeFormat
import com.google.zxing.WriterException
import com.journeyapps.barcodescanner.BarcodeEncoder

object BarcodeDecoder {
    fun decode(value: String?, format: String?, width: Int, height: Int): Bitmap? {
        if (value.isNullOrBlank() || format == null) {
            return null
        }
        val parsedFormat = parseBarCodeFormat(format) ?: return null
        val barcodeEncoder = BarcodeEncoder()
        return try {

            barcodeEncoder.encodeBitmap(value, parsedFormat, width, height)
        } catch (e: WriterException) {
            e.printStackTrace()
            null
        }

    }

    fun decodeWithRotate(value: String?, format: String?, rotateDegree: Int, width: Int, height: Int): Bitmap? {
        val decodedBitmap = decode(value, format, width, height);
        val matrix = Matrix()
        matrix.postRotate(rotateDegree.toFloat())
        val inversedWidth = height
        val inversedHeight = width
        val scaledBitmap = Bitmap.createScaledBitmap(decodedBitmap, inversedWidth, inversedHeight, true)
        return Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.width, scaledBitmap.height, matrix, true)
    }

    fun parseBarCodeFormat(value: String): BarcodeFormat? = BarcodeFormat.values().find { it.name == value }

    fun parseBarcodeFormat(value: String?, fallback: BarcodeFormat): BarcodeFormat {
        return BarcodeDecoder.parseBarCodeFormat(value ?: fallback.name) ?: fallback
    }

}



