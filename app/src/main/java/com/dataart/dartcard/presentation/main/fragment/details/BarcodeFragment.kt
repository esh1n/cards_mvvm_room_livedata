package com.dataart.dartcard.presentation.main.fragment.details

import android.app.Dialog
import android.arch.lifecycle.Observer
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import com.dataart.dartcard.R
import com.dataart.dartcard.databinding.FragmentBarcodeFullViewBinding
import com.dataart.dartcard.presentation.base.BaseVMDialogFragment
import com.dataart.dartcard.presentation.main.viewmodel.BarCodeViewModel
import com.dataart.dartcard.presentation.main.viewmodel.MainViewModelFactory
import javax.inject.Inject


class BarcodeFragment : BaseVMDialogFragment<BarCodeViewModel>() {

    @Inject
    override lateinit var factory: MainViewModelFactory

    override val viewModelClass = BarCodeViewModel::class.java

    override val layoutResource = R.layout.fragment_barcode_full_view

    private var binding: FragmentBarcodeFullViewBinding? = null


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val cardId = arguments?.getString(CARD_ID)
        viewModel.card.observe(this, Observer { card ->
            run {
                card?.let {
                    binding?.card = card
                    binding?.executePendingBindings()

                }
            }
        })
        cardId?.let {
            val width = resources.getDimension(R.dimen.bar_code_image_fullview_width).toInt()
            val height = resources.getDimension(R.dimen.bar_code_image_fullview_height).toInt()

            viewModel.loadBarCode(it, width, height)
        }
    }

    override fun setupView(rootView: View) {
        binding = DataBindingUtil.bind(rootView)
        binding?.btnClose?.setOnClickListener { _ -> dismissAllowingStateLoss() }

    }

    override fun onStart() {
        super.onStart()
        val params = dialog.window.attributes
        params.width = WindowManager.LayoutParams.MATCH_PARENT
        params.height = WindowManager.LayoutParams.MATCH_PARENT
        dialog.window.attributes = params
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window.requestFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    companion object {

        private const val CARD_ID = "cardId"

        fun newInstance(id: String) = BarcodeFragment().apply {
            arguments = Bundle().apply { putString(CARD_ID, id) }
        }
    }
}
