package com.dataart.dartcard.presentation.di.module

import com.dataart.dartcard.data.job.JobResult
import com.dataart.dartcard.data.job.RxJobResultBus
import dagger.Module
import dagger.Provides
import io.reactivex.subjects.PublishSubject
import javax.inject.Singleton

@Module
class EventBusModule {

    @Provides
    @Singleton
    fun provideRxBus(): RxJobResultBus {
        val subject = PublishSubject.create<JobResult>()

        return object : RxJobResultBus {
            override fun postEvent(event: JobResult) {
                subject.onNext(event)
            }

            override val events = subject
        }
    }
}
