package com.dataart.dartcard.presentation.util

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Parcelable
import com.dataart.dartcard.BuildConfig
import com.dataart.dartcard.R
import java.util.*


object EmailSenderUtil {

    private val deviceName: String
        get() {
            val manufacturer = Build.MANUFACTURER
            val model = Build.MODEL
            return "${manufacturer.capitalize()} $model"
        }

    private fun createEmailOnlyChooserIntent(context: Context, source: Intent, chooserTitle: CharSequence): Intent {
        val intents = Stack<Intent>()
        val i = Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto",
                "info@domain.com", null))
        val activities = context.packageManager.queryIntentActivities(i, 0)

        for (ri in activities) {
            val target = Intent(source)
            target.setPackage(ri.activityInfo.packageName)
            intents.add(target)
        }

        if (!intents.isEmpty()) {
            return Intent.createChooser(intents.removeAt(0),
                    chooserTitle).apply {
                putExtra(Intent.EXTRA_INITIAL_INTENTS,
                        intents.toTypedArray<Parcelable>())
            }
        } else {
            return Intent.createChooser(source, chooserTitle)
        }
    }

    fun sendEmail(context: Context) {
        val i = Intent(Intent.ACTION_SEND)
        i.type = "*/*"
        i.putExtra(Intent.EXTRA_EMAIL, arrayOf(context.getString(R.string.email_address_talent_lab), context.getString(R.string.email_address_disco_dev)))
        i.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.email_subject))
        i.putExtra(Intent.EXTRA_TEXT, context.getString(R.string.email_text,
                BuildConfig.VERSION_NAME,
                android.os.Build.VERSION.SDK_INT,
                deviceName))

        val chooserIntent = createEmailOnlyChooserIntent(context, i, context.getString(R.string.email_chooser_intent_title))
        context.startActivity(chooserIntent)

    }


}
