package com.dataart.dartcard.presentation.main.models

import android.support.annotation.DrawableRes
import android.support.annotation.StringRes

import com.dataart.dartcard.R

enum class MoreMenuItem(@field:StringRes val textResourceId: Int,
                        @field:DrawableRes val iconResourceId: Int) {

    SEND_FEEDBACK(R.string.text_send_feedback, R.drawable.ic_more_menu_feedback),
    LOGOUT(R.string.text_logout, R.drawable.ic_more_menu_logout)
}
