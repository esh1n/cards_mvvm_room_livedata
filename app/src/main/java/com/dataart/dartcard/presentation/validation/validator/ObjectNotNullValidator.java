package com.dataart.dartcard.presentation.validation.validator;


import com.dataart.dartcard.presentation.validation.ValidationInput;

/**
 * Created by akhutornoy on 10.10.17.
 */

public class ObjectNotNullValidator implements Validator {

    @Override
    public ValidationResult validate(ValidationInput validationInput) {
        Object input = validationInput.getInput();
        return input == null ? ValidationResult.EMPTY : ValidationResult.PASSED;
    }
}