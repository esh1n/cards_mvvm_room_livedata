package com.dataart.dartcard.presentation.util

import org.apache.commons.io.IOUtils

import java.io.IOException

object TestUtils {

    @Throws(IOException::class)
    fun loadResourceAsString(resourceFilePath: String): String {
        return IOUtils.toString(TestUtils::class.java.getResourceAsStream(resourceFilePath), "UTF-8")
    }
}
