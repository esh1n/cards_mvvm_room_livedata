package com.dataart.dartcard.presentation.main.models

data class BrandModel(var name: String, var serverId: String? = null, var photo: String? = null, val synced: Boolean)
