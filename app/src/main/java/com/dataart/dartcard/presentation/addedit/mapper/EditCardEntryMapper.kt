package com.dataart.dartcard.presentation.addedit.mapper

import com.dataart.dartcard.data.room.entity.CardEntry
import com.dataart.dartcard.domain.mapper.Mapper
import com.dataart.dartcard.presentation.addedit.model.AddEditCardModel


open class EditCardEntryMapper() : Mapper<AddEditCardModel, CardEntry>() {

    override fun map(source: AddEditCardModel): CardEntry {

        return CardEntry(
                id = source.id!!.toInt(),
                brandId = source.brandModel!!.name,
                barCodeFormat = source.barCode?.format,
                barCode = source.barCode?.value,
                facePhoto = source.facePhoto,
                backPhoto = source.backPhoto,
                note = source.note,
                cardNumber = source.cardNumber,
                isPrivate = source.isPrivate,
                discount = source.discount)
    }

}
