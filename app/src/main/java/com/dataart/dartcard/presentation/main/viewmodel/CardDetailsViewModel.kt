package com.dataart.dartcard.presentation.main.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import com.dataart.dartcard.domain.SchedulersFacade
import com.dataart.dartcard.domain.cards.CardsRepository
import com.dataart.dartcard.domain.favorite.UserCardRepository
import com.dataart.dartcard.presentation.base.viewmodel.BaseLoadableViewModel
import com.dataart.dartcard.presentation.base.viewmodel.SingleLiveEvent
import com.dataart.dartcard.presentation.main.mapper.CardDetailMapper
import com.dataart.dartcard.presentation.main.models.CardModel

class CardDetailsViewModel internal constructor(private val cardsRepository: CardsRepository,
                                                private val favsRepo: UserCardRepository) : BaseLoadableViewModel() {

    private var mapper = CardDetailMapper()
    val cardDeleted = SingleLiveEvent<Void>()

    fun getCard(id: String): LiveData<CardModel> {
        return Transformations.map(cardsRepository.getCardById(id)) { mapper.map(it) }
    }

    fun manageFavoriteState(card: CardModel) {
        addDisposable(
                favsRepo.updateFavoriteState(card)
                        .compose(SchedulersFacade.applySchedulersCompletable())
                        .doOnSubscribe { _ -> loadingStatus.setValue(true) }
                        .doAfterTerminate { loadingStatus.setValue(false) }
                        .subscribe({ }, { throwable -> error.setValue(throwable) }))
    }

    fun deleteCard(card: CardModel) {
        addDisposable(
                cardsRepository.deleteCard(card.id)
                        .compose(SchedulersFacade.applySchedulersCompletable())
                        .doOnSubscribe { _ -> loadingStatus.setValue(true) }
                        .doAfterTerminate { loadingStatus.setValue(false) }
                        .subscribe({ cardDeleted.call() }, { throwable -> error.setValue(throwable) }))
    }
}
