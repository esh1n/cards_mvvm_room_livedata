package com.dataart.dartcard.presentation.main


import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v7.app.ActionBar
import android.view.MenuItem
import com.dataart.dartcard.R
import com.dataart.dartcard.presentation.base.SingleFragmentActivity
import com.dataart.dartcard.presentation.base.setABTitle
import com.dataart.dartcard.presentation.base.startActivity
import com.dataart.dartcard.presentation.main.adapter.fragment.MainFragmentTab
import com.dataart.dartcard.presentation.main.fragment.MainFragment
import com.dataart.dartcard.presentation.main.viewmodel.CardsActivityViewModel
import com.dataart.dartcard.presentation.util.DiscoColors.colorizeDisco
import kotlinx.android.synthetic.main.activity_cards.*


class CardsActivity : SingleFragmentActivity() {

    private var selectedItem: Int = 0

    override val contentViewResourceId = R.layout.activity_cards

    private lateinit var sharedViewModel: CardsActivityViewModel

    companion object {

        private const val SELECTED_ITEM = "arg_selected_item"

        fun start(context: Context) {
            context.startActivity<CardsActivity>()
        }
    }

    override fun getStartScreen(savedInstanceState: Bundle?) = MainFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedViewModel = ViewModelProviders.of(this).get(CardsActivityViewModel::class.java)
        setupBottomNavigation()
        initFragmentTransactionsListener()

        val restoredMenu = getSelectedFragmentMenuId(savedInstanceState)
        bottom_navigation.selectedItemId = restoredMenu.itemId

    }

    private fun setupBottomNavigation() {
        bottom_navigation.setOnNavigationItemSelectedListener { item ->
            selectFragment(item)
            true
        }
    }

    private fun selectFragment(menuItem: MenuItem) {
        val menuId = menuItem.itemId
        selectedItem = menuId
        updateBottomMenuCheckedItem(menuId)

        with(getTabByMenu(menuId)) {
            sharedViewModel.currentFragmentState.postValue(this)
            setABTitle(getTitleByTab(this))
        }

    }


    private fun getTitleByTab(tab: MainFragmentTab): CharSequence {
        if (tab == MainFragmentTab.CARDS) {
            return colorizeDisco(true)
        } else if (tab == MainFragmentTab.MORE) {
            return getString(R.string.title_more);
        }
        return ""
    }

    private fun getTabByMenu(menu: Int): MainFragmentTab {
        return when (menu) {
            R.id.menu_cards -> MainFragmentTab.CARDS
            R.id.menu_search -> MainFragmentTab.SEARCH
            R.id.menu_more -> MainFragmentTab.MORE
            else -> MainFragmentTab.MORE
        }
    }

    private fun updateBottomMenuCheckedItem(checkedFragmentId: Int) {
        val menu = bottom_navigation.menu
        for (i in 0 until menu.size()) {
            val menuItem = menu.getItem(i)
            menuItem.isChecked = menuItem.itemId == checkedFragmentId
        }
    }

    private fun getSelectedFragmentMenuId(savedInstance: Bundle?): MenuItem {
        val isSavedDataExist = savedInstance != null
        return if (isSavedDataExist) {
            selectedItem = savedInstance?.getInt(SELECTED_ITEM, 0) ?: 0
            bottom_navigation.menu.findItem(selectedItem)
        } else {
            val checkedItem = bottom_navigation!!.selectedItemId
            bottom_navigation.menu.findItem(checkedItem)
        }
    }

    private fun initFragmentTransactionsListener() {
        supportFragmentManager.addOnBackStackChangedListener { this.processFragmentsSwitching() }
    }

    private fun processFragmentsSwitching() {

        supportFragmentManager?.let {
            val actionBar = supportActionBar
            val isInRootFragment = it.backStackEntryCount == 0
            actionBar?.setDisplayHomeAsUpEnabled(!isInRootFragment)
            if (isInRootFragment) {
                updateTitleForRootFragment(actionBar)
            }
        }

    }

    private fun updateTitleForRootFragment(actionBar: ActionBar?) {
        actionBar?.let {
            val titleForRootFragment = getTitleByTab(getTabByMenu(bottom_navigation.selectedItemId))
            it.title = titleForRootFragment
        }
    }

    override fun onSetupActionBar(actionBar: ActionBar) {
        super.onSetupActionBar(actionBar)
        with(actionBar) {
            title = colorizeDisco(true)
            setHomeAsUpIndicator(R.drawable.ic_back_white)
            setDisplayHomeAsUpEnabled(false)
        }
    }

    override val isDisplayHomeAsUpEnabled = true

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(SELECTED_ITEM, selectedItem)
        super.onSaveInstanceState(outState)
    }

}
