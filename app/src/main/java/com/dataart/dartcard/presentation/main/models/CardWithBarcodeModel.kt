package com.dataart.dartcard.presentation.main.models

import android.graphics.Bitmap

class CardWithBarcodeModel(val brandName: String, val barCodeBitmap: Bitmap?)