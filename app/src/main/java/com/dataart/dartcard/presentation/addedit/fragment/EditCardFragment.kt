package com.dataart.dartcard.presentation.addedit.fragment

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.View
import com.dataart.dartcard.databinding.ItemAddPhotoBinding
import com.dataart.dartcard.presentation.addedit.activity.AddEditCardActivity
import com.dataart.dartcard.presentation.addedit.activity.AddEditCardActivity.Companion.CARD_ID_KEY
import com.dataart.dartcard.presentation.addedit.model.AddEditCardModel
import com.dataart.dartcard.presentation.util.ImageLoader.loadImage
import com.squareup.picasso.Picasso

class EditCardFragment : AddEditCardFragment() {

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeCards(arguments!!.getString(AddEditCardActivity.CARD_ID_KEY))
    }

    private fun observeCards(id: String) {
        viewModel.getCard(id).observe(this, Observer { card ->
            card?.let {
                loadImages(it.photo, it.backPhoto)
                sharedViewModel.copyCard(card, id)
                binding?.card = card
            }
        })
    }

    override fun saveCard(cardModel: AddEditCardModel) {
        viewModel.editCard(cardModel)
    }

    private fun loadImages(photo: String?, backPhoto: String?) {
        binding?.let {
            showImage(it.itemFacePhoto)
            showImage(it.itemBackPhoto)
            with(Picasso.get()) {
                loadImage(it.itemFacePhoto.ivPhotoImage, photo)
                loadImage(it.itemBackPhoto.ivPhotoImage, backPhoto)
            }
        }

    }

    private fun showImage(binding: ItemAddPhotoBinding?) {
        binding?.let {
            it.ivPhotoImage.visibility = View.VISIBLE
            it.containerAddPhoto.visibility = View.INVISIBLE
        }

    }

    companion object {

        fun newInstance(cardId: String) = EditCardFragment().apply {
            arguments = Bundle().apply { putString(CARD_ID_KEY, cardId) }
        }
    }
}




