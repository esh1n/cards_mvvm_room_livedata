package com.dataart.dartcard.presentation.main.fragment

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.Menu
import android.view.View
import com.dataart.dartcard.BuildConfig
import com.dataart.dartcard.R
import com.dataart.dartcard.presentation.auth.AuthActivity
import com.dataart.dartcard.presentation.base.BaseVMFragment
import com.dataart.dartcard.presentation.main.adapter.MoreMenuAdapter
import com.dataart.dartcard.presentation.main.models.MoreMenuItem
import com.dataart.dartcard.presentation.main.viewmodel.MainViewModelFactory
import com.dataart.dartcard.presentation.main.viewmodel.MoreViewModel
import com.dataart.dartcard.presentation.util.DialogUtil
import com.dataart.dartcard.presentation.util.EmailSenderUtil
import kotlinx.android.synthetic.main.fragment_more.*
import javax.inject.Inject

class MoreFragment : BaseVMFragment<MoreViewModel>() {

    @Inject
    override lateinit var factory: MainViewModelFactory

    override val viewModelClass = MoreViewModel::class.java

    override val layoutResource = R.layout.fragment_more


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        super.onPrepareOptionsMenu(menu)
        menu?.clear()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = MoreMenuAdapter(requireActivity())
        list_view_menu_items.adapter = adapter
        list_view_menu_items.setOnItemClickListener { _, _, position, _ ->
            val item = adapter.getItem(position)
            chooseAction(item).invoke()
        }
        setupBuildInfo()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.loadTimestamp()
        viewModel.timeStamp.observe(this, Observer { timestamp ->
            tv_last_refresh_date.text = timestamp ?: ""
        })
    }

    override fun setupView(rootView: View) {

    }

    private fun chooseAction(item: MoreMenuItem?): () -> Unit {
        item?.let {
            return when (it) {
                MoreMenuItem.LOGOUT ->
                    { ->
                        DialogUtil.showYesNoDialog(requireActivity(),
                                getString(R.string.text_logout_description),
                                getString(R.string.button_logout))
                        { AuthActivity.start(activity) }
                    }
                MoreMenuItem.SEND_FEEDBACK -> { -> EmailSenderUtil.sendEmail(requireActivity()) }

            }
        }
        return {}
    }

    private fun setupBuildInfo() {
        tv_app_version.text = getString(R.string.text_app_code, BuildConfig.VERSION_NAME)
    }


    companion object {
        fun newInstance(): Fragment {
            return MoreFragment()
        }
    }
}
