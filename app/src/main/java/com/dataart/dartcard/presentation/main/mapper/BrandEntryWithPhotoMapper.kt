package com.dataart.dartcard.presentation.main.mapper

import com.dataart.dartcard.data.room.entity.BrandWithPhoto
import com.dataart.dartcard.domain.mapper.Mapper
import com.dataart.dartcard.presentation.main.models.BrandModel

class BrandEntryWithPhotoMapper : Mapper<BrandWithPhoto, BrandModel>() {
    override fun map(source: BrandWithPhoto): BrandModel {
        return BrandModel(source.name, synced = source.synced).apply {
            photo = source.photo
        }
    }
}