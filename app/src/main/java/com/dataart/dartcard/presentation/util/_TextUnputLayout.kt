package com.dataart.dartcard.presentation.util

import android.graphics.Color
import android.text.SpannableString
import android.widget.EditText

fun EditText.markRequired() {
    hint = "$hint *"
}

fun EditText.markRequired(hintStr: String) {
    val astrix = "*"
    val text = "$hintStr $astrix"
    val spannableHint = SpannableString(text)
    SpannableUtil.addColoredPart(spannableHint, text, astrix, Color.RED)
    hint = spannableHint
}
