package com.dataart.dartcard.presentation.main.mapper

import com.dataart.dartcard.data.room.entity.CardDetailsEntry
import com.dataart.dartcard.domain.mapper.Mapper
import com.dataart.dartcard.presentation.main.models.CardModel

class CardDetailMapper : Mapper<CardDetailsEntry?, CardModel>() {


    override fun map(source: CardDetailsEntry?): CardModel {
        source?.let {
            return CardModel(source.id).apply {
                discount = source.discount
                brandName = source.brandName
                barCode = source.barCode
                barCodeFormat = source.barCodeFormat
                cardNumber = source.cardNumber
                note = source.note
                photo = source.facePhoto
                backPhoto = source.backPhoto
                ownerName = source.ownerName
                favorite = source.favorite
                mine = source.mine
                synced = source.synced
                isPrivate = source.isPrivate
            }
        }
        return CardModel("")

    }
}
