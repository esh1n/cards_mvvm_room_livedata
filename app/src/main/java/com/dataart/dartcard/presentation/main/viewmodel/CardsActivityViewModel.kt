package com.dataart.dartcard.presentation.main.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.dataart.dartcard.presentation.main.adapter.fragment.MainFragmentTab

class CardsActivityViewModel : ViewModel() {
    val currentFragmentState = MutableLiveData<MainFragmentTab>()
}