package com.dataart.dartcard.presentation.di

import com.dataart.dartcard.presentation.addedit.activity.AddEditCardActivity
import com.dataart.dartcard.presentation.auth.AuthActivity
import com.dataart.dartcard.presentation.di.fragments.AddEditActivityModule
import com.dataart.dartcard.presentation.di.fragments.AuthActivityModule
import com.dataart.dartcard.presentation.di.fragments.CardsActivityModule
import com.dataart.dartcard.presentation.di.module.ViewModelsModule
import com.dataart.dartcard.presentation.main.CardsActivity
import com.dataart.dartcard.presentation.start.StartActivity

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivitiesModule {

    @ContributesAndroidInjector(modules = [ViewModelsModule::class, CardsActivityModule::class])
    fun contributeCardsActivity(): CardsActivity

    @ContributesAndroidInjector(modules = [AuthActivityModule::class])
    fun contributeAuthActivity(): AuthActivity

    @ContributesAndroidInjector(modules = [ViewModelsModule::class])
    fun contributeStartActivity(): StartActivity

    @ContributesAndroidInjector(modules = [AddEditActivityModule::class])
    fun contributeAddEditActivity(): AddEditCardActivity
}
