package com.dataart.dartcard.presentation.main.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.paging.PagedList
import com.dataart.dartcard.data.room.entity.BrandEntry
import com.dataart.dartcard.data.room.entity.CardBrandDetails
import com.dataart.dartcard.domain.SchedulersFacade
import com.dataart.dartcard.domain.brands.BrandsRepository
import com.dataart.dartcard.domain.cards.CardsRepository
import com.dataart.dartcard.domain.favorite.UserCardRepository
import com.dataart.dartcard.presentation.base.viewmodel.BaseLoadableViewModel
import com.dataart.dartcard.presentation.main.models.CardModel

class BrandDetailsViewModel internal constructor(private val brandsRepository: BrandsRepository, private val cardsRepository: CardsRepository, private val favsRepository: UserCardRepository) : BaseLoadableViewModel() {

    fun getCardsByBrand(name: String): LiveData<PagedList<CardBrandDetails>> {
        return cardsRepository.loadBrandCards(name)
    }

    fun getBrand(brandName: String): LiveData<BrandEntry> {
        return brandsRepository.getBrand(brandName)
    }

    fun manageFavoriteState(card: CardModel) {
        addDisposable(
                favsRepository.updateFavoriteState(card)
                        .compose(SchedulersFacade.applySchedulersCompletable())
                        .doOnSubscribe { _ -> loadingStatus.setValue(true) }
                        .doAfterTerminate { loadingStatus.setValue(false) }
                        .subscribe({ }, { throwable -> error.setValue(throwable) }))
    }
}
