package com.dataart.dartcard.presentation.main.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import com.dataart.dartcard.domain.SchedulersFacade
import com.dataart.dartcard.domain.cards.CardsRepository
import com.dataart.dartcard.domain.favorite.UserCardRepository
import com.dataart.dartcard.presentation.main.mapper.SearchCardEntryMapper
import com.dataart.dartcard.presentation.main.models.CardModel


class GlobalSearchViewModel(private val repository: CardsRepository, private val favsRepository: UserCardRepository) : SearchBrandViewModel<List<CardModel>>() {

    private val cardEntityMapper = SearchCardEntryMapper()

    fun manageFavoriteState(card: CardModel) {
        addDisposable(
                favsRepository.updateFavoriteState(card)
                        .compose(SchedulersFacade.applySchedulersCompletable())
                        .doOnSubscribe { _ -> loadingStatus.setValue(true) }
                        .doAfterTerminate { loadingStatus.setValue(false) }
                        .subscribe({ }, { throwable -> error.setValue(throwable) }))
    }

    override fun searchExactQuery(query: String): LiveData<List<CardModel>> {
        return Transformations.map(repository.loadCardsByBrandNameOrOwnerName(query)) { cardEntityMapper.map(it) }
    }
}

