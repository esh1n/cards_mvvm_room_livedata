package com.dataart.dartcard.presentation.di

import com.dataart.dartcard.App
import com.dataart.dartcard.AppLifecycleListener
import com.dataart.dartcard.domain.jobmanager.SchedulerJobService
import com.dataart.dartcard.domain.jobmanager.jobs.BaseSyncJob
import com.dataart.dartcard.presentation.di.module.AppModule
import com.dataart.dartcard.presentation.di.module.DataModule
import com.dataart.dartcard.presentation.di.module.EventBusModule
import com.dataart.dartcard.presentation.di.module.WorkerModule
import dagger.BindsInstance
import dagger.Component
import dagger.Subcomponent
import dagger.android.AndroidInjector
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    AppModule::class,
    DataModule::class,
    EventBusModule::class,
    ActivitiesModule::class,
    WorkerModule::class])
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: App): Builder

        fun build(): AppComponent
    }

    fun for21Plus(): For21Plus

    fun inject(appLifecycleListener: AppLifecycleListener)

    fun inject(baseSyncJob: BaseSyncJob)

    @Subcomponent(modules = [For21Plus.Module::class])
    interface For21Plus {
        fun inject(app: App)
        @dagger.Module
        abstract class Module {
            @ContributesAndroidInjector
            internal abstract fun contributeSchedulerJobService(): SchedulerJobService
        }
    }

}


