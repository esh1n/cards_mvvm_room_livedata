package com.dataart.dartcard.presentation.main.adapter

import android.arch.paging.PagedListAdapter
import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.dataart.dartcard.R
import com.dataart.dartcard.data.room.entity.BrandWithPhoto
import com.dataart.dartcard.databinding.ItemBrandBinding
import com.dataart.dartcard.presentation.main.mapper.BrandEntryWithPhotoMapper
import com.dataart.dartcard.presentation.main.models.BrandModel
import com.dataart.dartcard.presentation.util.ImageLoader.loadImage
import com.dataart.dartcard.presentation.util.inflate
import com.squareup.picasso.Picasso
import io.fabric.sdk.android.Fabric

class BrandPagedAdapter(private val itemSelectedAction: (String) -> Unit) : PagedListAdapter<BrandWithPhoto, BrandPagedAdapter.ViewHolder>(DIFF_CALLBACK) {

    private val brandEntryMapper: BrandEntryWithPhotoMapper = BrandEntryWithPhotoMapper()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = parent.context.inflate(R.layout.item_brand, parent)
        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = getItem(position)
        if (user != null) {
            holder.bindTo(brandEntryMapper.map(user))
        } else {
            holder.clear()
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private val binding: ItemBrandBinding? = DataBindingUtil.bind(itemView)

        init {
            binding?.root?.setOnClickListener(this)
        }

        fun bindTo(brandModel: BrandModel) {
            binding?.brand = brandModel
            Picasso.get().loadImage(binding?.appCompatImageView, brandModel.photo)
        }

        fun clear() {
            binding?.brand = null
        }

        override fun onClick(v: View) {
            val brandEntry = getItem(adapterPosition)
            if (brandEntry != null) {
                itemSelectedAction.invoke(brandEntry.name)
            } else {
                Fabric.getLogger().e(ViewHolder::class.java.simpleName, "brand is null at on click listener")
            }

        }
    }

    companion object {
        val DIFF_CALLBACK: DiffUtil.ItemCallback<BrandWithPhoto> = object : DiffUtil.ItemCallback<BrandWithPhoto>() {
            override fun areItemsTheSame(oldItem: BrandWithPhoto, newItem: BrandWithPhoto): Boolean {
                return oldItem.name == newItem.name
            }

            override fun areContentsTheSame(oldItem: BrandWithPhoto, newItem: BrandWithPhoto): Boolean {
                return oldItem == newItem
            }
        }
    }

}

