package com.dataart.dartcard.presentation.base


import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.app.AppCompatDelegate

import com.dataart.dartcard.R

import dagger.android.AndroidInjection

/**
 * Created by sergeyeshin on 11/5/17.
 */

abstract class BaseDIActivity : AppCompatActivity() {

    protected open val contentViewResourceId = R.layout.activity_container

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(contentViewResourceId)
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }

}
