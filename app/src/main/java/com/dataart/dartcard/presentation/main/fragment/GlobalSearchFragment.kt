package com.dataart.dartcard.presentation.main.fragment

import android.arch.lifecycle.Observer
import android.view.MenuItem
import com.dataart.dartcard.R
import com.dataart.dartcard.presentation.base.SearchFragment
import com.dataart.dartcard.presentation.base.addFragmentToStack
import com.dataart.dartcard.presentation.main.adapter.CardClickHandler
import com.dataart.dartcard.presentation.main.adapter.CardsAdapter
import com.dataart.dartcard.presentation.main.fragment.details.CardDetailFragment
import com.dataart.dartcard.presentation.main.models.CardModel
import com.dataart.dartcard.presentation.main.viewmodel.GlobalSearchViewModel
import com.dataart.dartcard.presentation.main.viewmodel.MainViewModelFactory
import javax.inject.Inject

class GlobalSearchFragment : SearchFragment<GlobalSearchViewModel, CardsAdapter>() {

    @Inject
    override lateinit var factory: MainViewModelFactory

    private val clickHandler = object : CardClickHandler {
        override fun manageFavorite(cardModel: CardModel) {
            viewModel.manageFavoriteState(cardModel)
        }

        override fun openDetails(cardModel: CardModel) {
            val fragment = CardDetailFragment.newInstance(cardModel.id)
            activity?.addFragmentToStack(fragment)
        }
    }

    override val viewModelClass = GlobalSearchViewModel::class.java

    override val layoutResource = R.layout.fragment_global_search

    override val adapterImplementation = CardsAdapter(clickHandler)

    override fun queryHintResourceId() = R.string.action_search_global

    override fun observeItemsByName(searchQuery: String) {
        viewModel.searchContainsQuery(searchQuery).observe(this, Observer<List<CardModel>> { cards ->
            cards?.let {
                swapCardsInUI(it)
            }
        })
    }

    override fun onUpdateSearchViewItem(searchItem: MenuItem) {
        super.onUpdateSearchViewItem(searchItem)
        with(searchItem) {
            expandActionView()
            actionView.clearFocus()
        }
    }

    private fun swapCardsInUI(cardModels: List<CardModel>) {
        adapter.swapCards(cardModels)
        if (cardModels.isEmpty()) {
            showEmptyView()
        } else {
            hideEmptyView()
        }
    }

    override val emptyTextViewId = R.string.empty_text_no_cards_global
}
