package com.dataart.dartcard.presentation.util

import android.content.Context
import android.graphics.Color
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import com.dataart.dartcard.R
import java.util.*

object DiscoColors {

    fun Context.colorizeDisco(upperCase: Boolean): SpannableString {
        var appName = getString(R.string.app_name)
        appName = if (upperCase) appName.toUpperCase() else appName
        return SpannableString(appName).apply {
            val discoColors = getDiscoColors()
            for (i in 0 until appName.length) {
                val color = discoColors[i]
                setSpan(ForegroundColorSpan(color), i, i + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            }
        }
    }

    private fun Context.getDiscoColors(): List<Int> {
        val colorsTxt = resources.getStringArray(R.array.disco_colors)
        return ArrayList<Int>().apply {
            for (i in colorsTxt.indices) {
                val newColor = Color.parseColor(colorsTxt[i])
                add(newColor)
            }
        }
    }


}
