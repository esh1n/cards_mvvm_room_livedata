package com.dataart.dartcard.presentation.util

object StringUtils {
    fun String?.isNonEmptyOrNull(): Boolean {
        return !this.isNullOrEmpty()
    }

    fun String?.capitalize(): String {
        this?.let {
            return "${it[0].toUpperCase()}${it.substring(1)}"
        }
        return ""
    }
}
