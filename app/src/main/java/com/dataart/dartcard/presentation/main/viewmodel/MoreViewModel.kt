package com.dataart.dartcard.presentation.main.viewmodel

import android.arch.lifecycle.MutableLiveData
import com.dataart.dartcard.data.cache.CacheContext
import com.dataart.dartcard.domain.SchedulersFacade
import com.dataart.dartcard.presentation.base.viewmodel.BaseLoadableViewModel
import com.dataart.dartcard.presentation.main.mapper.EntityDateMapper
import io.reactivex.Single

class MoreViewModel internal constructor(private val cacheContext: CacheContext) : BaseLoadableViewModel() {

    val timeStamp = MutableLiveData<String>()

    fun loadTimestamp() {
        addDisposable(
                Single.fromCallable { -> cacheContext.lastSyncTimeStamp }
                        .map(EntityDateMapper(EntityDateMapper.DateFormat.SYNC_TIME_STAMP)::map)
                        .compose(SchedulersFacade.applySchedulersSingle())
                        .doOnSubscribe { _ -> loadingStatus.value = true }
                        .doAfterTerminate { -> loadingStatus.value = false }
                        .subscribe { timestamp -> this.timeStamp.value = timestamp })
    }
}