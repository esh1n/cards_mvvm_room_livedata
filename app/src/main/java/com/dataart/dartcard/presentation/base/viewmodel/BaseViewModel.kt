package com.dataart.dartcard.presentation.base.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel() : ViewModel() {

    private val disposables = CompositeDisposable()

    var error = MutableLiveData<Throwable>()
        protected set

    override fun onCleared() {
        disposables.clear()
    }

    fun addDisposable(d: Disposable) {
        disposables.add(d)
    }
}