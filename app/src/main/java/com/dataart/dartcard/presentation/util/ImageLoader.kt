package com.dataart.dartcard.presentation.util

import android.support.annotation.DrawableRes
import android.util.Log
import android.widget.ImageView
import com.dataart.dartcard.R
import com.dataart.dartcard.presentation.util.views.RoundedCornersTransformation
import com.squareup.picasso.Callback
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import com.squareup.picasso.RequestCreator

object ImageLoader {
    fun Picasso?.loadImage(iv: ImageView?, url: String?) {
        iv?.let {
            val cornerRadius = it.resources.getDimension(R.dimen.card_photo_radius).toInt()
            loadImage(it, url, R.drawable.ic_card_placeholder, cornerRadius)
        }

    }

    fun Picasso?.loadImage(iv: ImageView, url: String?, @DrawableRes placeHolder: Int, cornerRadius: Int) {
        this?.let { _ ->
            if (url.isNullOrBlank()) {
                iv.setImageResource(placeHolder)
            } else {
                val isServerImage = url!!.startsWith("https")
                val path = if (isServerImage) url else "file:$url"
                createBaseRequestCreator(placeHolder, path, cornerRadius)
                        .networkPolicy(NetworkPolicy.OFFLINE)
                        .fit()
                        .into(iv, object : Callback {
                            override fun onSuccess() {
                                Log.d("image", "success at offline")
                            }

                            override fun onError(e: Exception) {

                                createBaseRequestCreator(placeHolder, url, cornerRadius).fit().into(iv)
                            }
                        })
            }
        }

    }

    private fun Picasso.createBaseRequestCreator(@DrawableRes placeHolder: Int, url: String, cornerRadius: Int): RequestCreator {
        Log.d("UpdloadImage", "Uri $url")
        return this.load(url)
                .error(placeHolder)
                .placeholder(placeHolder)
                .transform(RoundedCornersTransformation(cornerRadius, cornerRadius))
    }
}
