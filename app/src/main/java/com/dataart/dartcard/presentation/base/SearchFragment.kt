package com.dataart.dartcard.presentation.base

import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import com.dataart.dartcard.R
import com.dataart.dartcard.presentation.base.viewmodel.BaseLoadableViewModel
import com.jakewharton.rxbinding2.support.v7.widget.RxSearchView
import io.reactivex.disposables.CompositeDisposable
import java.util.concurrent.TimeUnit

abstract class SearchFragment<T : BaseLoadableViewModel, A : RecyclerView.Adapter<*>> : LoadableListFragment<T, A>() {

    protected val disposables = CompositeDisposable()

    protected var searchView: SearchView? = null

    protected val fullEmptySearchDescription: String
        get() = getString(emptySearchTextId, searchView!!.query)

    protected val emptySearchTextId: Int
        @StringRes
        get() = R.string.empty_text_search


    abstract fun observeItemsByName(searchQuery: String)

    override fun load() {
        val query = searchView?.query ?: ""
        observeItemsByName(query.toString())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu?.clear()
        inflater!!.inflate(R.menu.menu_search_cards, menu)

    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        super.onPrepareOptionsMenu(menu)
        menu?.let {
            val searchItem: MenuItem? = menu.findItem(R.id.action_search)
            if (searchItem != null) {
                onUpdateSearchViewItem(searchItem)
                searchView = searchItem.actionView as SearchView
                searchView?.setOnQueryTextListener(null)
                val hint = getString(queryHintResourceId())
                searchView?.queryHint = hint
                searchView?.onActionViewExpanded()
                searchView?.let { sv ->
                    disposables.add(RxSearchView.queryTextChanges(sv)
                            .skipInitialValue()
                            .debounce(300, TimeUnit.MILLISECONDS)
                            .map { it.toString() }
                            .distinctUntilChanged()
                            .subscribe({ this.observeItemsByName(it) }, { _ -> }))
                }
            }

        }

    }

    override fun onDestroyOptionsMenu() {
        super.onDestroyOptionsMenu()

    }

    @StringRes
    protected open fun queryHintResourceId() = R.string.action_search

    protected open fun onUpdateSearchViewItem(searchItem: MenuItem) {

    }

    override fun showEmptyView() {
        val searching = searchView != null &&
                !searchView!!.query.isNullOrEmpty()
        emptyView?.let {
            it.text = if (searching) fullEmptySearchDescription else getString(emptyTextViewId)
        }
        super.showEmptyView()
    }
}


