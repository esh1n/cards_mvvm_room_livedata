package com.dataart.dartcard.presentation.auth.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.birbit.android.jobqueue.JobManager
import com.dataart.dartcard.domain.auth.AuthRepository
import com.dataart.dartcard.presentation.main.viewmodel.MoreMenuViewModel
import com.dataart.dartcard.presentation.start.StartScreenViewModel

class AuthViewModelFactory(private val authRepository: AuthRepository, private val workManager: JobManager) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AuthViewModel::class.java)) {
            return AuthViewModel(authRepository, workManager) as T
        }
        if (modelClass.isAssignableFrom(StartScreenViewModel::class.java)) {
            return StartScreenViewModel(authRepository) as T
        }

        if (modelClass.isAssignableFrom(MoreMenuViewModel::class.java)) {
            return MoreMenuViewModel() as T
        }

        throw IllegalArgumentException("Unknown ViewModel class")
    }
}