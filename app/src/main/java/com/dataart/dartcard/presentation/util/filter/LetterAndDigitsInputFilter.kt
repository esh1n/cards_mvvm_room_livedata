package com.dataart.dartcard.presentation.util.filter

import android.text.InputFilter
import android.text.Spanned

class LetterAndDigitsInputFilter : InputFilter {
    override fun filter(source: CharSequence, start: Int, end: Int, dest: Spanned, dstart: Int, dend: Int): CharSequence? {
        for (i in start until end) {
            if (!Character.isLetterOrDigit(source[i])) {
                return "";
            }
        }
        return null
    }
}