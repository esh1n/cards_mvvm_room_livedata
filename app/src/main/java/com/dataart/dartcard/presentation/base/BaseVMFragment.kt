package com.dataart.dartcard.presentation.base

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log

import com.dataart.dartcard.presentation.base.viewmodel.BaseViewModel
import io.reactivex.disposables.CompositeDisposable

abstract class BaseVMFragment<VM : BaseViewModel> : BaseFragment() {

    protected lateinit var viewModel: VM

    abstract val viewModelClass: Class<VM>

    abstract val factory: ViewModelProvider.Factory

    protected val mCompositeDisposable = CompositeDisposable()


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, factory).get(viewModelClass)
        observeError()
    }

    private fun observeError() {
        viewModel.error.observe(this, Observer<Throwable> { this.onError(it!!) })
    }

    protected open fun onError(t: Throwable) {
        Log.e(viewModelClass.simpleName, t.message ?: "")
    }
}
