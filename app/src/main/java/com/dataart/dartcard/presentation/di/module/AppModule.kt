package com.dataart.dartcard.presentation.di.module

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.util.Log
import com.birbit.android.jobqueue.JobManager
import com.birbit.android.jobqueue.config.Configuration
import com.birbit.android.jobqueue.log.CustomLogger
import com.birbit.android.jobqueue.scheduling.FrameworkJobSchedulerService
import com.dataart.dartcard.App
import com.dataart.dartcard.data.room.AppDatabase
import com.dataart.dartcard.domain.jobmanager.SchedulerJobService
import com.dataart.dartcard.domain.jobmanager.jobs.BaseSyncJob
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    internal fun provideContext(application: App): Context {
        return application.applicationContext
    }

    @Provides
    @Singleton
    internal fun provideDatabase(application: App): AppDatabase {
        return AppDatabase.getInstance(application.applicationContext)
    }

    @Provides
    @Singleton
    internal fun provideJobManager(application: App): JobManager {
        return configureJobManager(application)
    }


    @Suppress("INACCESSIBLE_TYPE")
    private fun configureJobManager(app: App): JobManager {

        fun createLogger() = object : CustomLogger {

            override fun isDebugEnabled(): Boolean {
                return true
            }

            override fun d(text: String, vararg args: Any) {
                Log.d("JobManager", String.format(text, *args))
            }

            override fun e(t: Throwable, text: String, vararg args: Any) {
                Log.e("JobManager", String.format(text, *args), t)
            }

            override fun e(text: String, vararg args: Any) {
                Log.e("JobManager", String.format(text, *args))
            }

            override fun v(text: String, vararg args: Any) {
                // no-op
            }
        }

        val context = app.applicationContext
        val builder = Configuration.Builder(context)
                .minConsumerCount(1)//always keep at least one consumer alive
                .maxConsumerCount(3)//up to 3 consumers at a time
                .loadFactor(3)//3 jobs per consumer
                .consumerKeepAlive(120)//wait 2 minutes
                .customLogger(createLogger())


        // we are setting batch param below to false so that sync results are observed and acted upon in the background.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val scheduler = FrameworkJobSchedulerService.createSchedulerFor(context, SchedulerJobService::class.java)
            builder.scheduler(scheduler, false)
        }
        builder.injector { job ->
            Log.d("JobManager", "start inject")
            app.appComponent.inject(job as BaseSyncJob)
            Log.d("JobManager", "end inject")
        }
        return JobManager(builder.build())
    }

    @Provides
    @Singleton
    fun provideSharedPreferences(application: App): SharedPreferences {
        return application.getSharedPreferences("dataart.dartcard", Context.MODE_PRIVATE)
    }

}

