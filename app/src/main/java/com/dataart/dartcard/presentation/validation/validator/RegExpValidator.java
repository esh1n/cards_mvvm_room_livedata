package com.dataart.dartcard.presentation.validation.validator;


import com.dataart.dartcard.presentation.validation.ValidationInput;

import java.util.regex.Pattern;

public class RegExpValidator implements Validator {
    private Pattern pattern;

    public RegExpValidator(String regExp) {
        pattern = Pattern.compile(regExp);
    }

    @Override
    public ValidationResult validate(ValidationInput validationInput) {
        Object input = validationInput.getInput();

        if (!(input instanceof String)) {
            throw new IllegalArgumentException("Illegal type. Need: String");
        }

        String stringToValidate = (String) input;
        if (stringToValidate.equals("")) {
            if (validationInput.isMandatory()) {
                return ValidationResult.EMPTY;
            } else {
                return ValidationResult.PASSED;
            }
        }

        boolean matches = pattern.matcher((CharSequence) input).matches();
        return matches ? ValidationResult.PASSED : ValidationResult.FAILED;
    }
}