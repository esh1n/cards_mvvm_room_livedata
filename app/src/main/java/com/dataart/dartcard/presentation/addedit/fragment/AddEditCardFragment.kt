package com.dataart.dartcard.presentation.addedit.fragment

import android.Manifest
import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.text.InputFilter
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import com.dataart.dartcard.R
import com.dataart.dartcard.databinding.FragmentAddEditCardBinding
import com.dataart.dartcard.databinding.ItemAddPhotoBinding
import com.dataart.dartcard.databinding.ViewMenuSubmitBinding
import com.dataart.dartcard.presentation.addedit.activity.CustomScannerActivity
import com.dataart.dartcard.presentation.addedit.activity.CustomScannerActivity.CancelScanState
import com.dataart.dartcard.presentation.addedit.model.AddEditCardModel
import com.dataart.dartcard.presentation.addedit.model.BarCodeModel
import com.dataart.dartcard.presentation.addedit.viewmodel.AddEditCardFlowViewModel
import com.dataart.dartcard.presentation.addedit.viewmodel.AddEditCardViewModel
import com.dataart.dartcard.presentation.addedit.viewmodel.AddEditCardViewModelFactory
import com.dataart.dartcard.presentation.base.BaseVMFragment
import com.dataart.dartcard.presentation.base.addFragmentToStack
import com.dataart.dartcard.presentation.base.hideKeyboard
import com.dataart.dartcard.presentation.util.ImageSaver
import com.dataart.dartcard.presentation.util.SnackbarBuilder
import com.dataart.dartcard.presentation.util.filter.InputFilterMinMaxIntRange
import com.dataart.dartcard.presentation.util.filter.LetterAndDigitsInputFilter
import com.dataart.dartcard.presentation.util.markRequired
import com.dataart.dartcard.presentation.validation.ValidationField
import com.dataart.dartcard.presentation.validation.ValidationInput
import com.dataart.dartcard.presentation.widget.ValidatedEditText
import com.google.zxing.integration.android.IntentIntegrator
import com.jakewharton.rxbinding2.widget.RxTextView
import io.card.payment.CardIOActivity
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.internal.disposables.EmptyDisposable
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

open class AddEditCardFragment : BaseVMFragment<AddEditCardViewModel>() {


    @Inject
    override lateinit var factory: AddEditCardViewModelFactory

    override val viewModelClass = AddEditCardViewModel::class.java

    protected lateinit var sharedViewModel: AddEditCardFlowViewModel

    override val layoutResource = R.layout.fragment_add_edit_card

    private var item: MenuItem? = null
    protected var binding: FragmentAddEditCardBinding? = null
    private var menuVisible: Boolean = false

    private var menuSubmitBinding: ViewMenuSubmitBinding? = null


    var cameraActionCode = CAPTURE_FACE_IMAGE_REQUEST_CODE


    private lateinit var disposable: CompositeDisposable

    private val discountInputSource: Observable<String>
        get() = RxTextView.textChangeEvents(binding!!.tilDiscount)
                .debounce(100, TimeUnit.MILLISECONDS)
                .map { value -> value.view().text.toString() }
                .filter { it.isNotEmpty() }
                .distinctUntilChanged()

    private val cardNumberSource: Observable<ValidationField>
        get() {
            val validatedView = binding?.vetCardNumber
            val editText = validatedView?.editText!!
            return ValidatedEditText
                    .getTextChangesSource(editText)
                    .map { _ ->
                        ValidationField.newBuilder()
                                .withType(ValidationField.Type.CARD_NUMBER)
                                .withInput(ValidationInput(editText.text.toString(), false))
                                .withSuccessValidationAction { sharedViewModel.selectCardNumber(editText.text.toString()) }
                                .withEmptyInputAction {
                                    sharedViewModel.selectCardNumber("")
                                }
                                .withValidationErrorAction {
                                    sharedViewModel.selectCardNumber("")
                                }
                                .build()
                    }
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        disposable = CompositeDisposable()
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        disposable.clear()
        super.onDestroyView()
    }

    override fun setupView(rootView: View) {
        super.setupView(rootView)
        binding = DataBindingUtil.bind(rootView)
        binding?.let {
            it.tilBrand.editText?.setOnClickListener { _ -> navigateToChooseBrand() }
            it.tilBarCode.editText?.setOnClickListener { _ -> scanBarCodeFromFragment() }
            it.itemBackPhoto.tvPhotoTitle.setText(R.string.text_back_side_photo)
            it.itemBackPhoto.ivPhotoImage.setImageResource(R.drawable.ic_card_placeholder_back)
            it.tilCardDiscount.editText?.let { editText ->
                setDiscountFilters(editText)
            }
            setCardNumberLimits(it)

            disposable.add(addRedAsterixToBrandName(it) ?: EmptyDisposable.INSTANCE)

        }
        setListeners()

    }

    private fun setCardNumberLimits(it: FragmentAddEditCardBinding) {
        val cardNameLength = resources.getInteger(R.integer.card_number_length_limit);
        it.vetCardNumber.editText?.filters = arrayOf(LetterAndDigitsInputFilter(), InputFilter.LengthFilter(cardNameLength));
    }

    private fun addRedAsterixToBrandName(it: FragmentAddEditCardBinding): Disposable? {
        val hintStr = getString(R.string.text_input_brand)
        val editText = it.tilBrand.editText!!
        editText.markRequired(hintStr)
        return RxTextView.textChangeEvents(editText)
                .map { value -> value.view().text.toString() }
                .subscribe({ value ->
                    if (value.isBlank()) {
                        it.tilBrand.hint = "";
                        editText.markRequired(hintStr)
                    } else {
                        it.tilBrand.hint = "$hintStr *"

                    }
                }, { error ->
                    SnackbarBuilder.buildErrorSnack(requireActivity(), error.message.orEmpty()).show()
                })
    }

    private fun setListeners() {
        binding?.let {
            it.itemFacePhoto.root.setOnClickListener { _ -> tryToCaptureImage(CAPTURE_FACE_IMAGE_REQUEST_CODE) }
            it.itemBackPhoto.root.setOnClickListener { _ -> tryToCaptureImage(CAPTURE_BACK_IMAGE_REQUEST_CODE) }
        }

    }

    override fun onError(t: Throwable) {
        super.onError(t)
        hideKeyboard()
        SnackbarBuilder.buildErrorSnack(view!!, t.message.toString()).show()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sharedViewModel = ViewModelProviders.of(requireActivity()).get(AddEditCardFlowViewModel::class.java)
        sharedViewModel.init()
        binding?.tilCardDiscount?.editText?.let { editText ->
            viewModel.fixDiscountField(getDiscountFiledSource(editText))
        }
        observeLoadingStatus()
        observeSelectedBrand()
        observeNavigation()
        observeDiscountUpdate()
    }

    override fun onStart() {
        super.onStart()
        viewModel.validateFields(cardNumberSource)
        // viewModel.addPercentMask(getDiscountInputSource());
    }

    private fun observeSelectedBrand() {
        sharedViewModel.card.observe(this, Observer { card ->
            card?.let { builderModel ->
                val brandModel = builderModel.brandModel

                brandModel?.let {
                    binding?.tilBrand?.editText?.setText(brandModel.name)
                }
                val barCodeModel = builderModel.barCode

                barCodeModel?.let {
                    binding?.tilBarCode?.editText?.setText(it.value)
                }
                val barCodeExist = barCodeModel != null
                val brandExists = brandModel != null
                val isCardNumberExist = !builderModel.cardNumber.isNullOrBlank()
                val menuVisible = brandExists && (barCodeExist || isCardNumberExist);
                this.menuVisible = menuVisible;
                Handler().post { item?.isVisible = menuVisible }
                requireActivity().invalidateOptionsMenu()
            }
        })
    }

    private fun observeDiscountUpdate() {
        viewModel.formattedDiscount.observe(this, Observer { discount ->
            discount?.let { value ->
                if (!value.isEmpty()) {
                    binding?.tilCardDiscount?.editText?.let {
                        it.setText(value)
                        it.setSelection(value.length)
                    }
                }
            }
        })
    }

    private fun setDiscountFilters(editText: EditText) {
        val filterArray = arrayOfNulls<InputFilter>(2)
        filterArray[0] = InputFilter.LengthFilter(3)
        filterArray[1] = InputFilterMinMaxIntRange(0, 100)
        editText.filters = filterArray

    }

    private fun getDiscountFiledSource(editText: EditText) = Observable.defer {
        RxTextView.afterTextChangeEvents(editText)
                .map { value -> value.view().text.toString() }
                .debounce(100, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
    }

    private fun scanBarCodeFromFragment() {
        IntentIntegrator.forSupportFragment(this)
                .setCaptureActivity(CustomScannerActivity::class.java)
                .setPrompt("")
                .setRequestCode(SCAN_BARCODE_REQUEST_CODE)
                .setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES)
                .setOrientationLocked(false)
                .initiateScan()
    }

    private fun onCaptureCardImage(requestCode: Int) {
        val intent = Intent(activity, CardIOActivity::class.java)
                .putExtra(CardIOActivity.EXTRA_SUPPRESS_SCAN, true)
                .putExtra(CardIOActivity.EXTRA_RETURN_CARD_IMAGE, true)
                .putExtra(CardIOActivity.EXTRA_USE_PAYPAL_ACTIONBAR_ICON, false)

        startActivityForResult(intent, requestCode)
    }

    private fun tryToCaptureImage(requestCode: Int) {
        val isCameraPermissionGranted = ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
        if (isCameraPermissionGranted) {
            onCaptureCardImage(requestCode)
        } else {
            cameraActionCode = requestCode
            requestPermissions(arrayOf(Manifest.permission.CAMERA), MY_PERMISSIONS_REQUEST_CAMERA)
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_CAMERA -> {
                if (grantResults.isNotEmpty()) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        onCaptureCardImage(cameraActionCode)
                    } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                        // Should we show an explanation?
                        if (ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(), Manifest.permission.CAMERA)) {
                            val cameraAction: View.OnClickListener = View.OnClickListener { tryToCaptureImage(cameraActionCode) }
                            SnackbarBuilder.buildSnack(view!!, getString(R.string.hint_need_camera_for_photo), getString(R.string.text_require), cameraAction).show()
                        } else {
                            val buttonToBlock = if (cameraActionCode == CAPTURE_FACE_IMAGE_REQUEST_CODE) binding?.itemFacePhoto?.root else binding?.itemBackPhoto?.root
                            buttonToBlock?.isEnabled = false
                        }
                    }
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == SCAN_BARCODE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_CANCELED) {
                if (data != null) {
                    val scanCancelCode = CancelScanState.valueOf(data.getStringExtra(BAR_CODE_ADD_OTHER_WAY))
                    when (scanCancelCode) {
                        CustomScannerActivity.CancelScanState.INPUT_MANUALLY -> {
                            openBarCodeInputFragment()
                            return
                        }
                        CustomScannerActivity.CancelScanState.NO_BARCODE ->
                            //Nothing to do
                            return
                    }
                }
                return
            }
            val result = IntentIntegrator.parseActivityResult(resultCode, data)
            if (result.contents != null) {
                updateBarCode(result.contents, result.formatName)
            }

        } else if (requestCode == CAPTURE_FACE_IMAGE_REQUEST_CODE || requestCode == CAPTURE_BACK_IMAGE_REQUEST_CODE) {

            if (resultCode != Activity.RESULT_CANCELED && data != null) {
                val isFacePhoto = requestCode == CAPTURE_FACE_IMAGE_REQUEST_CODE
                val fileName = if (isFacePhoto) "frontPhoto.jpeg" else "backPhoto.jpeg"
                val saveFacePhoto = sharedViewModel::selectFacePhoto
                val saveBackPhoto = sharedViewModel::selectBackPhoto
                val saveMethod = if (isFacePhoto) saveFacePhoto else saveBackPhoto
                val imageViewToSet = if (isFacePhoto) binding!!.itemFacePhoto else binding!!.itemBackPhoto
                val photoBitmap = CardIOActivity.getCapturedCardImage(data)
                writeImageAndShow(photoBitmap, fileName, saveMethod)
                setBitmapToImage(imageViewToSet, photoBitmap)

            }
        }
    }

    private fun writeImageAndShow(photoBitmap: Bitmap, fileName: String, saveMethod: (String) -> Unit) {
        val fileNameWithTimeStamp = fileName + "_" + Date().time
        val imageSaver = ImageSaver(fileNameWithTimeStamp, requireActivity());
        val imagePath = imageSaver.saveToInternalStorage(photoBitmap)
        saveMethod(imagePath)
    }

    private fun setBitmapToImage(it: ItemAddPhotoBinding?, bitmap: Bitmap) {
        it?.let {
            it.ivPhotoImage.visibility = View.VISIBLE
            it.ivPhotoImage.setImageBitmap(bitmap)
            it.containerAddPhoto.visibility = View.INVISIBLE
        }
    }

    private fun openBarCodeInputFragment() {
        activity.addFragmentToStack(BarcodeInputFragment())
    }


    private fun updateBarCode(contents: String?, formatName: String) {
        contents?.let {
            val barCodeModel = BarCodeModel(contents, formatName)
            sharedViewModel.selectBarcode(barCodeModel)
        }
    }

    private fun navigateToChooseBrand() {
        activity.addFragmentToStack(ChooseBrandFragment())
    }

    private fun observeLoadingStatus() {
        viewModel.loadingStatus.observe(this, Observer<Boolean> { this.processLoadingStatus(it!!) })
    }

    private fun observeNavigation() {
        viewModel.navigateBackEvent.observe(this, Observer { _ -> requireActivity().finish() })
    }


    private fun processLoadingStatus(isLoading: Boolean) {
        menuSubmitBinding?.loadingIndicator?.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater!!.inflate(R.menu.menu_submit, menu)
        item = menu!!.findItem(R.id.action_submit)
        menuSubmitBinding = DataBindingUtil.bind(item!!.actionView)
        menuSubmitBinding?.let {
            it.btnSubmit.setOnClickListener { _ -> saveCard() }
            it.btnSubmit.visibility = if (menuVisible) View.VISIBLE else View.GONE
        }

    }

    private fun saveCard() {
        val cardModel = sharedViewModel.card.value;
        cardModel?.let {
            it.note = binding?.tilCardComment?.editText?.text.toString()
            it.cardNumber = binding?.vetCardNumber?.editText?.text.toString()
            it.discount = parseDiscount(binding?.tilDiscount?.text.toString())
            it.isPrivate = binding?.switchPrivate?.isChecked ?: false
            saveCard(it)
        }

    }

    open fun saveCard(cardModel: AddEditCardModel) {
        viewModel.saveCard(cardModel)
    }

    private fun parseDiscount(value: String?) = if (value.isNullOrBlank()) 0 else Integer.parseInt(value)

    companion object {

        const val BAR_CODE_ADD_OTHER_WAY = "ADD_OTHER"
        private const val CAPTURE_FACE_IMAGE_REQUEST_CODE = 101
        private const val CAPTURE_BACK_IMAGE_REQUEST_CODE = 102
        private const val SCAN_BARCODE_REQUEST_CODE = 100
        private const val MY_PERMISSIONS_REQUEST_CAMERA = 123

        fun newInstance(): AddEditCardFragment {
            return AddEditCardFragment()
        }
    }


}
