package com.dataart.dartcard.presentation.main.fragment.cards

import android.arch.lifecycle.Observer
import com.dataart.dartcard.R
import com.dataart.dartcard.presentation.base.addFragmentToStack
import com.dataart.dartcard.presentation.main.adapter.CardClickHandler
import com.dataart.dartcard.presentation.main.adapter.CardsAdapter
import com.dataart.dartcard.presentation.main.fragment.details.CardDetailFragment
import com.dataart.dartcard.presentation.main.models.CardModel
import com.dataart.dartcard.presentation.main.viewmodel.FavoriteCardsViewModel

class FavoriteCardsFragment : HomeSearchFragment<FavoriteCardsViewModel, CardsAdapter>() {

    private val clickHandler = object : CardClickHandler {
        override fun manageFavorite(cardModel: CardModel) {
            viewModel.unFavorite(cardModel.id)
        }

        override fun openDetails(cardModel: CardModel) {
            val fragment = CardDetailFragment.newInstance(cardModel.id)
            activity.addFragmentToStack(fragment)
        }
    }

    override fun observeItemsByName(searchQuery: String) {
        viewModel.searchContainsQuery(searchQuery).observe(this, Observer<List<CardModel>> { this.swapCardsInUI(it) })
    }

    private fun swapCardsInUI(cardModels: List<CardModel>?) {
        adapter.swapCards(cardModels.orEmpty())
        if (cardModels.orEmpty().isEmpty()) {
            showEmptyView()
        } else {
            hideEmptyView()
        }
    }

    override val layoutResource = R.layout.fragment_home

    override val adapterImplementation = CardsAdapter(clickHandler)

    override val viewModelClass = FavoriteCardsViewModel::class.java

    override fun refresh() {
        viewModel.refreshCards()
    }

    override val emptyTextViewId = R.string.empty_text_no_fav_cards
}

