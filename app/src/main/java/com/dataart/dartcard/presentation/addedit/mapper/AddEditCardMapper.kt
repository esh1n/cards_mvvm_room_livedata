package com.dataart.dartcard.presentation.addedit.mapper

import com.dataart.dartcard.data.room.entity.CardDetailsEntry
import com.dataart.dartcard.domain.mapper.Mapper
import com.dataart.dartcard.presentation.main.models.CardModel

class EditCardMapper : Mapper<CardDetailsEntry?, CardModel>() {

    override fun map(source: CardDetailsEntry?): CardModel {
        source?.let {
            return CardModel(it.id).apply {
                discount = it.discount
                brandName = it.brandName
                barCode = it.barCode
                barCodeFormat = it.barCodeFormat
                cardNumber = it.cardNumber
                note = it.note
                photo = it.facePhoto
                backPhoto = it.backPhoto
                ownerName = it.ownerName
                favorite = it.favorite
                mine = it.mine
                synced = it.synced
                serverId = it.serverId
                isPrivate = it.isPrivate
            }
        }
        return CardModel("")

    }
}