package com.dataart.dartcard.presentation.main.fragment.cards

import android.arch.lifecycle.Observer
import com.dataart.dartcard.R
import com.dataart.dartcard.presentation.base.addFragmentToStack
import com.dataart.dartcard.presentation.main.adapter.BrandPagedAdapter
import com.dataart.dartcard.presentation.main.fragment.details.BrandDetailsFragment
import com.dataart.dartcard.presentation.main.viewmodel.BrandsViewModel

class BrandsFragment : HomeSearchFragment<BrandsViewModel, BrandPagedAdapter>() {

    override val layoutResource = R.layout.fragment_home

    override val adapterImplementation = BrandPagedAdapter { openBrandDetails(it) }

    override val viewModelClass = BrandsViewModel::class.java

    override fun refresh() {
        viewModel.refreshBrands()
    }

    private fun openBrandDetails(brandId: String) {
        val brandDetailsFragment = BrandDetailsFragment.newInstance(brandId)
        activity.addFragmentToStack(brandDetailsFragment)
    }

    override fun observeItemsByName(searchQuery: String) {
        viewModel.searchContainsQuery(searchQuery).observe(this, Observer { brandEntries ->
            adapter.submitList(brandEntries)
            if (brandEntries.orEmpty().isEmpty()) {
                showEmptyView()
            } else {
                hideEmptyView()
            }
        })
    }

    override val emptyTextViewId = R.string.empty_text_no_brands
}
