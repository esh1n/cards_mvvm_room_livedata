package com.dataart.dartcard.presentation.main.adapter

import android.arch.paging.PagedListAdapter
import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.dataart.dartcard.R
import com.dataart.dartcard.data.room.entity.CardBrandDetails
import com.dataart.dartcard.databinding.ItemCardBrandDetailsBinding
import com.dataart.dartcard.presentation.main.mapper.CardBrandedFavoriteMapper
import com.dataart.dartcard.presentation.main.models.CardModel
import com.dataart.dartcard.presentation.util.ImageLoader.loadImage
import com.dataart.dartcard.presentation.util.inflate
import com.squareup.picasso.Picasso

class BrandCardsAdapter(private val itemSelectedAction: CardClickHandler) : PagedListAdapter<CardBrandDetails, BrandCardsAdapter.ViewHolder>(DIFF_CALLBACK) {

    private val cardMapper: CardBrandedFavoriteMapper = CardBrandedFavoriteMapper()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = parent.context.inflate(R.layout.item_card_brand_details, parent)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val card = getItem(position)
        if (card != null) {
            holder.bindTo(cardMapper.map(card))
        } else {
            holder.clear()
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private val binding: ItemCardBrandDetailsBinding? = DataBindingUtil.bind(itemView)

        init {
            binding?.imvFavoriteCard?.setOnClickListener(this)
            itemView.setOnClickListener(this)
        }

        fun bindTo(cardModel: CardModel) {
            binding?.card = cardModel
            Picasso.get().loadImage(binding?.appCompatImageView, cardModel.photo)
        }

        fun clear() {
            binding?.card = null
        }

        override fun onClick(v: View) {
            val card = getItem(adapterPosition)
            card?.let {
                val cardModel = cardMapper.map(it)
                if (v.id == R.id.imv_favorite_card) {
                    itemSelectedAction.manageFavorite(cardModel)
                } else {
                    itemSelectedAction.openDetails(cardModel)
                }
            }

        }
    }

    companion object {

        val DIFF_CALLBACK: DiffUtil.ItemCallback<CardBrandDetails> = object : DiffUtil.ItemCallback<CardBrandDetails>() {
            override fun areItemsTheSame(oldItem: CardBrandDetails, newItem: CardBrandDetails): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: CardBrandDetails, newItem: CardBrandDetails): Boolean {
                return oldItem == newItem
            }
        }
    }
}


