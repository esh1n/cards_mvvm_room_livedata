package com.dataart.dartcard.presentation.main.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.view.View
import com.dataart.dartcard.R
import com.dataart.dartcard.presentation.base.BaseSessionVMFragment
import com.dataart.dartcard.presentation.main.adapter.fragment.MainFragmentTab
import com.dataart.dartcard.presentation.main.adapter.fragment.MainFragmentsPagerAdapter
import com.dataart.dartcard.presentation.main.fragment.cards.CardsTabHostFragment
import com.dataart.dartcard.presentation.main.viewmodel.CardsActivityViewModel
import com.dataart.dartcard.presentation.main.viewmodel.MainViewModelFactory
import com.dataart.dartcard.presentation.main.viewmodel.RootContainerViewModel
import kotlinx.android.synthetic.main.fragment_main.*
import javax.inject.Inject


class MainFragment : BaseSessionVMFragment<RootContainerViewModel>() {
    override val layoutResource = R.layout.fragment_main

    override val viewModelClass = RootContainerViewModel::class.java

    @Inject
    override lateinit var factory: MainViewModelFactory

    private lateinit var sharedViewModel: CardsActivityViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = MainFragmentsPagerAdapter(requireFragmentManager(), requireActivity())
        viewpager_main.adapter = adapter
        invalidateFragmentMenus(adapter, viewpager_main.currentItem);
        viewpager_main.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                invalidateFragmentMenus(adapter, position)
            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sharedViewModel = ViewModelProviders.of(requireActivity()).get(CardsActivityViewModel::class.java)
        sharedViewModel.currentFragmentState.observe(this, Observer {
            it?.let { tab ->
                val currentTab = MainFragmentTab.getByPosition(viewpager_main.currentItem)
                if (currentTab != it) {
                    viewpager_main.currentItem = tab.value
                    fragmentManager?.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
                }
            }

        })
    }

    private fun invalidateFragmentMenus(adapter: FragmentPagerAdapter, position: Int) {
        val fm = requireActivity().supportFragmentManager
        for (i in 0 until adapter.count) {
            val page = fm.findFragmentByTag("android:switcher:${R.id.viewpager_main}:$i")
            val isPageCurrent = i == position
            page?.setHasOptionsMenu(isPageCurrent)

            (page as? CardsTabHostFragment)?.let {
                val currentFragment = it.getCurrentFragment()
                currentFragment?.setHasOptionsMenu(isPageCurrent)
            }

        }
        requireActivity().invalidateOptionsMenu();
    }

}

