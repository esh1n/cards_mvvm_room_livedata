package com.dataart.dartcard.presentation.main.mapper


import com.dataart.dartcard.data.room.entity.CardBrandDetails
import com.dataart.dartcard.domain.mapper.Mapper
import com.dataart.dartcard.presentation.main.models.CardModel

class CardBrandedFavoriteMapper : Mapper<CardBrandDetails, CardModel>() {
    override fun map(source: CardBrandDetails): CardModel {
        return CardModel(source.id).apply {
            photo = source.photo
            discount = source.discount
            favorite = source.favorite
            ownerName = source.ownerName
            synced = source.synced
            mine = source.mine
        }
    }
}
