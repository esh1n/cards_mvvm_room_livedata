package com.dataart.dartcard.presentation.validation;

import android.support.annotation.Nullable;


public class ValidationField {
    private Type type;
    private ValidationInput input;
    private SuccessValidationAction successValidationAction;
    private ValidationErrorAction validationErrorAction;
    private EmptyInputAction emptyInputAction;

    private ValidationField(Builder builder) {
        this.type = builder.type;
        this.input = builder.input;
        this.successValidationAction = builder.successValidationAction == null ?
                () -> {
                } : builder.successValidationAction;
        this.validationErrorAction = builder.validationErrorAction == null ?
                () -> {
                } : builder.validationErrorAction;
        this.emptyInputAction = builder.emptyInputAction == null ?
                () -> {
                } : builder.emptyInputAction;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Type getType() {
        return type;
    }

    public ValidationInput getInput() {
        return input;
    }

    public SuccessValidationAction getSuccessValidationAction() {
        return successValidationAction;
    }

    public ValidationErrorAction getValidationErrorAction() {
        return validationErrorAction;
    }

    public EmptyInputAction getEmptyInputAction() {
        return emptyInputAction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ValidationField that = (ValidationField) o;

        return type == that.type;
    }

    @Override
    public int hashCode() {
        return type.hashCode();
    }

    public enum Type {
        BRAND_NAME, NAME, BARCODE, CARD_NUMBER
    }

    public interface ValidationResultAction {
        void call();
    }

    public interface SuccessValidationAction extends ValidationResultAction {
    }

    public interface ValidationErrorAction extends ValidationResultAction {
    }

    public interface EmptyInputAction extends ValidationResultAction {
    }

    public static final class Builder {
        private Type type;
        private ValidationInput input;
        private SuccessValidationAction successValidationAction;
        private ValidationErrorAction validationErrorAction;
        private EmptyInputAction emptyInputAction;

        private Builder() {
        }

        public Builder withType(Type type) {
            this.type = type;
            return this;
        }

        public Builder withInput(ValidationInput input) {
            this.input = input;
            return this;
        }

        public Builder withSuccessValidationAction(@Nullable SuccessValidationAction successValidationAction) {
            this.successValidationAction = successValidationAction;
            return this;
        }

        public Builder withValidationErrorAction(@Nullable ValidationErrorAction validationErrorAction) {
            this.validationErrorAction = validationErrorAction;
            return this;
        }

        public Builder withEmptyInputAction(@Nullable EmptyInputAction emptyInputAction) {
            this.emptyInputAction = emptyInputAction;
            return this;
        }

        public ValidationField build() {
            return new ValidationField(this);
        }
    }
}
