package com.dataart.dartcard.presentation.util

import android.databinding.BindingAdapter
import android.graphics.Bitmap
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.dataart.dartcard.R


@BindingAdapter("imageBitmap")
fun loadImageAdapter(iv: ImageView, bitmap: Bitmap?) {
    bitmap?.let {
        iv.setImageBitmap(it)
    }

}

@BindingAdapter("imageFavorite")
fun setImageUrl(imageView: ImageView, favorite: Boolean?) {
    imageView.setImageResource(if (favorite != false) R.drawable.ic_favorite_active else R.drawable.ic_favorite_inactive);
}

@BindingAdapter(value = ["cardOwner"], requireAll = false)
fun setCardOwner(tv: TextView, cardOwner: String?) {
    tv.text = cardOwner ?: tv.context.getString(R.string.text_no_cardwner)

}

@BindingAdapter("intAsString")
fun setIntAsString(tv: TextView, value: Int?) {
    tv.text = value.toString()
}

@BindingAdapter("intAsResource")
fun setIntAsResource(tv: TextView, value: Int?) {
    value?.let {
        tv.text = tv.context.getString(it)
    }
}

@BindingAdapter("intAsDrawable")
fun setIntAsDrawable(im: ImageView, value: Int?) {
    value?.let {
        im.setImageResource(it)
    }
}

@BindingAdapter("visibleOrGone")
fun visibleOrGone(im: View, visible: Boolean?) {
    visible?.let {
        im.visibility = if (visible) View.VISIBLE else View.GONE
    }
}

@BindingAdapter("discount")
fun setDiscount(tv: TextView, discount: Int?) {
    val parsedDiscount = discount ?: 0
    val discountExist = parsedDiscount != 0
    val context = tv.context
    tv.text = if (discountExist)
        context.getString(R.string.text_discount, parsedDiscount)
    else
        context.getString(R.string.fallback_discount_not_specified)
}

@BindingAdapter(value = ["barcodeValue", "barcodeFormat", "barcodeWidth", "barcodeHeight"], requireAll = false)
fun setBarCode(im: ImageView, value: String?, format: String?, width: Float, height: Float) {
    if (value != null && format != null) {
        val widthInt = width.toInt()
        val heightInt = height.toInt()
        val bitmap = BarcodeDecoder.decode(value, format, widthInt, heightInt)
        if (bitmap == null) {
            (im.parent as View).visibility = View.GONE
            return
        }
        im.setImageBitmap(bitmap)

    }
}
