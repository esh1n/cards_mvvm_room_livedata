package com.dataart.dartcard.presentation.main.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import com.birbit.android.jobqueue.JobManager
import com.dataart.dartcard.domain.SchedulersFacade
import com.dataart.dartcard.domain.cards.CardsRepository
import com.dataart.dartcard.domain.favorite.UserCardRepository
import com.dataart.dartcard.domain.jobmanager.startSync
import com.dataart.dartcard.presentation.main.mapper.MyCardEntityMapper
import com.dataart.dartcard.presentation.main.models.CardModel
import io.reactivex.Completable

class MyCardsViewModel internal constructor(private val cardsRepository: CardsRepository,
                                            private val favsRepository: UserCardRepository,
                                            private val jobManager: JobManager)
    : SearchBrandViewModel<List<CardModel>>() {

    private val cardEntityMapper: MyCardEntityMapper = MyCardEntityMapper()


    fun manageFavoriteState(card: CardModel) {
        addDisposable(
                favsRepository.updateFavoriteState(card)
                        .compose(SchedulersFacade.applySchedulersCompletable())
                        .doOnSubscribe { _ -> loadingStatus.setValue(true) }
                        .doAfterTerminate { loadingStatus.setValue(false) }
                        .subscribe({ }, { throwable -> error.setValue(throwable) }))
    }

    override fun searchExactQuery(query: String): LiveData<List<CardModel>> {
        return Transformations.map(cardsRepository.loadMyCardsFromDBByQuery(query)) { cardEntityMapper.map(it) }
    }

    fun refreshMyCards() {
        addDisposable(
                Completable.fromCallable { jobManager.startSync() }
                .compose(SchedulersFacade.applySchedulersCompletable())
                .doOnSubscribe { _ ->
                    loadingStatus.setValue(true)
                }
                .doAfterTerminate {
                    loadingStatus.setValue(false)
                }
                .subscribe({ }, { throwable -> error.setValue(throwable) }))
    }
}
