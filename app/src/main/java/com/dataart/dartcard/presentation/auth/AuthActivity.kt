package com.dataart.dartcard.presentation.auth

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment

import com.dataart.dartcard.presentation.base.SingleFragmentActivity
import com.dataart.dartcard.presentation.base.startActivity


class AuthActivity : SingleFragmentActivity() {

    override fun getStartScreen(savedInstanceState: Bundle?): Fragment {
        return AuthFragment()
    }

    companion object {


        fun start(activity: Activity?) {
            activity.startActivity<AuthActivity>()
        }
    }
}
