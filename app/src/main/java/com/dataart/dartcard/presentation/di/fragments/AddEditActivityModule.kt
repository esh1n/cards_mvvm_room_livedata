package com.dataart.dartcard.presentation.di.fragments

import com.dataart.dartcard.presentation.addedit.fragment.AddEditCardFragment
import com.dataart.dartcard.presentation.addedit.fragment.BarcodeInputFragment
import com.dataart.dartcard.presentation.addedit.fragment.ChooseBrandFragment
import com.dataart.dartcard.presentation.addedit.fragment.EditCardFragment
import com.dataart.dartcard.presentation.di.module.ViewModelsModule

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface AddEditActivityModule {

    @ContributesAndroidInjector(modules = [ViewModelsModule::class])
    fun contributeAddEditCardFragment(): AddEditCardFragment

    @ContributesAndroidInjector(modules = [ViewModelsModule::class])
    fun contributeEditCardFragment(): EditCardFragment

    @ContributesAndroidInjector(modules = [ViewModelsModule::class])
    fun contributeChooseBrandFragment(): ChooseBrandFragment

    @ContributesAndroidInjector(modules = [ViewModelsModule::class])
    fun contributeAddBarcodeManuallyFragment(): BarcodeInputFragment
}

