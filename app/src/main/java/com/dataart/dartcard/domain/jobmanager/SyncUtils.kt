package com.dataart.dartcard.domain.jobmanager

import com.birbit.android.jobqueue.JobManager
import com.dataart.dartcard.domain.jobmanager.jobs.*


val SYNC_DB_ID: String get() = "SYNC_WORK_ID"
val SYNC_JOB_TAG: String get() = "SYNC_JOB_TAG"

fun JobManager.startSync() {
    addJobInBackground(SyncAllJob())
}

fun JobManager.addCard(newCardId: Long) {
    addJobInBackground(AddCardJob(newCardId))
}

fun JobManager.editCard(cardId: Long) {
    addJobInBackground(EditCardJob(cardId))
}

fun JobManager.deleteCard(serverCardId: String) {
    addJobInBackground(DeleteCardJob(serverCardId))
}

fun JobManager.syncLike(cardId: Int) {
    addJobInBackground(SyncLikeJob(cardId))
}

//fun WorkManager.startPeriodicSync() {
//    val constraints = Constraints.Builder()
//            .setRequiredNetworkType(NetworkType.CONNECTED)
//            .build()
//
//    val syncAllDataWorker = PeriodicWorkRequest.Builder(SyncAllDataWorker::class.java, 2L, TimeUnit.DAYS)
//            .setConstraints(constraints)
//            .build()
//
//    enqueueUniquePeriodicWork(SYNC_DB_ID, ExistingPeriodicWorkPolicy.KEEP, syncAllDataWorker);
//
//}
