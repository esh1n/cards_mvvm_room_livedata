package com.dataart.dartcard.domain.jobmanager.jobs

import android.util.Log

class SyncAllJob : BaseSyncJob(singleId = generateSingleId(jobType = JobType.SYNC_ALL)) {

    override fun onAdded() {

        Log.d(TAG, "Executing onAdded() for sync all ")
    }

    @Throws(Throwable::class)
    override fun onRun() {
        Log.d(TAG, "Executing onRun() for sync all")
        val token = cacheContext.currentSessionToken
        if (token.isNullOrBlank()) {
            return
        }
        val (persons, cards, favorites, brands) = syncRepository.getAllCacheFromServer.blockingGet()
        with(syncRepository) {
            syncUsers(persons)
            syncBrands(brands)
            syncCards(cards)
            syncLikes(favorites)
        }
        cacheContext.refreshSyncTimestamp()
    }

    companion object {
        private const val TAG = "SyncAllJob"
    }
}
