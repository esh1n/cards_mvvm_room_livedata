package com.dataart.dartcard.domain.favorite

import android.arch.lifecycle.LiveData
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.birbit.android.jobqueue.JobManager
import com.dataart.dartcard.data.UserSessionApiService
import com.dataart.dartcard.data.room.AppDatabase
import com.dataart.dartcard.data.room.dao.FavoriteCardDao
import com.dataart.dartcard.data.room.dao.UserDao
import com.dataart.dartcard.data.room.entity.BrandedCard
import com.dataart.dartcard.data.room.entity.FavoriteCardEntry
import com.dataart.dartcard.domain.jobmanager.syncLike
import com.dataart.dartcard.presentation.main.models.CardModel
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class UserCardRepository @Inject
constructor(private val userSessionApiService: UserSessionApiService, database: AppDatabase, private val jobManager: JobManager) {

    private val dao: FavoriteCardDao = database.favCardDao()
    private val userDao: UserDao = database.userDao()

    fun getFromApi(): Completable {
        return userSessionApiService.favorites
                .map<List<FavoriteCardEntry>> { FavoriteMapper().map(it) }.flatMapCompletable { favs ->
                    Completable.fromAction {
                        dao.bulkInsert(favs)
                        dao.bulkUpdate(favs)
                    }
                }
    }

    fun getFavoritesCardsFromCache(brandName: String): LiveData<PagedList<BrandedCard>> {
        return LivePagedListBuilder(dao
                .loadFavoriteCardsForUser("%", brandName), 20)
                .build()
    }

    fun updateFavoriteState(card: CardModel): Completable {
        return Single.just(card)
                .flatMapCompletable { (id) ->
                    if (card.favorite) {
                        return@flatMapCompletable unFavoriteCard(id)
                    } else {
                        return@flatMapCompletable favoriteCard(id)
                    }
                }
    }

    private fun favoriteCard(cardId: String): Completable {
        return Single
                .fromCallable { dao.updateOrInsertLike(cardId.toInt(), userDao.ownerId()!!) }
                .flatMapCompletable {
                    Completable.fromAction { jobManager.syncLike(cardId.toInt()) }
                }
    }

    fun unFavoriteCard(cardId: String): Completable {
        return Single
                .fromCallable { dao.updateOrInsertUnLike(cardId.toInt(), userDao.ownerId()!!) }
                .flatMapCompletable {
                    Completable.fromAction { jobManager.syncLike(cardId.toInt()) }
                }

    }
}
