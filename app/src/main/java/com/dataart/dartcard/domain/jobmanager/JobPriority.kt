package com.dataart.dartcard.domain.jobmanager

enum class JobPriority(val value: Int) {
    LOW(0), MID(50), HIGH(100)

}