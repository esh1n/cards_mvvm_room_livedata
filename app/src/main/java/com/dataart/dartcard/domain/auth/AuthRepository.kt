package com.dataart.dartcard.domain.auth

import com.birbit.android.jobqueue.JobManager
import com.birbit.android.jobqueue.TagConstraint
import com.dataart.dartcard.data.AuthorizationApiService
import com.dataart.dartcard.data.cache.CacheContext
import com.dataart.dartcard.data.room.AppDatabase
import com.dataart.dartcard.domain.jobmanager.SYNC_JOB_TAG
import io.reactivex.Completable
import io.reactivex.Observable


class AuthRepository(private val database: AppDatabase, private val cacheContext: CacheContext, private val authorizationApiService: AuthorizationApiService, private val jobManager: JobManager) {

    val isAuthorized: Observable<Boolean>
        get() {
            return Observable.just(!cacheContext.currentSessionToken.isNullOrBlank())
        }

    fun login(authCode: String, email: String): Completable {
        return Completable.fromAction { ->
            if (email != cacheContext.userEmail) {
                cacheContext.clearSession()
                database.clearAllTables()
                cacheContext.userEmail = email
            }
        }.andThen(authorizationApiService
                .getToken(authCode))
                .map { it.token }
                .flatMapCompletable { token ->
                    Completable.fromAction {
                        cacheContext.currentSessionToken = token
                    }
                }

    }

    fun logout(): Completable {
        return Completable.fromAction {
            jobManager.cancelJobs(TagConstraint.ANY, SYNC_JOB_TAG)
            cacheContext.clearToken()
        }
    }
}
