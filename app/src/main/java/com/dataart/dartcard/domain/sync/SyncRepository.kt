package com.dataart.dartcard.domain.sync

import android.util.Log
import com.birbit.android.jobqueue.JobManager
import com.dataart.dartcard.data.UserSessionApiService
import com.dataart.dartcard.data.cache.request.AddEditCardRequest
import com.dataart.dartcard.data.cache.request.mapper.CardEntryRequestMapper
import com.dataart.dartcard.data.cache.response.AddEditCardIdsResponse
import com.dataart.dartcard.data.cache.response.FavoriteCard
import com.dataart.dartcard.data.room.AppDatabase
import com.dataart.dartcard.data.room.dao.BrandDao
import com.dataart.dartcard.data.room.dao.CardDao
import com.dataart.dartcard.data.room.dao.FavoriteCardDao
import com.dataart.dartcard.data.room.dao.UserDao
import com.dataart.dartcard.data.room.entity.BrandEntry
import com.dataart.dartcard.data.room.entity.CardEntry
import com.dataart.dartcard.data.room.entity.LikeUpdateEntry
import com.dataart.dartcard.data.room.entity.UserEntry
import com.dataart.dartcard.data.room.util.RetrofitImageUtil
import com.dataart.dartcard.domain.AllCacheEntity
import com.dataart.dartcard.domain.cards.mapper.CardMapper
import com.dataart.dartcard.domain.jobmanager.addCard
import com.dataart.dartcard.domain.jobmanager.editCard
import com.dataart.dartcard.domain.jobmanager.syncLike
import com.dataart.dartcard.domain.mapper.AllCacheMapper
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import java.io.IOException
import javax.inject.Inject


class SyncRepository @Inject
constructor(private val apiService: UserSessionApiService, database: AppDatabase, private val jobManager: JobManager) {
    private val userDao: UserDao = database.userDao()
    private val brandDao: BrandDao = database.brandDao()
    private val cardDao: CardDao = database.cardDao()
    private val favoriteCardDao: FavoriteCardDao = database.favCardDao()

    val getAllCacheFromServer: Single<AllCacheEntity>
        get() = apiService.syncAll().map { AllCacheMapper().map(it) }

    fun syncUsers(users: List<UserEntry>) {
        userDao.insertOrUpdateUsers(users)
    }

    fun syncBrands(brands: List<BrandEntry>) {
        val brandsFromServer = mutableListOf<BrandEntry>()
        brandsFromServer.addAll(brands)
        val allCacheBrands = brandDao.allBrands()

        brandDao.deleteOutdatedBrands(brandsFromServer.map { it.serverId!!.toInt() }.toIntArray())

        val newServerBrands = brandsFromServer.filter { serverBrand -> !allCacheBrands.any { it.name == serverBrand.name } }
        if (!newServerBrands.isEmpty()) {
            brandDao.bulkInsert(newServerBrands)
        }
        brandsFromServer.removeAll(newServerBrands)
        val cacheAndServerBrandsToUpdate = brandsFromServer.filter { serverBrand -> allCacheBrands.any { it.serverId == serverBrand.serverId && it.synced } }
        brandDao.bulkUpdate(cacheAndServerBrandsToUpdate)
    }

    fun syncCards(cards: List<CardEntry>) {
        val cardsFromServer = mutableListOf<CardEntry>()
        cardsFromServer.addAll(cards)


        cardDao.deleteOutdatedCards(cardsFromServer.map { it.serverId!!.toInt() }.toIntArray())

        val allCacheCards = cardDao.allCards()
        val newServerCards = cardsFromServer.filter { serverCard -> !allCacheCards.any { it.serverId == serverCard.serverId } }
        if (!newServerCards.isEmpty()) {
            cardDao.bulkInsert(newServerCards)
        }
        cardsFromServer.removeAll(newServerCards)
        cardDao.bulkUpdateServerCards(cardsFromServer)
        brandDao.deleteAllEmptyBrands()
        allCacheCards.filter { !it.synced }.forEach {
            if (it.serverId.isNullOrBlank()) {
                jobManager.addCard(it.id.toLong())
            } else {
                jobManager.editCard(it.id.toLong())
            }
        }

    }

    fun syncLikes(likes: List<FavoriteCard>) {
        favoriteCardDao.syncLikes(likes)
        if (favoriteCardDao.checkIfUnSyncedLikesExist()) {
            val notSyncedLikesAndDisLikes = favoriteCardDao.getNotSyncedLikesAndDisLikes()
            Observable
                    .fromIterable(notSyncedLikesAndDisLikes)
                    .flatMapCompletable { like -> Completable.fromAction { jobManager.syncLike(like.localId) } }
                    .blockingAwait()
        }

    }

    fun getCardWithBrandFromDb(id: Long) = cardDao.getCardWithBrandById(id)

    fun updateCardServerId(id: Long, serverId: String) = cardDao.updateServerId(id, serverId)

    fun editCardOnServer(serverId: String, cardRequest: AddEditCardRequest) = apiService.editCard(serverId, cardRequest)

    fun addCardOnServer(cardRequest: AddEditCardRequest) = apiService.addCard(cardRequest)

    fun deleteCard(serverCardId: String) {
        apiService
                .deleteCard(serverCardId)
                .blockingAwait()
    }


    fun uploadCardPhotosIfNeeded(cardEntry: CardEntry, cardResponse: AddEditCardIdsResponse): Single<Triple<String, String?, String?>> {
        val hasPhotoToUpload = checkIfCardHasPhotoToUpload(cardEntry)
        return if (hasPhotoToUpload) {
            uploadPhotos(cardResponse.id, cardEntry.facePhoto, cardEntry.backPhoto)
        } else {
            Log.d("RxJavaWork", "update no photo")
            Single.just(Triple(cardResponse.id, null, null))
        }
    }

    private fun checkIfCardHasPhotoToUpload(cardEntry: CardEntry): Boolean {
        val facePhoto = cardEntry.facePhoto
        val backPhoto = cardEntry.backPhoto
        val needUploadFacePhoto = !(facePhoto.isNullOrBlank() || facePhoto!!.startsWith("https"))
        val needUploadBackPhoto = !(backPhoto.isNullOrBlank() || backPhoto!!.startsWith("https"))
        return needUploadFacePhoto || needUploadBackPhoto
    }

    @Throws(IOException::class)
    private fun uploadPhotos(cardId: String, facePhotoUriOrUrl: String?, backPhotoUriOrUrl: String?): Single<Triple<String, String?, String?>> {
        val facePhotoURI = if (facePhotoUriOrUrl?.startsWith("https") == false) facePhotoUriOrUrl else null
        val backPhotoURI = if (backPhotoUriOrUrl?.startsWith("https") == false) backPhotoUriOrUrl else null
        val facePhotoPart = RetrofitImageUtil.tryConvertImage("frontPhoto", facePhotoURI)
        val backPhotoPart = RetrofitImageUtil.tryConvertImage("backPhoto", backPhotoURI)

        return apiService.uploadPhoto(facePhotoPart, backPhotoPart, cardId)
                .map { (frontPhotoId, backPhotoId) ->
                    Log.d("UpdloadImage", "photo $frontPhotoId $backPhotoId")
                    val facePhotoUrl = if (frontPhotoId.isNullOrBlank()) facePhotoUriOrUrl else CardMapper.generateUrl(frontPhotoId)
                    val backPhotoUrl = if (backPhotoId.isNullOrBlank()) backPhotoUriOrUrl else CardMapper.generateUrl(backPhotoId)
                    Triple(cardId, facePhotoUrl, backPhotoUrl)
                }
    }

    fun updateBrandIfNeeded(brandEntry: BrandEntry, brandId: String) {
        if (brandEntry.serverId.isNullOrBlank()) {
            brandEntry.serverId = brandId
            brandEntry.synced = true
            val updated = brandDao.update(brandEntry)
            Log.d("RxJavaWork", "updated brand ${brandEntry.name} $updated")
        }
    }

    fun syncLike(id: Int): Completable {
        return Single.fromCallable { favoriteCardDao.getNotSyncedLike(id) }
                .flatMap(this::likeOnServer)
                .flatMapCompletable { localId -> Completable.fromAction { favoriteCardDao.updateSyncedState(localId) } }

    }

    private fun likeOnServer(like: LikeUpdateEntry): Single<Int> {
        return if (like.serverId == null) {
            addCard(like.localId.toLong())
                    .map { serverId -> return@map like.copy(serverId = serverId) }
                    .flatMap { manageLikeState(it) }
        } else {
            manageLikeState(like)
        }
    }

    private fun manageLikeState(like: LikeUpdateEntry): Single<Int> {
        return if (like.favorite) {
            apiService.favoriteCard(like.serverId!!).andThen(Single.just(like.localId))
        } else {
            apiService.unFavoriteCard(like.serverId!!).andThen(Single.just(like.localId))
        }
    }

    fun addCard(cardId: Long): Single<String> {
        val cardWithBrand = getCardWithBrandFromDb(cardId)
        val card = cardWithBrand.cardEntry
        val brand = cardWithBrand.brand
        return Single.just(card)
                .flatMap { it ->
                    if (it.synced)
                        Single.just(card.serverId)
                    else
                        addUnSyncedCard(card, brand)
                }
    }

    private fun addUnSyncedCard(card: CardEntry, brand: BrandEntry): Single<String> {
        return Single.just(card)
                .filter { !card.synced }
                .map {
                    CardEntryRequestMapper().map(it)
                }
                .flatMapSingle { cardRequest ->
                    Log.d("RxJava", "before request ${Thread.currentThread().name}")
                    addCardOnServer(cardRequest)
                }
                .flatMap {
                    Log.d("RxJava", "startUpdate ${Thread.currentThread().name}")
                    val updated = updateCardServerId(card.id.toLong(), it.id)
                    Log.d("RxJavaWork", "updated ${card.id} $updated")
                    updateBrandIfNeeded(brand, it.brandId)
                    return@flatMap uploadCardPhotosIfNeeded(card, it)
                }
                .flatMap {
                    val serverId = it.first
                    val facePhoto = it.second
                    val backPhoto = it.third
                    updateCardPhotosUrlByServerId(serverId, facePhoto, backPhoto)
                    return@flatMap Single.just(serverId)
                }
    }

    fun updateCardPhotosUrlByServerId(serverId: String, facePhotoUrl: String?, backPhotoUrl: String?): Int {
        return cardDao.updatePhotosByServerId(serverId, facePhotoUrl, backPhotoUrl)
    }
}


