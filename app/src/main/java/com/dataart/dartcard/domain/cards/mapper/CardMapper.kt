package com.dataart.dartcard.domain.cards.mapper

import com.dataart.dartcard.BuildConfig
import com.dataart.dartcard.data.cache.response.Card
import com.dataart.dartcard.data.room.entity.CardEntry
import com.dataart.dartcard.domain.mapper.Mapper


class CardMapper(private val brandNameGetter: (String) -> String) : Mapper<Card, CardEntry>() {
    override fun map(source: Card): CardEntry {
        return CardEntry(serverId = source.id,
                ownerId = source.ownerId,
                brandId = brandNameGetter.invoke(source.brandId),
                barCode = source.barCode,
                cardNumber = source.cardNumber,
                barCodeFormat = source.barCodeFormat,
                note = source.note,
                facePhoto = generateUrl(source.facePhoto),
                backPhoto = generateUrl(source.backPhoto),
                discount = source.discount,
                isPrivate = source.isPrivate,
                synced = true)
    }

    companion object {

        fun generateUrl(photoId: String?): String? {
            return if (photoId.isNullOrBlank()) {
                null
            } else {
                val updatedPhotoId = if (BuildConfig.FLAVOR == "dev") photoId?.replaceFirst("disco", "disco_dev") else photoId
                BuildConfig.API_ENDPOINT + updatedPhotoId
            }
        }

        fun getOwnerName(first: String?, last: String?): String {
            return "${first ?: ""} ${last ?: ""}"
        }
    }
}
