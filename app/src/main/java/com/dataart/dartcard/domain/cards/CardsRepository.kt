package com.dataart.dartcard.domain.cards

import android.arch.lifecycle.LiveData
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import android.util.Log
import com.birbit.android.jobqueue.JobManager
import com.dataart.dartcard.data.UserSessionApiService
import com.dataart.dartcard.data.room.AppDatabase
import com.dataart.dartcard.data.room.dao.BrandDao
import com.dataart.dartcard.data.room.dao.CardDao
import com.dataart.dartcard.data.room.dao.FavoriteCardDao
import com.dataart.dartcard.data.room.dao.UserDao
import com.dataart.dartcard.data.room.entity.*
import com.dataart.dartcard.domain.AllCacheEntity
import com.dataart.dartcard.domain.cards.mapper.AddCardEntryMapper
import com.dataart.dartcard.domain.jobmanager.addCard
import com.dataart.dartcard.domain.jobmanager.deleteCard
import com.dataart.dartcard.domain.jobmanager.editCard
import com.dataart.dartcard.domain.mapper.AllCacheMapper
import com.dataart.dartcard.presentation.addedit.mapper.EditCardEntryMapper
import com.dataart.dartcard.presentation.addedit.model.AddEditCardModel
import com.dataart.dartcard.presentation.main.mapper.NewBrandMapper
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class CardsRepository @Inject
constructor(private val userSessionApiService: UserSessionApiService, database: AppDatabase, private val jobManager: JobManager) {

    private val cardDao: CardDao = database.cardDao()
    private val favoriteCardDao: FavoriteCardDao = database.favCardDao()
    private val userDao: UserDao = database.userDao()
    private val brandDao: BrandDao = database.brandDao()

    val allCache: Single<AllCacheEntity>
        get() = userSessionApiService.syncAll()
                .map { AllCacheMapper().map(it) }

//    fun fetchFromApi(): Single<List<CardEntry>> {
//        return userSessionApiService.cards
//                .map { CardMapper(brandMap).map(it) }
//    }

    fun loadBarCode(cardId: String): Single<CardWithBarcodeEntry> {
        return cardDao.cardBarCode(cardId.toInt())
    }

    fun loadBrandCards(name: String): LiveData<PagedList<CardBrandDetails>> {
        return LivePagedListBuilder(cardDao.loadCardsByBrand(name), 20).build()
    }

    fun addCard(addCardModel: AddEditCardModel): Completable {
        return Single
                .just(addCardModel)
                .map { model ->
                    insertBrandIfNeeded(addCardModel.brandModel!!.name)
                    return@map model
                }.map { model: AddEditCardModel ->
                    val ownerId = userDao.ownerId()
                            ?: throw IllegalStateException("Null pointer of ownerId.Please try update your data")
                    val mappedEntry = AddCardEntryMapper(ownerId).map(model)
                    return@map mappedEntry
                }.map { cardToInsert ->
                    return@map insertNewCard(cardToInsert)
                }.flatMapCompletable { newCardId ->
                    Completable.fromAction {
                        jobManager.addCard(newCardId)
                    }
                }
    }

    private fun insertNewCard(cardEntry: CardEntry): Long {
        Log.d("addCard", "insert in db " + cardEntry.id)
        val cardId = cardDao.insert(cardEntry)
        val favoriteCardEntry = FavoriteCardEntry(cardId.toInt(), cardEntry.ownerId!!, synced = false)
        favoriteCardDao.insert(favoriteCardEntry)
        return cardId
    }

    private fun insertBrandIfNeeded(brandName: String) {
        if (brandDao.checkIfBrandNotExist(brandName)) {
            val brandEntry = NewBrandMapper().map(brandName)
            brandDao.insert(brandEntry)
        }
    }

    fun editCard(cardModel: AddEditCardModel): Completable {
        return Single
                .just(cardModel)
                .map { model ->
                    insertBrandIfNeeded(cardModel.brandModel!!.name)
                    return@map model
                }.map { model: AddEditCardModel ->
                    val mappedEntry = EditCardEntryMapper().map(model)
                    return@map mappedEntry
                }.map { cardToUpdate ->
                    cardDao.updateCardFromUser(cardToUpdate)
                    return@map cardToUpdate.id
                }.flatMapCompletable { localCardId ->
                    Completable.fromAction {
                        jobManager.editCard(localCardId.toLong())
                    }
                }
    }


    fun getCardById(id: String): LiveData<CardDetailsEntry?> {
        return cardDao.getCardById(id.toInt())
    }

    fun deleteCard(id: String): Completable {
        return Single.just(id)
                .map { return@map cardDao.deleteAndReturnServerId(id.toInt()) }
                .filter { serverId ->
                    !serverId.isBlank()
                }
                .flatMapCompletable { serverId ->
                    Completable.fromAction { jobManager.deleteCard(serverId) }
                }


    }

    fun loadMyCardsFromDBByQuery(query: String): LiveData<List<MyCardsEntry>> {
        return cardDao.loadMyCardsByName(query)
    }

    fun loadCardsByBrandNameOrOwnerName(query: String): LiveData<List<SearchCardEntry>> {
        return cardDao.loadCardsByBrandNameOrOwnerName(query)
    }

}
