package com.dataart.dartcard.domain.user

import com.dataart.dartcard.data.UserSessionApiService
import com.dataart.dartcard.data.room.AppDatabase
import com.dataart.dartcard.data.room.dao.UserDao
import javax.inject.Inject

class UserRepository @Inject
constructor(private val userSessionApiService: UserSessionApiService, database: AppDatabase) {
    private val userDao: UserDao = database.userDao()

}
