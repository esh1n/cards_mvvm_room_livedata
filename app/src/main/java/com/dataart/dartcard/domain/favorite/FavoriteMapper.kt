package com.dataart.dartcard.domain.favorite

import com.dataart.dartcard.data.cache.response.FavoriteCard
import com.dataart.dartcard.data.room.entity.FavoriteCardEntry
import com.dataart.dartcard.domain.mapper.Mapper

class FavoriteMapper : Mapper<FavoriteCard, FavoriteCardEntry>() {
    override fun map(source: FavoriteCard): FavoriteCardEntry {
        return FavoriteCardEntry(cardId = source.cardId, userId = source.userId)
    }
}
