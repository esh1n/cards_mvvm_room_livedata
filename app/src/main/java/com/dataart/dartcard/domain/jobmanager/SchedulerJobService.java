package com.dataart.dartcard.domain.jobmanager;

import android.support.annotation.NonNull;

import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.scheduling.FrameworkJobSchedulerService;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

public class SchedulerJobService extends FrameworkJobSchedulerService {


    @Inject
    JobManager manager;

    @Override
    public void onCreate() {
        AndroidInjection.inject(this);
        super.onCreate();
    }

    @NonNull
    @Override
    protected JobManager getJobManager() {

        return manager;
    }
}

