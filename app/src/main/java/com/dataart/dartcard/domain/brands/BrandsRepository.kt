package com.dataart.dartcard.domain.brands

import android.arch.lifecycle.LiveData
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.dataart.dartcard.data.UserSessionApiService
import com.dataart.dartcard.data.room.AppDatabase
import com.dataart.dartcard.data.room.dao.BrandDao
import com.dataart.dartcard.data.room.entity.BrandEntry
import com.dataart.dartcard.data.room.entity.BrandWithPhoto
import com.dataart.dartcard.presentation.main.mapper.AddBrandEntryMapper
import com.dataart.dartcard.presentation.main.models.BrandModel
import io.reactivex.Maybe
import io.reactivex.Single
import javax.inject.Inject

class BrandsRepository @Inject
constructor(private val userSessionApiService: UserSessionApiService, database: AppDatabase) {

    private val brandDao: BrandDao = database.brandDao()

    fun getBrands(brandName: String): LiveData<PagedList<BrandWithPhoto>> {
        return LivePagedListBuilder(brandDao.allBrandsWithPhotoByName(brandName), 20).build()
    }



    fun findBrandsByName(name: CharSequence): Maybe<List<BrandModel>> {
        return brandDao.getBrandsByName(name.toString() + "%")
                .map { AddBrandEntryMapper().map(it) }
    }

    fun tryToFindBrandByName(nameFromEditText: String): Maybe<BrandModel> {
        val brandModel = BrandModel(nameFromEditText, null, null, false)
        return brandDao.getBrandByName(nameFromEditText)
                .map<BrandModel> { AddBrandEntryMapper().map(it) }
                .defaultIfEmpty(brandModel)
    }

    fun fetchFromApi(): Single<List<BrandEntry>> {
        return userSessionApiService.brands
                .map { BrandsMapper().map(it) }
    }

    fun saveBrands(brands: List<BrandEntry>) {
        brandDao.bulkInsert(brands)
        brandDao.bulkUpdate(brands)
    }

    fun getBrand(brandName: String): LiveData<BrandEntry> {
        return brandDao.getLiveBrandByName(brandName)
    }

    fun deleteAllEmptyBrands() {
        brandDao.deleteAllEmptyBrands()
    }
}
