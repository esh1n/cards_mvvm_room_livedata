package com.dataart.dartcard.domain.sync

import com.dataart.dartcard.data.cache.response.Card
import com.dataart.dartcard.data.cache.response.User
import com.dataart.dartcard.data.room.entity.UserEntry
import com.dataart.dartcard.domain.cards.mapper.CardMapper
import java.util.*

class UsersMapper(val owner: User, val cards: List<Card>) {

    fun map() = ArrayList(aggregateUsers(cards, owner))

    private fun aggregateUsers(cards: List<Card>, owner: User): Set<UserEntry> {
        return HashSet<UserEntry>().apply {
            val ownerUserEntry = UserEntry(id = owner.id,
                    name = CardMapper.getOwnerName(owner.firstName, owner.lastName),
                    owner = true)
            add(ownerUserEntry)
            for ((_, ownerId, _, _, _, _, _, _, _, _, ownerFirstName, ownerLastName) in cards) {
                val user = UserEntry(id = ownerId,
                        name = CardMapper.getOwnerName(ownerFirstName, ownerLastName),
                        owner = ownerId == owner.id)
                add(user)
            }
        }
    }
}