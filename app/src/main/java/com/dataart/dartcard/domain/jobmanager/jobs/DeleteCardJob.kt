package com.dataart.dartcard.domain.jobmanager.jobs

import android.util.Log

class DeleteCardJob(private val serverCardId: String) : BaseSyncJob(singleId = generateSingleId(serverCardId, JobType.DELETE_CARD)) {


    override fun onRun() {
        syncRepository.deleteCard(serverCardId)
        Log.d(TAG, "Executing onRun() for comment $serverCardId")
    }

    override fun onAdded() {
        Log.d(TAG, "added job");
    }

    companion object {
        private const val TAG = "AddCardJob"
    }

}