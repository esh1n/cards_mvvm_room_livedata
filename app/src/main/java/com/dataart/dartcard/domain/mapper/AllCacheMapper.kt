package com.dataart.dartcard.domain.mapper

import com.dataart.dartcard.data.cache.response.AllCacheResponse
import com.dataart.dartcard.domain.AllCacheEntity
import com.dataart.dartcard.domain.brands.BrandsMapper
import com.dataart.dartcard.domain.cards.mapper.CardMapper
import com.dataart.dartcard.domain.sync.UsersMapper

class AllCacheMapper : Mapper<AllCacheResponse, AllCacheEntity>() {
    //TODO make mapping to allcache responce , not use entities
    override fun map(source: AllCacheResponse): AllCacheEntity {
        val users = UsersMapper(source.person, source.cards.orEmpty()).map()
        val brandEntries = BrandsMapper().map(source.brands)
        val brandMap = source.brands.orEmpty().associate { it.id to it.name }
        val brandNameGetter = { serverId: String -> brandMap[serverId]!! }
        val cardEntries = CardMapper(brandNameGetter).map(source.cards)
        return AllCacheEntity(users, cardEntries, source.favorites.orEmpty(), brandEntries)

    }


}
