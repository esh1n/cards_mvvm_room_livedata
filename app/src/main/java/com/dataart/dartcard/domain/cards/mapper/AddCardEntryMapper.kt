package com.dataart.dartcard.domain.cards.mapper

import com.dataart.dartcard.data.room.entity.CardEntry
import com.dataart.dartcard.domain.mapper.Mapper
import com.dataart.dartcard.presentation.addedit.model.AddEditCardModel

open class AddCardEntryMapper(val ownerId: String) : Mapper<AddEditCardModel, CardEntry>() {

    override fun map(source: AddEditCardModel): CardEntry {

        return CardEntry(
                synced = false,
                serverId = source.serverId,
                brandId = source.brandModel!!.name,
                ownerId = ownerId,
                barCodeFormat = source.barCode?.format,
                barCode = source.barCode?.value,
                facePhoto = source.facePhoto,
                backPhoto = source.backPhoto,
                note = source.note,
                cardNumber = source.cardNumber,
                isPrivate = source.isPrivate,
                discount = source.discount)
    }

}
