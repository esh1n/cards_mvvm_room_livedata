package com.dataart.dartcard.domain.brands

import com.dataart.dartcard.data.cache.response.Brand
import com.dataart.dartcard.data.room.entity.BrandEntry
import com.dataart.dartcard.data.room.util.StringSearchUtil
import com.dataart.dartcard.domain.mapper.Mapper

class BrandsMapper : Mapper<Brand, BrandEntry>() {

    override fun map(source: Brand): BrandEntry {
        val searchableName = StringSearchUtil.applyDefaultSearchRules(source.name)
        return BrandEntry(serverId = source.id, name = source.name, searchableName = searchableName, synced = true);
    }
}
