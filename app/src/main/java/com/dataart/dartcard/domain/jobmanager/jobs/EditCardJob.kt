package com.dataart.dartcard.domain.jobmanager.jobs

import android.util.Log
import com.dataart.dartcard.data.cache.request.mapper.CardEntryRequestMapper
import io.reactivex.Completable
import io.reactivex.Single


class EditCardJob(private val cardId: Long) : BaseSyncJob(singleId = generateSingleId(cardId.toString(), JobType.EDIT_CARD)) {

    override fun onRun() {
        if (!isCancelled) {
            editCard(cardId)
        }
        Log.d(TAG, "Executing onRun() for comment $cardId")
    }

    override fun onAdded() {
        Log.d(TAG, "added job");
    }

    companion object {
        private const val TAG = "AddCardJob"
    }

    private fun editCard(cardId: Long) {
        with(syncRepository) {
            val cardWithBrand = getCardWithBrandFromDb(cardId)
            val card = cardWithBrand.cardEntry
            val brand = cardWithBrand.brand
            if (card.synced) {
                return
            }
            Single.just(card)
                    .map {
                        CardEntryRequestMapper().map(it)
                    }
                    .flatMap {
                        editCardOnServer(card.serverId!!, it)
                    }
                    .flatMap {
                        updateBrandIfNeeded(brand, it.brandId)
                        return@flatMap uploadCardPhotosIfNeeded(card, it)
                    }
                    .flatMapCompletable {
                        val facePhoto = it.second
                        val backPhoto = it.third
                        val serverId = it.first
                        Completable.fromAction {
                            updateCardPhotosUrlByServerId(serverId, facePhoto, backPhoto)
                        }
                    }
                    .blockingAwait()
        }

    }

}