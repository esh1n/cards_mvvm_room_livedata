package com.dataart.dartcard.domain.jobmanager.jobs

import android.util.Log
import io.reactivex.Completable

class AddCardJob(private val cardId: Long) : BaseSyncJob(singleId = generateSingleId(cardId.toString(), JobType.ADD_CARD)) {

    override fun onRun() {
        if (!isCancelled) {
            syncRepository
                    .addCard(cardId)
                    .flatMapCompletable { Completable.complete() }
                    .blockingAwait()
        }
        Log.d(TAG, "Executing onRun() for comment $cardId")
    }

    override fun onAdded() {
        Log.d(TAG, "added job");
    }

    companion object {
        private const val TAG = "AddCardJob"
    }

}




