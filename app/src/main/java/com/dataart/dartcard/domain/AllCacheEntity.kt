package com.dataart.dartcard.domain

import com.dataart.dartcard.data.cache.response.FavoriteCard
import com.dataart.dartcard.data.room.entity.BrandEntry
import com.dataart.dartcard.data.room.entity.CardEntry
import com.dataart.dartcard.data.room.entity.UserEntry

data class AllCacheEntity(val persons: List<UserEntry>,
                          val cards: List<CardEntry>,
                          val favorites: List<FavoriteCard>,
                          val brands: List<BrandEntry>)
