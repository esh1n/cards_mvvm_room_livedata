package com.dataart.dartcard.domain.jobmanager.jobs

import android.util.Log
import com.birbit.android.jobqueue.Job
import com.birbit.android.jobqueue.Params
import com.birbit.android.jobqueue.RetryConstraint
import com.dataart.dartcard.data.cache.CacheContext
import com.dataart.dartcard.data.job.JobResult
import com.dataart.dartcard.data.job.JobResultStatus
import com.dataart.dartcard.data.job.RxJobResultBus
import com.dataart.dartcard.data.retrofit.RefreshTokenException
import com.dataart.dartcard.data.retrofit.RetrofitException
import com.dataart.dartcard.domain.jobmanager.JobPriority
import com.dataart.dartcard.domain.jobmanager.SYNC_DB_ID
import com.dataart.dartcard.domain.jobmanager.SYNC_JOB_TAG
import com.dataart.dartcard.domain.sync.SyncRepository
import javax.inject.Inject

abstract class BaseSyncJob(groupId: String = SYNC_DB_ID, tag: String = SYNC_JOB_TAG, singleId: String? = null) :
        Job(Params(JobPriority.MID.value).requireNetwork().groupBy(groupId).addTags(tag).singleInstanceBy(singleId).persist()) {
    @Transient
    @Inject
    lateinit var syncRepository: SyncRepository


    @Transient
    @Inject
    lateinit var jobResultBus: RxJobResultBus

    @Transient
    @Inject
    lateinit var cacheContext: CacheContext

    override fun shouldReRunOnThrowable(throwable: Throwable, runCount: Int, maxRunCount: Int): RetryConstraint {
        if (runCount > maxRunCount) return RetryConstraint.CANCEL;
        if (throwable is RetrofitException) {

            if (throwable is RefreshTokenException) {
                val jobResult = JobResult(singleInstanceId, JobResultStatus.FAIL, throwable)
                jobResultBus.postEvent(jobResult)
            }
            val statusCode = throwable.response.code()
            if (statusCode in 400..499) {
                return RetryConstraint.CANCEL
            }
            return RetryConstraint.createExponentialBackoff(runCount, 1000)
        } else {
            return RetryConstraint.CANCEL
        }
        // if we are here, most likely the connection was lost during job execution

    }

    companion object {
        fun generateSingleId(id: String? = null, jobType: JobType): String {
            if (id == null) {
                return jobType.name
            } else {
                return "${jobType.name}_$id"
            }

        }
    }

    enum class JobType {
        ADD_CARD, EDIT_CARD, SYNC_ALL, LIKE_CARD, DELETE_CARD
    }

    override fun getRetryLimit(): Int {
        return 1
    }

    open fun onCancelJob(cancelReason: Int, throwable: Throwable?) {}

    override fun onCancel(cancelReason: Int, throwable: Throwable?) {
        Log.d("BaseJob", "canceling job. reason: $cancelReason, throwable: $throwable")
        if (throwable is RefreshTokenException) {
            val jobResult = JobResult(singleInstanceId, JobResultStatus.FAIL, throwable)
            jobResultBus.postEvent(jobResult)
        }
        onCancelJob(cancelReason, throwable)
    }
}
