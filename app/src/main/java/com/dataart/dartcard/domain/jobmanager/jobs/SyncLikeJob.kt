package com.dataart.dartcard.domain.jobmanager.jobs

import android.util.Log


class SyncLikeJob(val cardId: Int) : BaseSyncJob(singleId = generateSingleId(cardId.toString(), JobType.LIKE_CARD)) {

    override fun onRun() {
        syncRepository.syncLike(cardId).blockingAwait()
    }

    override fun onAdded() {
        Log.d(TAG, "added job");
    }

    companion object {
        private const val TAG = "AddCardJob"
    }

}