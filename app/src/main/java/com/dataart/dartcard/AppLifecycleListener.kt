package com.dataart.dartcard


import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import com.birbit.android.jobqueue.JobManager
import com.dataart.dartcard.domain.jobmanager.startSync
import javax.inject.Inject

class AppLifecycleListener(app: App) : LifecycleObserver {

    @Inject
    internal lateinit var workManager: JobManager

    init {
        app.appComponent.inject(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onMoveToForeground() {
        workManager.startSync()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onMoveToBackground() {
    }
}
