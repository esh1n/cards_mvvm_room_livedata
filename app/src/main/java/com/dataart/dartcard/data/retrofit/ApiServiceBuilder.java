package com.dataart.dartcard.data.retrofit;


import okhttp3.OkHttpClient;
import retrofit2.CallAdapter;
import retrofit2.Converter;
import retrofit2.Retrofit;


public class ApiServiceBuilder {

    private String mEndPoint;
    private Converter.Factory mConverterFactory;
    private CallAdapter.Factory mCallAdapterFactory;
    private OkHttpClient mClient;

    private ApiServiceBuilder(String endPoint) {
        mEndPoint = endPoint;
    }

    public static ApiServiceBuilder newBuilder(String endPoint) {
        return new ApiServiceBuilder(endPoint);
    }

    public ApiServiceBuilder withCallAdapterFactory(CallAdapter.Factory callAdapterFactory) {
        mCallAdapterFactory = callAdapterFactory;
        return this;
    }

    public ApiServiceBuilder withConverterFactory(Converter.Factory converterFactory) {
        mConverterFactory = converterFactory;
        return this;
    }

    public ApiServiceBuilder withOkHttpClient(OkHttpClient client) {
        mClient = client;
        return this;
    }

    public <S> S build(Class<S> clazz) {
        return new Retrofit.Builder()
                .baseUrl(mEndPoint)
                .client(mClient)
                .addConverterFactory(mConverterFactory)
                .addCallAdapterFactory(mCallAdapterFactory)
                .build()
                .create(clazz);
    }
}