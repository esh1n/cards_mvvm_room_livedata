package com.dataart.dartcard.data.room.dao

import android.arch.lifecycle.LiveData
import android.arch.paging.DataSource
import android.arch.persistence.room.*
import android.arch.persistence.room.OnConflictStrategy.IGNORE
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import com.dataart.dartcard.data.room.entity.*
import io.reactivex.Single

/**
 * Created by sergeyeshin on 11/3/17.
 */

@Dao
abstract class CardDao {


    @Query("SELECT * from card")
    abstract fun allCards(): List<CardEntry>

    @Insert(onConflict = IGNORE)
    abstract fun bulkInsert(cardEntries: List<CardEntry>)

    @Update(onConflict = IGNORE)
    abstract fun bulkUpdate(cardEntries: List<CardEntry>)

    @Update(onConflict = REPLACE)
    abstract fun updateCard(cardEntry: CardEntry)

    @Insert(onConflict = REPLACE)
    abstract fun insert(cardEntry: CardEntry): Long

    @Update
    abstract fun update(vararg cardEntries: CardEntry)

    @Query("SELECT DISTINCT " +
            "(SELECT favorite FROM user_card_join WHERE card_id = cardId) as favorite, " +
            "user.owner AS mine," +
            "user.name AS ownerName," +
            "card.card_id as id, " +
            "card.synced as synced, " +
            "card.brandId as  brandName," +
            "serverId,barCode,facePhoto,backPhoto,cardNumber,discount,barCodeFormat,isPrivate,note " +
            "FROM  card JOIN user ON user.id = ownerId " +
            "WHERE card.card_id = :id")
    abstract fun getCardById(id: Int): LiveData<CardDetailsEntry?>

    @Transaction
    @Query("SELECT * from card WHERE card_id = :id")
    abstract fun getCardWithBrandById(id: Long): CardWithEmbeddedBrand

    @Query("SELECT " +
            "card.synced as synced," +
            "card.isPrivate as isPrivate," +
            "brandId AS brandName," +
            "coalesce(facePhoto, backPhoto) AS photo," +
            "card_id AS id," +
            "user.name AS ownerName," +
            "(SELECT favorite FROM user_card_join WHERE card_id = cardId)  as favorite " +
            "FROM brand JOIN card ON brand.name = brandId JOIN user ON user.id = ownerId " +
            "WHERE owner = 1 AND searchableName like :nameQuery " +
            "ORDER BY favorite DESC, brandName ASC")
    abstract fun loadMyCardsByName(nameQuery: String): LiveData<List<MyCardsEntry>>


    @Query("SELECT brand.name AS brandName," +
            "coalesce(facePhoto, backPhoto) AS photo," +
            "card.card_id AS id," +
            "card.isPrivate as isPrivate," +
            "user.name AS ownerName," +
            "user.owner AS mine," +
            "card.synced AS synced," +
            "(SELECT favorite FROM user_card_join WHERE card_id = cardId)  as favorite " +
            "FROM brand JOIN card ON brand.name = brandId JOIN user ON user.id = ownerId " +
            "WHERE (isPrivate = 0 OR owner = 1) AND (user.name LIKE :nameQuery OR searchableName like :nameQuery) " +
            "ORDER BY favorite DESC, brandName ASC")
    abstract fun loadCardsByBrandNameOrOwnerName(nameQuery: String): LiveData<List<SearchCardEntry>>

    @Query("SELECT " +
            "coalesce(facePhoto, backPhoto) as photo," +
            "discount , note, isPrivate," +
            "card.serverId as serverId, " +
            "card.card_id as id, " +
            "card.synced AS synced," +
            "(SELECT favorite FROM user_card_join WHERE card_id = cardId) as favorite, " +
            "user.name as ownerName, " +
            "user.owner as mine " +
            "FROM card JOIN user ON user.id = ownerId " +
            "WHERE (isPrivate = 0 OR owner = 1) AND (card.brandId = :brandName)  ORDER BY favorite DESC")
    abstract fun loadCardsByBrand(brandName: String): DataSource.Factory<Int, CardBrandDetails>

    @Query("delete from card where card_id = :id")
    abstract fun deleteById(id: Int): Int

    @Query("select serverId from card where card_id = :id")
    abstract fun getServerId(id: Int): String?

    @Query("SELECT barCode AS value,barCodeFormat AS format, brand.name AS brandName FROM card JOIN brand ON brandId = brand.name WHERE card_id =:cardId")
    abstract fun cardBarCode(cardId: Int): Single<CardWithBarcodeEntry>

    @Delete
    abstract fun bulkDelete(outDatedCards: List<CardEntry>)

    @Query("DELETE FROM card WHERE synced = 1 AND serverId NOT IN(:cardIds) ")
    abstract fun deleteOutdatedCards(cardIds: IntArray)

    @Query("UPDATE card SET brandId = :brandId,barcodeFormat = :barcodeFormat,barCode=:barcodeValue,facePhoto=:frontPhoto,backPhoto=:backPhoto,note=:note,cardNumber=:cardNumber,discount=:discount,isPrivate=:isPrivate, synced = 1 WHERE serverId = :serverId AND synced = 1")
    abstract fun updateServerCard(serverId: String,
                                  brandId: String,
                                  barcodeFormat: String?,
                                  barcodeValue: String?,
                                  frontPhoto: String?,
                                  backPhoto: String?,
                                  note: String?,
                                  cardNumber: String?,
                                  discount: Int,
                                  isPrivate: Boolean): Int


    @Query("UPDATE card SET facePhoto = coalesce(:facePhoto, facePhoto),backPhoto= coalesce(:backPhoto,backPhoto),brandId = :brandId,barcodeFormat = :barcodeFormat,barCode=:barcodeValue,note=:note,cardNumber=:cardNumber,discount=:discount,isPrivate=:isPrivate,synced =0 WHERE card_id =:id")
    abstract fun updateFromUser(id: Long,
                                facePhoto: String?,
                                backPhoto: String?,
                                note: String?,
                                cardNumber: String?,
                                discount: Int,
                                isPrivate: Boolean,
                                brandId: String,
                                barcodeFormat: String?,
                                barcodeValue: String?): Int

    open fun updateCardFromUser(cardEntry: CardEntry): Int {
        return updateFromUser(cardEntry.id.toLong(), cardEntry.facePhoto, cardEntry.backPhoto, cardEntry.note, cardEntry.cardNumber, cardEntry.discount, cardEntry.isPrivate, cardEntry.brandId, cardEntry.barCodeFormat, cardEntry.barCode)
    }

    @Transaction
    open fun bulkUpdateServerCards(cardsFromServer: List<CardEntry>) {
        for (card in cardsFromServer) {
            updateServerCard(card.serverId!!, card.brandId, card.barCodeFormat, card.barCode, card.facePhoto, card.backPhoto, card.note, card.cardNumber, card.discount, card.isPrivate)
        }
    }

    @Query("delete from brand where name not in (select brandId from card)")
    abstract fun deleteAllEmptyBrands()

    @Transaction
    open fun deleteAndReturnServerId(id: Int): String {
        val serverId = getServerId(id)
        val deleted = deleteById(id) > 0
        deleteAllEmptyBrands()
        return if (deleted) serverId ?: "" else ""
    }

    @Query("UPDATE card SET serverId = :serverId,synced = 1 WHERE card_id =:localId")
    abstract fun updateServerId(localId: Long, serverId: String): Int

    @Query("UPDATE card SET facePhoto = :facePhoto,backPhoto=:backPhoto,synced = 1 WHERE card_id =:cardLocalId")
    abstract fun updatePhotos(cardLocalId: Int, facePhoto: String?, backPhoto: String?): Int

    @Query("UPDATE card SET facePhoto = coalesce(:facePhoto, facePhoto),backPhoto= coalesce(:backPhoto,backPhoto),synced = 1 WHERE serverId =:serverId")
    abstract fun updatePhotosByServerId(serverId: String, facePhoto: String?, backPhoto: String?): Int
}
