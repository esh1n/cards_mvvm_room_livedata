package com.dataart.dartcard.data.room.entity


data class MyCardsEntry(val id: String,
                        val synced: Boolean,
                        val brandName: String,
                        val ownerName: String,
                        val photo: String? = null,
                        val favorite: Boolean = false,
                        val isPrivate: Boolean = false)