package com.dataart.dartcard.data.job

import io.reactivex.Observable

interface RxJobResultBus {

    val events: Observable<JobResult>

    fun postEvent(event: JobResult)
}
