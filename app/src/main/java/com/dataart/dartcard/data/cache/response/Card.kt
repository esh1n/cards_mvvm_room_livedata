package com.dataart.dartcard.data.cache.response

import com.google.gson.annotations.SerializedName

data class Card(@SerializedName("id") val id: String,
                @SerializedName("ownerId") val ownerId: String,
                @SerializedName("brandId") val brandId: String,
                @SerializedName("discount") val discount: Int = 0,
                @SerializedName("note") val note: String?,
                @SerializedName("cardNumber") val cardNumber: String?,
                @SerializedName("barCode") val barCode: String?,
                @SerializedName("barCodeFormat") val barCodeFormat: String?,
                @SerializedName("frontPhoto") val facePhoto: String?,
                @SerializedName("backPhoto") val backPhoto: String?,
                @SerializedName("ownerFirstName") val ownerFirstName: String?,
                @SerializedName("ownerLastName") val ownerLastName: String?,
                @SerializedName("private") val isPrivate: Boolean)

