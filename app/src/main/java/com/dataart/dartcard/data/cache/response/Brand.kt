package com.dataart.dartcard.data.cache.response

import com.google.gson.annotations.SerializedName

data class Brand(@SerializedName("id")
                 val id: String,
                 @SerializedName("name")
                 val name: String) {


}
