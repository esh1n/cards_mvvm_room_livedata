package com.dataart.dartcard.data.cache.response

import com.google.gson.annotations.SerializedName

data class FavoriteCard(@SerializedName("cardId") val cardId: Int,
                        @SerializedName("personId") val userId: String)
