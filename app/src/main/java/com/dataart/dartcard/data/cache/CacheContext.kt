package com.dataart.dartcard.data.cache

import com.dataart.dartcard.data.cache.CacheContext.SingleValueCachePolicy.FILE_SYSTEM_SECURE
import com.dataart.dartcard.data.cache.CacheContext.SingleValueCachePolicy.MAIN_STORAGE
import io.reactivex.annotations.NonNull
import java.util.*

class CacheContext(fileSystemSingleValueCache: SingleValueCache) {
    private val mSingleValueCaches = HashMap<SingleValueCachePolicy, SingleValueCache>()


    private val tokenLock = Any()

    var currentSessionToken: String?
        get() {
            var token: String? = null
            synchronized(tokenLock) {
                val secureStorage = mSingleValueCaches[FILE_SYSTEM_SECURE]
                token = secureStorage?.getValue(SingleValueCache.KEY_TOKEN, String::class.java)
            }
            return token
        }
        set(newToken) {
            synchronized(tokenLock) {
                newToken?.let {
                    val secureStorage = mSingleValueCaches[FILE_SYSTEM_SECURE]
                    secureStorage?.saveValue(SingleValueCache.KEY_TOKEN, newToken)
                }
            }
        }


    fun clearToken() {
        currentSessionToken = ""
    }

    var lastSyncTimeStamp: Long
        get() {
            val secureStorage = mSingleValueCaches[FILE_SYSTEM_SECURE]
            val timestamp = secureStorage?.getValue(SingleValueCache.KEY_SYNC_TIMESTAMP, String::class.java)
            return if (timestamp.isNullOrBlank()) Calendar.getInstance().time.time else timestamp!!.toLong()

        }
        private set(_) {

        }

    var userEmail: String
        get() {
            val secureStorage = mSingleValueCaches[FILE_SYSTEM_SECURE]
            return secureStorage?.getValue(SingleValueCache.USER_EMAIL, String::class.java).orEmpty()

        }
        set(userEmail) {
            val mainStorage = mSingleValueCaches[FILE_SYSTEM_SECURE]
            mainStorage?.saveValue(SingleValueCache.USER_EMAIL, userEmail)
        }

    fun refreshSyncTimestamp() {
        val mainStorage = mSingleValueCaches[FILE_SYSTEM_SECURE]
        mainStorage?.saveValue(SingleValueCache.KEY_SYNC_TIMESTAMP, Calendar.getInstance().time.time)
    }

    init {
        mSingleValueCaches[MAIN_STORAGE] = RAMSingleValueCache()
        mSingleValueCaches[FILE_SYSTEM_SECURE] = fileSystemSingleValueCache
    }

    fun clearSession() {
        for (policy in SingleValueCachePolicy.values()) {
            mSingleValueCaches[policy]?.deleteAll()
        }
    }


    @NonNull
    fun getSingleValueCache(policy: SingleValueCachePolicy): SingleValueCache {
        return mSingleValueCaches[policy]
                ?: throw IllegalArgumentException("Single Value Cache doesn't exist:$policy")
    }

    fun <E : Any> saveEntityToSVC(@NonNull entity: E, policy: SingleValueCachePolicy) {
        val mainStorage = mSingleValueCaches[policy]
        mainStorage?.saveValue(entity.javaClass.getSimpleName(), entity)
    }

    fun <E> removeEntityFromSVC(@NonNull entityClass: Class<E>, policy: SingleValueCachePolicy) {
        val mainStorage = mSingleValueCaches[policy]
        mainStorage?.deleteValue(entityClass.simpleName)
    }

    fun <E : Any> getEntityFromSVC(entityClass: Class<E>, policy: SingleValueCachePolicy): E? {
        val mainStorage = mSingleValueCaches[policy]
        return mainStorage?.getValue(entityClass.simpleName, entityClass)
    }

    fun <E : Any> saveEntityToRAMSVC(@NonNull entity: E) {
        saveEntityToSVC(entity, SingleValueCachePolicy.MAIN_STORAGE)
    }

    fun <E> removeEntityFromRAMSVC(@NonNull entityClass: Class<E>) {
        removeEntityFromSVC(entityClass, SingleValueCachePolicy.MAIN_STORAGE)
    }

    fun <E : Any> getEntityFromRAMSVC(entityClass: Class<E>): E? {
        return getEntityFromSVC(entityClass, SingleValueCachePolicy.MAIN_STORAGE)
    }

    enum class SingleValueCachePolicy {
        MAIN_STORAGE, FILE_SYSTEM_SECURE
    }
}
