package com.dataart.dartcard.data.cache.request.mapper

import com.dataart.dartcard.data.cache.request.AddEditCardRequest
import com.dataart.dartcard.data.room.entity.CardEntry

class CardEntryRequestMapper {

    fun map(source: CardEntry): AddEditCardRequest {

        return AddEditCardRequest(brandName = source.brandId,
                barCode = source.barCode,
                barCodeFormat = source.barCodeFormat,
                discount = source.discount,
                note = source.note,
                cardNumber = source.cardNumber,
                isPrivate = source.isPrivate)
    }

}