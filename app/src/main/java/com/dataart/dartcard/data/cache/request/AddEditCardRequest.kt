package com.dataart.dartcard.data.cache.request

import com.google.gson.annotations.SerializedName

data class AddEditCardRequest(@SerializedName("brandName")
                              val brandName: String,
                              @SerializedName("barCode")
                              val barCode: String? = null,
                              @SerializedName("barCodeFormat")
                              val barCodeFormat: String? = null,
                              @SerializedName("cardNumber")
                              val cardNumber: String? = null,
                              @SerializedName("note")
                              val note: String? = null,
                              @SerializedName("discount")
                              val discount: Int = 0,
                              @SerializedName("private")
                              val isPrivate: Boolean)
