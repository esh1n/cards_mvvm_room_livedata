package com.dataart.dartcard.data.retrofit;


import com.dataart.dartcard.data.AuthorizationApiService;
import com.dataart.dartcard.data.cache.CacheContext;
import com.dataart.dartcard.data.cache.response.TokenResponse;

import okhttp3.Authenticator;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

public class TokenAuthenticator implements Authenticator {

    private final AuthorizationApiService apiService;
    private final CacheContext cacheContext;

    public TokenAuthenticator(AuthorizationApiService apiService, final CacheContext cacheContext) {

        this.apiService = apiService;
        this.cacheContext = cacheContext;
    }

    @Override
    public Request authenticate(Route route, Response response) {
        synchronized (TokenAuthenticator.class) {
            String oldToken = cacheContext.getCurrentSessionToken();
            if (response.code() == 401) {
                TokenResponse refreshResponse = apiService.refreshToken(oldToken).blockingGet();
                if (refreshResponse != null) {
                    String token = refreshResponse.getToken();
                    cacheContext.setCurrentSessionToken(token);
                    return response.request().newBuilder()
                            .header("Authorization", "Bearer " + token)
                            .build();
                } else {
                    return null;
                }
            } else {
                return response.request()
                        .newBuilder()
                        .header("Authorization", "Bearer " + oldToken)
                        .build();
            }
        }
    }
}