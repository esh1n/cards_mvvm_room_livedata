package com.dataart.dartcard.data.room.dao

import android.arch.paging.DataSource
import android.arch.persistence.room.*
import android.arch.persistence.room.OnConflictStrategy.IGNORE
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import com.dataart.dartcard.data.cache.response.FavoriteCard
import com.dataart.dartcard.data.room.entity.BrandedCard
import com.dataart.dartcard.data.room.entity.FavoriteCardEntry
import com.dataart.dartcard.data.room.entity.LikeUpdateEntry
import com.dataart.dartcard.data.room.entity.UserEntry

@Dao
abstract class FavoriteCardDao {

    @Query("SELECT userId as id,name,owner FROM" +
            " user INNER JOIN user_card_join ON user.id=user_card_join.userId" +
            " WHERE user_card_join.cardId=:cardId")
    abstract fun getUsersForCards(cardId: String): List<UserEntry>

    @Query("SELECT brand.name AS brandName," +
            "coalesce(facePhoto, backPhoto) AS photo," +
            "card_id AS cardId, " +
            "card.synced AS synced, " +
            "card.isPrivate AS isPrivate, " +
            "user.name AS ownerName, " +
            "user.owner AS mine " +
            "FROM card " +
            "JOIN user_card_join ON card.card_id=user_card_join.cardId " +
            "JOIN brand ON brand.name=card.brandId " +
            "JOIN user ON user.id = ownerId " +
            " WHERE user_card_join.userId LIKE :userId AND favorite = 1 AND searchableName LIKE :brandName ORDER BY brand.name")
    abstract fun loadFavoriteCardsForUser(userId: String, brandName: String): DataSource.Factory<Int, BrandedCard>


    @Insert(onConflict = REPLACE)
    abstract fun bulkInsert(userCardEntries: List<FavoriteCardEntry>)

    @Update(onConflict = REPLACE)
    abstract fun bulkUpdate(userCardEntries: List<FavoriteCardEntry>)

    @Insert(onConflict = REPLACE)
    abstract fun insert(userCard: FavoriteCardEntry): Long

    @Insert(onConflict = IGNORE)
    abstract fun insertWithIgnore(userCard: FavoriteCardEntry): Long

    @Query("DELETE FROM user_card_join WHERE cardId = :cardId")
    abstract fun deleteById(cardId: Int)

    @Query("UPDATE user_card_join SET favorite = 1,synced = 0 WHERE cardId = :cardId AND userId=:ownerId")
    abstract fun favoriteCardByUser(cardId: Int, ownerId: String): Int

    @Query("UPDATE user_card_join SET synced = 1 WHERE cardId = :cardId")
    abstract fun updateSyncedState(cardId: Int): Int

    @Query("UPDATE user_card_join SET favorite = 0,synced = 0 WHERE cardId = :cardId AND userId=:ownerId")
    abstract fun unFavoriteCardByUser(cardId: Int, ownerId: String): Int

    @Update(onConflict = REPLACE)
    abstract fun update(favoriteEntry: FavoriteCardEntry): Int

    @Query("SELECT card_id from card where serverId=:serverCardId")
    abstract fun getCardLocalId(serverCardId: String): Int

    @Query("delete from user_card_join where favorite=0 AND synced =1")
    abstract fun deleteNotLikes(): Int

    @Query("SELECT EXISTS(SELECT 1 from user_card_join WHERE synced = 0)")
    abstract fun checkIfUnSyncedLikesExist(): Boolean

    @Query("SELECT EXISTS(SELECT 1 from card WHERE serverId =:serverCardId)")
    abstract fun checkIfCardExists(serverCardId: String): Boolean

    @Query("UPDATE user_card_join SET favorite = 1 WHERE userId= :ownerId AND cardId = (SELECT card_id from card WHERE serverId =:serverCardId and synced = 1)")
    abstract fun updateFromServerLike(serverCardId: String, ownerId: String): Int


    @Transaction
    open fun updateOrInsertLike(cardId: Int, ownerId: String) {
        val count = favoriteCardByUser(cardId, ownerId)
        if (count == 0) {
            insert(FavoriteCardEntry(cardId, ownerId, favorite = true, synced = false))
        }
    }

    @Transaction
    open fun updateOrInsertUnLike(cardId: Int, ownerId: String) {
        val count = unFavoriteCardByUser(cardId, ownerId)
        if (count == 0) {
            insert(FavoriteCardEntry(cardId, ownerId, favorite = false, synced = false))
        }
    }

    @Transaction
    open fun syncLikes(likes: List<FavoriteCard>) {
        // deleteNotLikes()
        for (like in likes) {
            val serverCardId = like.cardId.toString()
            val count = updateFromServerLike(serverCardId, like.userId)
            if (count <= 0) {
                if (checkIfCardExists(serverCardId)) {
                    val cardLocalId = getCardLocalId(serverCardId)
                    val localLike = FavoriteCardEntry(cardLocalId, like.userId, true, true)
                    insertWithIgnore(localLike)
                }

            }
        }
    }


    @Query("select serverId,favorite,cardId as localId from user_card_join,card where user_card_join.synced = 0 AND card_id = cardId")
    abstract fun getNotSyncedLikesAndDisLikes(): List<LikeUpdateEntry>

    @Query("select serverId,favorite,cardId as localId from user_card_join,card where card_id = cardId AND card_id = :cardId")
    abstract fun getNotSyncedLike(cardId: Int): LikeUpdateEntry
}