package com.dataart.dartcard.data.room.entity

data class SearchCardEntry(val id: String,
                           val mine: Boolean,
                           val synced: Boolean,
                           val brandName: String,
                           val ownerName: String,
                           val photo: String? = null,
                           val favorite: Boolean = false,
                           val isPrivate: Boolean = false)
