package com.dataart.dartcard.data.room.entity

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Relation

class CardWithEmbeddedBrand {

    @Embedded
    lateinit var cardEntry: CardEntry

    @Relation(parentColumn = "brandId", entityColumn = "name")
    lateinit var brands: List<BrandEntry>

    val brand: BrandEntry
        get() = brands[0]

}