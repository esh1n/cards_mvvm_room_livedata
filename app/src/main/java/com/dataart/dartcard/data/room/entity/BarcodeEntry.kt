package com.dataart.dartcard.data.room.entity

data class CardWithBarcodeEntry(val brandName: String, val value: String, val format: String)
