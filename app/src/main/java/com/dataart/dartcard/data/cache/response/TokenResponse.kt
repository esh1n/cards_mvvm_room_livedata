package com.dataart.dartcard.data.cache.response

import com.google.gson.annotations.SerializedName

data class TokenResponse(@SerializedName("accessToken")
                         var token: String)