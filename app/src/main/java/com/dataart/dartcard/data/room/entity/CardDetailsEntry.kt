package com.dataart.dartcard.data.room.entity


data class CardDetailsEntry(
        val id: String,
        val serverId: String?,
        val synced: Boolean,
        val isPrivate: Boolean,
        val favorite: Boolean = false,
        val mine: Boolean = false,
        val ownerName: String,
        val brandName: String,
        val barCodeFormat: String? = null,
        val barCode: String? = null,
        val facePhoto: String? = null,
        val backPhoto: String? = null,
        val note: String? = null,
        val cardNumber: String? = null,
        val discount: Int = 0)