package com.dataart.dartcard.data.room.dao

import android.arch.lifecycle.LiveData
import android.arch.paging.DataSource
import android.arch.persistence.room.*
import android.arch.persistence.room.OnConflictStrategy.IGNORE
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import com.dataart.dartcard.data.room.entity.BrandEntry
import com.dataart.dartcard.data.room.entity.BrandWithPhoto
import io.reactivex.Maybe

@Dao
abstract class BrandDao {

    @Query("SELECT * from brand")
    abstract fun allBrands(): List<BrandEntry>

    @Insert(onConflict = IGNORE)
    abstract fun bulkInsert(brandEntries: List<BrandEntry>)

    @Update(onConflict = REPLACE)
    abstract fun bulkUpdate(brandEntries: List<BrandEntry>)

    @Update
    abstract fun update(vararg brandEntries: BrandEntry): Int

    @Delete
    abstract fun delete(vararg brandEntries: BrandEntry)

    @Query("SELECT * FROM brand ORDER BY name")
    abstract fun allBrandsSource(): DataSource.Factory<Int, BrandEntry>

    @Query("SELECT name,synced, " +
            "(SELECT DISTINCT coalesce(facePhoto, backPhoto) FROM card" +
            " WHERE (card.facePhoto IS NOT NULL OR card.backPhoto IS NOT NULL) AND brandId = name) AS photo" +
            " FROM brand b" +
            " WHERE (searchableName like :brandName) AND (name in (select brandId from card,user WHERE (isPrivate = 0 OR owner = 1)))" +
            " ORDER BY searchableName")
    abstract fun allBrandsWithPhotoByName(brandName: String): DataSource.Factory<Int, BrandWithPhoto>

    @Query("SELECT * FROM brand WHERE name like :name ORDER BY name")
    abstract fun getBrandsByName(name: String): Maybe<List<BrandEntry>>

    @Query("SELECT DISTINCT * FROM brand WHERE name = :name ")
    abstract fun getBrandByName(name: String): Maybe<BrandEntry>

    @Query("SELECT DISTINCT * FROM brand WHERE name = :name ")
    abstract fun getLiveBrandByName(name: String): LiveData<BrandEntry>

    @Insert(onConflict = IGNORE)
    abstract fun insert(brandEntry: BrandEntry)

    @Query("delete from brand where name not in (select brandId from card)")
    abstract fun deleteAllEmptyBrands()

    @Delete
    abstract fun bulkDelete(outDatedBrands: List<BrandEntry>)

    @Query("DELETE FROM brand WHERE synced = 1 AND serverId NOT IN(:brandIds) ")
    abstract fun deleteOutdatedBrands(brandIds: IntArray)

    @Query("select exists(select 1 from brand where name=:brandName)")
    abstract fun checkIfBrandExist(brandName: String): Boolean

    fun checkIfBrandNotExist(brandName: String) = !checkIfBrandExist(brandName)
}
