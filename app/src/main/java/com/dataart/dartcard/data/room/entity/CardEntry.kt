package com.dataart.dartcard.data.room.entity

import android.arch.persistence.room.*
import android.arch.persistence.room.ForeignKey.CASCADE


@Entity(tableName = "card",
        foreignKeys = [ForeignKey(entity = BrandEntry::class, parentColumns = arrayOf("name"), childColumns = arrayOf("brandId"), onDelete = CASCADE, onUpdate = CASCADE),
            ForeignKey(entity = UserEntry::class, parentColumns = arrayOf("id"), childColumns = arrayOf("ownerId"), onDelete = CASCADE, onUpdate = CASCADE)],
        indices = [Index("brandId"), Index("ownerId"), Index(value = ["serverId"], unique = true)])
data class CardEntry(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "card_id")
        var id: Int = 0,
        var serverId: String? = null,
        var ownerId: String? = null,
        val brandId: String,
        var barCode: String? = null,
        var barCodeFormat: String? = null,
        var facePhoto: String? = null,
        var backPhoto: String? = null,
        var note: String? = null,
        var cardNumber: String? = null,
        var discount: Int = 0,
        var synced: Boolean = false,
        var isPrivate: Boolean = false) {


}





