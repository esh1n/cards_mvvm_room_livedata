package com.dataart.dartcard.data.retrofit;

import android.util.Log;

import com.dataart.dartcard.data.cache.CacheContext;

import java.net.URI;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Authenticator;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;


public class OkHttpBuilder {
    private static final int UNAUTHORIZED_HTTP_CODE = 401;
    private String mHostName;
    private OkHttpClient.Builder mClientBuilder = new OkHttpClient.Builder();

    private OkHttpBuilder(String endPoint) {
        mHostName = URI.create(endPoint).getHost();
    }

    public static OkHttpBuilder newBuilder(String endPoint) {
        return new OkHttpBuilder(endPoint);
    }

    public OkHttpBuilder withLoggingInterceptor(HttpLoggingInterceptor.Level level) {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(level);
        mClientBuilder.addInterceptor(loggingInterceptor);
        mClientBuilder.protocols(Collections.singletonList(Protocol.HTTP_1_1));
        return this;
    }

    public OkHttpBuilder withReadTimeOut(int timeInSeconds) {
        mClientBuilder.readTimeout(timeInSeconds, TimeUnit.SECONDS);
        return this;
    }

    public OkHttpBuilder withWriteTimeOut(int timeInSeconds) {
        mClientBuilder.writeTimeout(timeInSeconds, TimeUnit.SECONDS);
        return this;
    }

    public OkHttpBuilder withConnectTimeOut(int timeInSeconds) {
        mClientBuilder.connectTimeout(timeInSeconds, TimeUnit.SECONDS);
        return this;
    }

    public OkHttpBuilder withAuthorizationHeaderInterceptor(final CacheContext cacheContext) {
        mClientBuilder.addInterceptor(chain -> {
            Request request = chain.request();
            Request.Builder builder = request.newBuilder();

            return chain.proceed(addAuthorizationHeader(builder, cacheContext.getCurrentSessionToken()).build());
            //return chain.proceed(addAuthorizationHeader(builder, "ya29").build());
        });

        return this;
    }

    public OkHttpBuilder withClientTypeInterceptor() {
        mClientBuilder.addInterceptor(chain -> {
            Request request = chain.request();
            Request.Builder builder = request.newBuilder();
            builder.header("Client-Type", "Android");
            return chain.proceed(builder.build());
        });

        return this;
    }

    public OkHttpBuilder withAuthenticator(Authenticator authenticator) {
        mClientBuilder.authenticator(authenticator);
        return this;
    }


    public OkHttpBuilder withUnauthorizedErrorInterceptor() {
        mClientBuilder.addInterceptor(chain -> {
            Request request = chain.request();
            Response response = chain.proceed(request);
            if (response != null && response.code() == UNAUTHORIZED_HTTP_CODE) {

                //rxBus.postEvent(new UnauthorizedErrorEvent());
            }
            return response;
        });

        return this;
    }

    public OkHttpBuilder withSelfSignedSSLSocketFactory() {
        try {
            SSLContext sslContext = SSLContext.getInstance("SSL");
            X509TrustManager trustManager = new X509TrustManager() {
                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType)
                        throws CertificateException {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[]{};
                }
            };
            sslContext.init(null, new TrustManager[]{trustManager}, new SecureRandom());
            SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            mClientBuilder.sslSocketFactory(sslSocketFactory, trustManager);
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            throw new RuntimeException("Failed to setup self signed ssl socket factory", e);
        }

        return this;
    }


    public OkHttpBuilder withHostNameVerifier() {
        mClientBuilder.hostnameVerifier((hostname, session) -> hostname.equalsIgnoreCase(mHostName));
        return this;
    }

    public OkHttpClient build() {
        return mClientBuilder.build();
    }

    private Request.Builder addAuthorizationHeader(Request.Builder requestBuilder, String token) {
        Log.d("Auth", "attach token to request " + token);
        requestBuilder.header("Authorization", "Bearer " + token);
        return requestBuilder;
    }
}