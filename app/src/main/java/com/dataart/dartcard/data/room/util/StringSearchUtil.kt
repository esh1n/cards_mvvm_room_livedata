package com.dataart.dartcard.data.room.util

object StringSearchUtil {
    fun applyDefaultSearchRules(oldQuery: String?): String {
        if (oldQuery != null) {
            val replaced = oldQuery.replace("[Ёё]+".toRegex(), "е")
            val removedExcessChar = replaced.replace("\\p{P}".toRegex(), "")
            val lowerCased = removedExcessChar.toLowerCase()
            return lowerCased.trim { it <= ' ' }.replace(" +".toRegex(), " ")
        }
        return ""

    }
}
