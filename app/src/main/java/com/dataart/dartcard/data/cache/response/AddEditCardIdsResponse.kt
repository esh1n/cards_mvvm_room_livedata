package com.dataart.dartcard.data.cache.response

import com.google.gson.annotations.SerializedName

data class AddEditCardIdsResponse(@field:SerializedName("id")
                                  val id: String,
                                  @SerializedName("brandId")
                                  val brandId: String)
