package com.dataart.dartcard.data.room.entity

data class BrandedCard(val cardId: String,
                       val mine: Boolean,
                       val synced: Boolean,
                       val isPrivate: Boolean,
                       val brandName: String,
                       val ownerName: String,
                       val photo: String? = null)
