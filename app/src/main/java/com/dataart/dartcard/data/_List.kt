package com.dataart.dartcard.data


fun <T> Collection<T>?.isNotNullOrEmpty(): Boolean {
   return this != null && isNotEmpty();
}