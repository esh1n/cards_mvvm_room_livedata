package com.dataart.dartcard.data.room.entity

data class LikeUpdateEntry(val serverId: String?, val favorite: Boolean, val localId: Int)