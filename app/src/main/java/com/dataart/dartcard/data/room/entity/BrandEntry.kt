package com.dataart.dartcard.data.room.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "brand")
data class BrandEntry(@PrimaryKey
                      val name: String,
                      val searchableName: String,
                      var serverId: String? = null,
                      var synced: Boolean = false) {

}
