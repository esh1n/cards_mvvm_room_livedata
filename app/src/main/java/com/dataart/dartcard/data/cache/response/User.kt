package com.dataart.dartcard.data.cache.response


import com.google.gson.annotations.SerializedName

data class User(@SerializedName("personId")
                val id: String,
                @SerializedName("firstName")
                val firstName: String?,
                @SerializedName("lastName")
                val lastName: String?
)
