package com.dataart.dartcard.data.cache.response

import com.google.gson.annotations.SerializedName

data class ImageResponse(@SerializedName("frontPhoto")
                         var frontPhotoId: String?,
                         @SerializedName("backPhoto")
                         var backPhotoId: String?)
