package com.dataart.dartcard.data.room.entity

data class BrandWithPhoto(val name: String,
                          val synced: Boolean,
                          val photo: String? = null)
