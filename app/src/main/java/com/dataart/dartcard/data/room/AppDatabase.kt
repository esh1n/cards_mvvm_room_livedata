package com.dataart.dartcard.data.room

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context

import com.dataart.dartcard.data.room.dao.BrandDao
import com.dataart.dartcard.data.room.dao.CardDao
import com.dataart.dartcard.data.room.dao.FavoriteCardDao
import com.dataart.dartcard.data.room.dao.UserDao
import com.dataart.dartcard.data.room.entity.BrandEntry
import com.dataart.dartcard.data.room.entity.CardEntry
import com.dataart.dartcard.data.room.entity.FavoriteCardEntry
import com.dataart.dartcard.data.room.entity.UserEntry


/**
 * Created by sergeyeshin on 11/3/17.
 */

@Database(entities = [UserEntry::class, CardEntry::class, BrandEntry::class, FavoriteCardEntry::class], version = 15, exportSchema = false)
@TypeConverters(AppConverters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun brandDao(): BrandDao

    abstract fun cardDao(): CardDao

    abstract fun favCardDao(): FavoriteCardDao

    abstract fun userDao(): UserDao

    companion object {

        private const val DATABASE_NAME = "shareCard"

        fun getInstance(context: Context): AppDatabase {
            return Room.databaseBuilder(context.applicationContext,
                    AppDatabase::class.java, AppDatabase.DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .build()
        }
    }
}
