package com.dataart.dartcard.data.job

class JobResult(val id: String, val status: JobResultStatus, val error: Throwable?) {
}

enum class JobResultStatus {
    PENDING, SUCCESS, FAIL
}