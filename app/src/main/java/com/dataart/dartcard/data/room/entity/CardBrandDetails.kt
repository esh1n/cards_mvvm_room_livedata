package com.dataart.dartcard.data.room.entity


data class CardBrandDetails(val photo: String? = null,
                            val mine: Boolean,
                            val serverId: String? = null,
                            val id: String,
                            val favorite: Boolean = false,
                            val discount: Int = 0,
                            val note: String? = null,
                            val ownerName: String,
                            val synced: Boolean,
                            val isPrivate: Boolean)
