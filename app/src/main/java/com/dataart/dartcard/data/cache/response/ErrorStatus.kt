package com.dataart.dartcard.data.cache.response

import com.google.gson.annotations.SerializedName

enum class ErrorStatus {
    @SerializedName("0")
    SUCCESS,
    @SerializedName("1")
    INVALID_CREDENTIALS,
    @SerializedName("2")
    ACCOUNT_LOCKED,
    @SerializedName("3")
    ALREADY_AUTHENTICATED
}
