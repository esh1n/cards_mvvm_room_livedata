package com.dataart.dartcard.data.room.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.ForeignKey.CASCADE
import android.arch.persistence.room.Index

@Entity(tableName = "user_card_join",
        primaryKeys = ["cardId", "userId"],
        foreignKeys = [ForeignKey(entity = CardEntry::class, parentColumns = arrayOf("card_id"), childColumns = arrayOf("cardId"), onDelete = CASCADE),
            ForeignKey(entity = UserEntry::class, parentColumns = arrayOf("id"),
                    childColumns = arrayOf("userId"))], indices = [Index("userId"), Index("cardId")])
data class FavoriteCardEntry(val cardId: Int,
                             val userId: String,
                             var favorite: Boolean = true,
                             var synced: Boolean = true)