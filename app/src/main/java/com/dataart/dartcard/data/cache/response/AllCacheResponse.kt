package com.dataart.dartcard.data.cache.response

import com.google.gson.annotations.SerializedName

data class AllCacheResponse(@SerializedName("person")
                            val person: User,
                            @SerializedName("cards")
                            val cards: List<Card>?,
                            @SerializedName("favorites")
                            val favorites: List<FavoriteCard>?,
                            @SerializedName("brands")
                            val brands: List<Brand>?)