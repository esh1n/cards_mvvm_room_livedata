package com.dataart.dartcard

import android.app.Activity
import android.app.Service
import android.arch.lifecycle.ProcessLifecycleOwner
import android.os.Build.VERSION.SDK_INT
import android.os.Build.VERSION_CODES.LOLLIPOP
import android.support.multidex.MultiDexApplication
import com.crashlytics.android.Crashlytics
import com.dataart.dartcard.presentation.di.AppComponent
import com.dataart.dartcard.presentation.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.HasServiceInjector
import io.fabric.sdk.android.Fabric
import javax.inject.Inject


class App : MultiDexApplication(), HasActivityInjector, HasServiceInjector {
    override fun activityInjector(): AndroidInjector<Activity> = activityInjector
    override fun serviceInjector(): AndroidInjector<Service> = serviceInjector

    lateinit var appComponent: AppComponent
        private set

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder().application(this).build()
        if (SDK_INT >= LOLLIPOP) {
            appComponent.for21Plus().inject(this)
        } else {
            appComponent.inject(this)
        }

        ProcessLifecycleOwner.get().lifecycle.addObserver(AppLifecycleListener(this))
        Fabric.with(this, Crashlytics())
    }


    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var serviceInjector: DispatchingAndroidInjector<Service>
}
