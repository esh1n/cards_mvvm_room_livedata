package com.dataart.dartcard.data


import com.dataart.dartcard.data.cache.request.AddEditCardRequest
import com.dataart.dartcard.data.cache.response.*
import io.reactivex.Completable
import io.reactivex.Single
import okhttp3.MultipartBody
import retrofit2.http.*

interface UserSessionApiService {

    @get:GET("/disco_dev/api/cards")
    val cards: Single<List<Card>>

    @get:GET("/disco_dev/api/brands")
    val brands: Single<List<Brand>>

    @get:GET("/disco_dev/api/favorites")
    val favorites: Single<List<FavoriteCard>>

    @get:GET("/disco_dev/api/user")
    val user: Single<User>

    @GET("/disco_dev/api/syncall")
    fun syncAll(): Single<AllCacheResponse>

    @POST("/disco_dev/api/cards")
    fun addCard(@Body card: AddEditCardRequest): Single<AddEditCardIdsResponse>

    @PUT("/disco_dev/api/cards")
    fun editCard(@Query("cardId") id: String, @Body card: AddEditCardRequest): Single<AddEditCardIdsResponse>

    @DELETE("/disco_dev/api/favorites")
    fun unFavoriteCard(@Query("cardId") id: String): Completable

    @DELETE("/disco_dev/api/cards")
    fun deleteCard(@Query("cardId") id: String): Completable

    @POST("/disco_dev/api/favorites")
    fun favoriteCard(@Query("cardId") id: String): Completable

    @Multipart
    @POST("/disco_dev/api/photos")
    fun uploadPhoto(@Part front: MultipartBody.Part,
                    @Part back: MultipartBody.Part,
                    @Query("cardId") cardId: String): Single<ImageResponse>


}

