package com.dataart.dartcard.presentation.di.module;

import android.content.Context;
import android.support.annotation.NonNull;

import com.dataart.dartcard.BuildConfig;
import com.dataart.dartcard.data.AuthorizationApiService;
import com.dataart.dartcard.data.UserSessionApiService;
import com.dataart.dartcard.data.cache.CacheContext;
import com.dataart.dartcard.data.job.RxJobResultBus;
import com.dataart.dartcard.data.retrofit.ApiServiceBuilder;
import com.dataart.dartcard.data.retrofit.OkHttpBuilder;
import com.dataart.dartcard.data.retrofit.RxErrorHandlingCallAdapterFactory;
import com.dataart.dartcard.data.retrofit.TokenAuthenticator;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.converter.gson.GsonConverterFactory;


@Module
public class DataModule extends BaseDataModule {

    private static final int READ_WRITE_TIMEOUT = 60;
    private static final int CONNECT_TIMEOUT = 5;

    @Provides
    @Singleton
    @NonNull
    @Named("no_session")
    public OkHttpClient provideOkHttpClient(CacheContext cache, RxJobResultBus rxBus) {
        return OkHttpBuilder.newBuilder(BuildConfig.API_ENDPOINT)
                .withLoggingInterceptor(HttpLoggingInterceptor.Level.BODY)
                .withReadTimeOut(READ_WRITE_TIMEOUT)
                .withWriteTimeOut(READ_WRITE_TIMEOUT)
                .withConnectTimeOut(CONNECT_TIMEOUT)
                .withSelfSignedSSLSocketFactory()
                .build();
    }

    @Provides
    @Singleton
    @NonNull
    public AuthorizationApiService provideAuthorizationApiService(@Named("no_session") OkHttpClient client) {
        return ApiServiceBuilder.newBuilder(BuildConfig.API_ENDPOINT)
                .withOkHttpClient(client)
                .withConverterFactory(GsonConverterFactory.create())
                .withCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                .build(AuthorizationApiService.class);
    }


    @Provides
    @Singleton
    @NonNull
    @Named("session")
    public OkHttpClient provideUserSessionOkHttpClient(AuthorizationApiService authorizationApiService, CacheContext cache, RxJobResultBus rxBus) {
        TokenAuthenticator tokenAuthenticator = new TokenAuthenticator(authorizationApiService, cache);
        return OkHttpBuilder.newBuilder(BuildConfig.API_ENDPOINT)
                .withLoggingInterceptor(HttpLoggingInterceptor.Level.BODY)
                .withReadTimeOut(READ_WRITE_TIMEOUT)
                .withWriteTimeOut(READ_WRITE_TIMEOUT)
                .withConnectTimeOut(CONNECT_TIMEOUT)
                .withAuthorizationHeaderInterceptor(cache)
                //.withUnauthorizedErrorInterceptor(rxBus)
                .withUnauthorizedErrorInterceptor()
                .withSelfSignedSSLSocketFactory()
                .withAuthenticator(tokenAuthenticator)
                .build();
    }


    @Provides
    @Singleton
    @NonNull
    public UserSessionApiService provideUserSessionApiService(@Named("session") OkHttpClient client) {
        return ApiServiceBuilder.newBuilder(BuildConfig.API_ENDPOINT)
                .withOkHttpClient(client)
                .withConverterFactory(GsonConverterFactory.create())
                .withCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                .build(UserSessionApiService.class);
    }

}