package com.dataart.dartcard.data

import com.dataart.dartcard.data.cache.response.TokenResponse

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface AuthorizationApiService {

    @GET("/disco/google/token")
    fun getToken(@Query("authCode") authCode: String): Single<TokenResponse>


    @GET("/disco/google/refresh")
    fun refreshToken(@Query("accessToken") accessToken: String): Single<TokenResponse>

}
