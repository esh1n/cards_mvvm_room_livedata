package com.dataart.dartcard.data


import com.dataart.dartcard.data.cache.request.AddEditCardRequest
import com.dataart.dartcard.data.cache.response.*
import io.reactivex.Completable
import io.reactivex.Single
import okhttp3.MultipartBody
import retrofit2.http.*

interface UserSessionApiService {

    @get:GET("/disco/api/cards")
    val cards: Single<List<Card>>

    @get:GET("/disco/api/brands")
    val brands: Single<List<Brand>>

    @get:GET("/disco/api/favorites")
    val favorites: Single<List<FavoriteCard>>

    @get:GET("/disco/api/user")
    val user: Single<User>

    @GET("/disco/api/syncall")
    fun syncAll(): Single<AllCacheResponse>

    @POST("/disco/api/cards")
    fun addCard(@Body card: AddEditCardRequest): Single<AddEditCardIdsResponse>

    @PUT("/disco/api/cards")
    fun editCard(@Query("cardId") id: String, @Body card: AddEditCardRequest): Single<AddEditCardIdsResponse>

    @DELETE("/disco/api/favorites")
    fun unFavoriteCard(@Query("cardId") id: String): Completable

    @DELETE("/disco/api/cards")
    fun deleteCard(@Query("cardId") id: String): Completable

    @POST("/disco/api/favorites")
    fun favoriteCard(@Query("cardId") id: String): Completable

    @Multipart
    @POST("disco/api/photos")
    fun uploadPhoto(@Part front: MultipartBody.Part,
                    @Part back: MultipartBody.Part,
                    @Query("cardId") cardId: String): Single<ImageResponse>


}

