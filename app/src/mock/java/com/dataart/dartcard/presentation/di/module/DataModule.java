package com.dataart.dartcard.presentation.di.module;

import android.support.annotation.NonNull;

import com.dataart.dartcard.data.UserSessionApiService;
import com.dataart.dartcard.data.cache.response.AllCacheResponse;
import com.dataart.dartcard.data.cache.response.Brand;
import com.dataart.dartcard.data.cache.response.Card;
import com.dataart.dartcard.data.cache.response.FavoriteCard;
import com.dataart.dartcard.data.cache.response.User;
import com.dataart.dartcard.presentation.util.TestUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;


@Module
public class DataModule extends BaseDataModule {

    private UserSessionApiService userSessionApiService = new UserSessionApiService() {

        private Gson mGson = new Gson();

        @Override
        public Single<AllCacheResponse> syncAll() {
            try {
                String json = TestUtils.loadResourceAsString("/startup.json");
                AllCacheResponse response = mGson.fromJson(json, AllCacheResponse.class);
                return Observable.just(response);
            } catch (Exception e) {
                throw new IllegalStateException();
            }
        }

        @Override
        public Single<List<Card>> getCards() {
            try {
                String json = TestUtils.loadResourceAsString("/cards.json");
                Type arrayType = new TypeToken<List<Card>>() {
                }.getType();
                List<Card> response = mGson.fromJson(json, arrayType);
                return Observable.just(response);
            } catch (Exception e) {
                throw new IllegalStateException();
            }
        }

        @Override
        public Single<List<Brand>> getBrands() {
            try {
                String json = TestUtils.loadResourceAsString("/brands.json");
                Type arrayType = new TypeToken<List<Brand>>() {
                }.getType();
                List<Brand> response = mGson.fromJson(json, arrayType);
                return Observable.just(response);
            } catch (Exception e) {
                throw new IllegalStateException();
            }
        }

        @Override
        public Single<List<FavoriteCard>> getFavorites() {
            try {
                String json = TestUtils.loadResourceAsString("/favorite.json");
                Type arrayType = new TypeToken<List<FavoriteCard>>() {
                }.getType();
                List<FavoriteCard> response = mGson.fromJson(json, arrayType);
                return Single.just(response);
            } catch (Exception e) {
                throw new IllegalStateException();
            }
        }

        @Override
        public Single<User> getUser() {
            try {
                String json = TestUtils.loadResourceAsString("/user.json");
                User response = mGson.fromJson(json, User.class);
                return Observable.just(response);
            } catch (Exception e) {
                throw new IllegalStateException();
            }
        }

        @Override
        public Completable unFavoriteCard(String id) {
            return Completable.complete();
        }

        @Override
        public Completable favoriteCard(String id) {
            return Completable.complete();
        }

    };


    @Provides
    @Singleton
    @NonNull
    public UserSessionApiService provideUserSessionApiService() {
        return userSessionApiService;
    }


}